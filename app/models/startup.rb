# == Schema Information
#
# Table name: startups
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  vision     :string
#  mission    :string
#  email      :string
#  phone      :string
#  website    :string
#  industry   :string
#  is_active  :boolean          default(TRUE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  avatar     :string
#  facebook   :string
#  linkedin   :string
#  twitter    :string
#  city       :string
#  country    :string
#  deleted_at :datetime
#

class Startup < ActiveRecord::Base

  include PublicActivity::Common

  #relatives
  has_many :ideas, dependent: :destroy
  # many to many association
  has_many :answers
  has_many :startup_entrepreneurs ,-> { order(created_at: :asc) }, dependent: :destroy 
  has_many :entrepreneurs , through: :startup_entrepreneurs
  # nested attributes
  # refactor this one
  accepts_nested_attributes_for :startup_entrepreneurs,
                                :reject_if => :all_blank,
                                :allow_destroy => true
  # callback to include some entrepreneurs to my new company
  #after_save :associate_startup_to_current_entrepreneur
  # generate avatar uploader
  mount_uploader :avatar, AvatarUploader
  # add followable capabilities
  acts_as_followable
  # don't delete information
  acts_as_paranoid


  #validations
  validates :name , presence: true , uniqueness: true , length: {minimum: 2 , maximum: 140}
  validates :vision, length: {minimum: 2 , maximum: 280} , allow_blank: true
  validates :mission , length: {minimum: 2 , maximum: 280} , allow_blank: true
  validates :phone, presence: true , uniqueness: true  ,length: {minimum: 6 , maximum: 21}, numericality: true  # , format: { with: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/ }
  validates :email, presence: true, uniqueness: true  , :email_format => {:message => I18n.t('activerecord.errors.models.startup.attributes.email.format')}
  validates :industry, length:  {minimum: 2 , maximum: 280} , allow_blank: true
  validates :website , uniqueness: true  , format: {with: URI.regexp , message: I18n.t('activerecord.errors.models.startup.attributes.website.format')} , allow_blank: true
  #validate social media
  validates :facebook , format: {with: /(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/ , message: I18n.t('activerecord.errors.models.startup.attributes.facebook.format')}, allow_blank: true
  validates :linkedin , format: {with: /(https?:)?\/\/(www\.)?linkedin.com\/(#!\/)?([^\/ ].)+/ , message: I18n.t('activerecord.errors.models.startup.attributes.linkedin.format')}, allow_blank: true
  validates :twitter , format: {with: /(https?:)?\/\/(www\.)?twitter.com\/(#!\/)?([^\/ ].)+/ , message: I18n.t('activerecord.errors.models.startup.attributes.twitter.format')}, allow_blank: true


  # instead of deleting, indicate the user requested a delete & timestamp it
  # def soft_delete
  #  update_attribute(:deleted_at, Time.current)
  # end


  def active_ideas
    ideas.where(is_active: true)
  end

  def unactive_ideas
    ideas.where(is_active: false)
  end

  def get_member(user)
    if user.class == Entrepreneur
      role = nil
      startup_entrepreneurs.each do |i|
        if i.entrepreneur_id == user.id
          role = i.role
        end
      end
      role || nil
    end
  end

  def deactivate_startup
    if self.update(is_active: false)
      self.ideas.each do |idea|
        if idea.is_active?
          if  idea.update(is_active: false)
            true
          else
            false
          end
        else
          true
        end
      end
    else
      false
    end
  end

  def activate_startup
    if self.update(is_active: true)
      self.ideas.each do |idea|
        if !idea.is_active? && !idea.drive_folder.nil? && !idea.advisor.nil?
          if  idea.update(is_active: true)
            true
          else
            false
          end
        else
          true
        end
      end
    else
      false
    end
  end


end
