# == Schema Information
#
# Table name: advisors
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  occupation             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  deleted_at             :datetime
#  avatar                 :string
#  is_expert              :boolean          default(FALSE), not null
#  authentication_token   :string
#

class Advisor < ActiveRecord::Base
  include PublicActivity::Common
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :token_authenticatable,
         :recoverable, :rememberable, :trackable, :timeoutable, :validatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # relatives
  has_many :ideas
  has_many :answers
  # photo uploader
  # you must change :avatar to your photo attribute
  mount_uploader :avatar, AvatarUploader
  #include some tags
  acts_as_taggable
  acts_as_messageable
  acts_as_reader

  # add follower capabilities
  acts_as_follower

  # ensure a token to use the API
  before_save :ensure_authentication_token

  def ideas
    all_following
  end

  def name 
    "#{first_name} #{last_name}"
  end

  def mailboxer_email(object)
    return email
  end

  validates :first_name , presence: true , length: {minimum: 2 , maximum: 80}
  validates :last_name , presence: true , length: {minimum: 2, maximum: 80}
  validates :phone , presence: true , length: {minimum: 6 , maximum: 21}, numericality: true # , format: { with: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/ } , uniqueness: true
  validates :email , presence:true , uniqueness: true


  validates :facebook , format: {with: /(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/ , allow_blank: true , message: I18n.t('activerecord.errors.models.entrepreneur.attributes.facebook.format')}
  validates :linkedin , format: {with: /(https?:)?\/\/(www\.)?linkedin.com\/(#!\/)?([^\/ ].)+/ , allow_blank: true  , message: I18n.t('activerecord.errors.models.entrepreneur.attributes.linkedin.format')}
  validates :twitter , format: {with: /(https?:)?\/\/(www\.)?twitter.com\/(#!\/)?([^\/ ].)+/ , allow_blank: true , message: I18n.t('activerecord.errors.models.entrepreneur.attributes.twitter.format')}


  validates  :summary , length: {maximum: 700}, allow_blank: true
  validates  :profession , length: {maximum: 140}, allow_blank: true
  validates  :occupation , length: {maximum: 280}, allow_blank: true


  # instead of deleting, indicate the user requested a delete & timestamp it
  def soft_delete
    update_attribute(:deleted_at, Time.current)
  end

  # ensure user account is active
  def active_for_authentication?
    super && !deleted_at
  end

  # provide a custom message for a deleted account
  def inactive_message
    !deleted_at ? super : :deleted_account
  end

  def activities
    advisor_activities_names = ['idea_stage.update_status','idea_activity.update_status','idea_activity.update_status','idea_activity.comment','idea_artifact.update_status','idea_artifact.create', 'idea.create_achievement', 'idea.delete_achievement','idea.create_pivot','idea.finish_process']
    public_activity_names = ['idea.create','startup.create']
    @activities = PublicActivity::Activity.where(key: advisor_activities_names).where(:recipient=>ideas).or(PublicActivity::Activity.where(key: public_activity_names))
    @activities.where.not('owner_id= ? AND owner_type= ?',self.id,'Advisor').where("created_at > ?", self.created_at).order('created_at DESC').limit(10)
  end

  def unread_activities_count
    activities.unread_by(self).count
  end

  def change_password(params={})
    #binding.pry
    #current_password = params.delete(:current_password) if !params[:current_password].blank?
    current_password = params[:current_password]

    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation) if params[:password_confirmation].blank?
    end


    result = if has_no_password?  || valid_password?(current_password)
               params.delete(:current_password)
               update_attributes(params)
             else
               self.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
               params.delete(:current_password)
               self.attributes = params
               false
             end

    clean_up_passwords
    result
  end

  def has_no_password?
    self.encrypted_password.blank?
  end

  # verify if there is an auth token
  def ensure_authentication_token
    self.authentication_token ||= generate_authentication_token
  end

  # Generate new authentication token (a.k.a. "single access token").
  def create_authentication_token
     self.authentication_token  =  Devise.friendly_token
     self.save
     self.authentication_token
  end

  private

  #generate a new authentication token
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless Advisor.where(authentication_token: token).first
    end
  end

end
