# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  question_id :integer
#  startup_id  :integer
#  advisor_id  :integer
#  value       :float
#  answered    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Answer < ActiveRecord::Base
	
	#associations
  belongs_to :question
  belongs_to :startup
  belongs_to :advisor
	
	#validates
	validates :answered, inclusion: {in: [true, false]}
  validates :question, presence: true, allow_blank: true
  validates :startup, presence: true
  validates :advisor, presence: true
end
