# == Schema Information
#
# Table name: questions
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Question < ActiveRecord::Base

	#associations
  has_many :answers

	#validates
  validates :name, presence: true, length: {minimum: 2 , maximum: 40}
  validates :description, presence: true, length: {minimum: 2 , maximum: 400}

end
