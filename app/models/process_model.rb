# == Schema Information
#
# Table name: process_models
#
#  id          :integer          not null, primary key
#  name        :string           default(""), not null
#  description :string           default(""), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ProcessModel < ActiveRecord::Base

  validates :name , presence: true , uniqueness: true , length: {minimum: 2 , maximum: 80}
  validates :description, allow_blank: true  , length: {minimum: 2 , maximum: 280 }


  #relatives
  has_many :stage_models , dependent: :destroy
  has_many :process_model_steps , dependent: :destroy
  has_many :idea_processes


  def stages_average
    stages_array = []
    stage_models = self.stage_models
    stage_models.each  do |stage|
      stages_hash = Hash.new
      stages_hash["id"] = stage.id
      stages_hash["name"] = stage.name
      stages_hash["duration"] =   IdeaStage.stage_average(stage.id)
      stages_array << stages_hash
    end
    stages_array
  end

  def process_duration
    IdeaProcess.process_duration(self.id)
  end


  def idea_progress_percentage
    ideas_array = []
    ideas  = Idea.where(is_active: true)
    ideas.each do |idea|
      ideas_hash = Hash.new
      ideas_hash["id"] = idea.id
      ideas_hash["idea_name"] = idea.name
      ideas_hash["percentage"] = idea.idea_process.completed_percentage
      ideas_hash["pivot"] = idea.idea_process.has_pivots?
      ideas_hash["stages"] = IdeaStage.stages_average_idea(idea.idea_process)
      ideas_array << ideas_hash
    end
    ideas_array
  end


  def ideas_stage_completed
    stages_array = []
    stage_models = self.stage_models
    stage_models.each  do |stage|
      stages_hash = Hash.new
      stages_hash["id"] = stage.id
      stages_hash["name"] = stage.name
      stages_hash["ideas"] =   IdeaStage.ideas_completed(stage.id)
      stages_array << stages_hash
    end
    stages_array
  end


  def ideas_in_progress
    ideas_array = []
    idea_processes = IdeaProcess.where(process_model_id: self.id)
    idea_processes.each do |idea_process|
     ideas_array << idea_process.idea.id
    end
    ideas_array.uniq.count
  end


  def process_desertion
    ideas_array = []
    ideas  = Idea.where(is_active: false)
    ideas.each do |idea|
      if idea.has_assigned_process?
        ideas_hash = Hash.new
        ideas_hash["id"] = idea.id
        ideas_hash["idea_name"] = idea.name
        ideas_hash["stages"] = IdeaStage.completed_stages(idea.idea_process)
        ideas_array << ideas_hash
      end
    end
    ideas_array
  end

  # get the number of ideas that are executing an stage
  def number_ideas_stages
    stages_array = []
    stage_models = self.stage_models
    stage_models.each  do |stage|
      stages_hash = Hash.new
      stages_hash["stage_id"] = stage.id
      stages_hash["stage_name"] = stage.name
      stages_hash["ideas_number"] =  IdeaStage.current_ideas_number(stage.id)
      stages_array << stages_hash
    end
    stages_array
  end

  # get ideas and the current stage
  def ideas_information
    ideas_array = []
    ideas  = Idea.where(is_active: true)
    ideas.each do |idea|
      if idea.has_assigned_process?
        ideas_hash = Hash.new
        ideas_hash["idea_id"] = idea.id
        ideas_hash["idea_name"] = idea.name
        ideas_hash["startup_name"] = idea.startup.name
        ideas_hash["current_activities"] = IdeaStage.current_activity_idea(idea.idea_process)
        ideas_array << ideas_hash
      end
    end
    ideas_array
  end


end
