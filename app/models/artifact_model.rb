# == Schema Information
#
# Table name: artifact_models
#
#  id                :integer          not null, primary key
#  name              :string           default(""), not null
#  description       :string           default(""), not null
#  activity_model_id :integer
#  artifact_type     :string           default("")
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  support           :string
#

class ArtifactModel < ActiveRecord::Base

  belongs_to :activity_model
  has_many :idea_artifacts
  
  mount_uploader :support, ArtifactUploader

  # internacionalization all
  validates :name , presence: true , uniqueness: {scope: :activity_model}, length: {minimum: 2 , maximum: 140}
  validates :description, allow_blank: true  , length: {minimum: 2 , maximum: 280 }
  validates :artifact_type , presence: true
  validates :support, file_size: { :less_than_or_equal_to => 10.megabytes.to_i }

end
