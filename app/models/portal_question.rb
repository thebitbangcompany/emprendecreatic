# == Schema Information
#
# Table name: portal_questions
#
#  id         :integer          not null, primary key
#  issue      :string           default(""), not null
#  content    :string           default(""), not null
#  email      :string           default(""), not null
#  author     :string           default(""), not null
#  status     :integer          default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  reply      :string
#

class PortalQuestion < ActiveRecord::Base

  validates :issue , presence: true , length: {minimum: 2 , maximum: 140}
  validates :content, presence: true, length: {minimum: 2 , maximum: 280}
  validates :email , presence: true, format: Devise::email_regexp
  validates :author , presence: true , length: {minimum: 2 }
  validates :status , presence: true

  #the field status has two possible options
    # 0 Pending  , needs a review
    # 1 Finished , when the administrator wants to send an email


  #with this callback we want to send an email
  after_update :send_a_message

  private

    def send_a_message
      if self.status == 1
        PlatformMailer.question_reply(self).deliver_later
      end
    end
end
