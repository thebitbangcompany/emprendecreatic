# == Schema Information
#
# Table name: activity_models
#
#  id             :integer          not null, primary key
#  name           :string           default(""), not null
#  description    :string           default(""), not null
#  stage_model_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  has_deadline   :boolean          default(FALSE), not null
#

class ActivityModel < ActiveRecord::Base

  #relatives
  belongs_to :stage_model
  has_many :artifact_models, dependent: :destroy
  has_many :idea_activities, dependent: :destroy
  
  # internacionalization all
  validates :name , presence: true , uniqueness: {scope: :stage_model}  , length: {minimum: 2 , maximum: 140}
  validates :description , allow_blank: true  , length: {minimum: 2 , maximum: 280 }

  accepts_nested_attributes_for :artifact_models , allow_destroy: true

end
