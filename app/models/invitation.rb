
class Invitation
  include ActiveModel::Validations
  attr_accessor :email

  validates :email, presence: true
end