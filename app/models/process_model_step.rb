# == Schema Information
#
# Table name: process_model_steps
#
#  id               :integer          not null, primary key
#  next_step_id     :integer          not null
#  previous_step_id :integer          not null
#  process_model_id :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class ProcessModelStep < ActiveRecord::Base

  belongs_to :process_model
  belongs_to :stage_model_previous_step , class_name: 'StageModel' , foreign_key: 'previous_step_id'
  belongs_to :stage_model_next_step , class_name: 'StageModel' , foreign_key: 'next_step_id'


end
