# == Schema Information
#
# Table name: images
#
#  id          :integer          not null, primary key
#  image       :string
#  description :string
#  idea_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Image < ActiveRecord::Base

  #relatives
  belongs_to :idea

  #validations
  #validates :description, presence: true, length: {minimum: 2 , maximum: 280}

  #uploader
  mount_uploader :image, IdeaImageUploader

end
