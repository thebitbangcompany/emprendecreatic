class Ability
  include CanCan::Ability

  def initialize(user)
    user || Entrepreneur.new
      can :send_message
      can :admin , Startup do |startup|
        if startup.get_member(user) == 'member'
          true
        else
          false
        end
      end
  end
end
