# == Schema Information
#
# Table name: stage_models
#
#  id               :integer          not null, primary key
#  name             :string           default(""), not null
#  description      :string           default(""), not null
#  process_model_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  image            :string
#  color            :string
#

class StageModel < ActiveRecord::Base

  #relatives
  belongs_to :process_model
  has_many :activity_models , dependent: :destroy
  has_many :process_model_next_steps , class_name: 'ProcessModelStep' , foreign_key: 'next_step_id'
  has_many :process_model_previous_steps , class_name: 'ProcessModelStep' , foreign_key: 'previous_step_id'
  has_many :idea_stages
  accepts_nested_attributes_for :activity_models , allow_destroy: true

  # internacionalization
  # all errors
  validates :name , presence: true , uniqueness: {scope: :process_model} , length: {minimum: 2 , maximum: 140}
  validates :description, allow_blank: true , length: {minimum: 2 , maximum: 280 }

  # add uploader
  mount_uploader :image, StageModelUploader


  def activities_average
    activities_array = []
    activity_models = self.activity_models
    activity_models.each  do |activity|
      activity_hash = Hash.new
      activity_hash["id"] = activity.id
      activity_hash["name"] = activity.name
      activity_hash["duration"] = IdeaActivity.activity_average(activity.id)
      activities_array << activity_hash
    end
    activities_array
  end

  # get the number of ideas that are executing an activity
  def number_ideas_activities
    activities_array = []
    activity_models = self.activity_models
    activity_models.each  do |activity|
      stages_hash = Hash.new
      stages_hash["activity_id"] = activity.id
      stages_hash["activity_name"] = activity.name
      stages_hash["ideas_number"] =  IdeaActivity.current_ideas_number(activity.id)
      activities_array << stages_hash
    end
    activities_array
  end


end

# remains generate the implementation of  achievements
