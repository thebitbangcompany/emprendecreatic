# == Schema Information
#
# Table name: idea_processes
#
#  id               :integer          not null, primary key
#  process_model_id :integer
#  start_date       :datetime
#  due_date         :datetime
#  idea_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class IdeaProcess < ActiveRecord::Base
  belongs_to :idea
  has_many :idea_stages ,-> { order(created_at: :asc) }, dependent: :destroy
  belongs_to :process_model
  has_many :idea_activities, through: :idea_stages, dependent: :destroy

  def finished_activities
    idea_activities.where(status: 2)
  end
  def finished_stages
    idea_stages.where(status: 2)
  end
  def completed_percentage
    completed = (finished_activities.count.to_f/idea_activities.count.to_f) * 100
    completed.round
  end

  def model
    process_model
  end


  def has_pivots?
    if self.idea_stages.where(is_pivot: true).empty?
       false
    else
      true
    end
  end


  # def idea_stages_status_validation(idea_stage)
    # other stage is active by default ?
    # binding.pry
    # initially no !
    # active = nil
    # idea_stages.each do  |stage|
      # each stage different to the param is reviewed
      # you can change the status of the current stage
      # if idea_stage != stage
        # if stage.status == 'start'
          # if other stage is started the loop breaks
          # active = stage
          # break
        # else
          # active = nil
        # end
      # end
    # end
     # return the value
     # active
  # end


	validates :process_model_id, presence: true
	validates :idea_id, presence: true
	validates :start_date, presence: true


  def self.process_duration(process_id)
    total_time = 0

    processes = self.where("process_model_id = ? AND due_date IS NOT NULL", process_id)
    processes.each do |process|
      total_time = total_time + TimeDifference.between(process.start_date, process.due_date).in_days
    end

    # calculate the average
    average = total_time /  (processes.count.nonzero? || 1)
    return average
  end


end
