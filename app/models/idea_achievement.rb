# == Schema Information
#
# Table name: idea_achievements
#
#  id                   :integer          not null, primary key
#  idea_id              :integer
#  achievement_model_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class IdeaAchievement < ActiveRecord::Base

  belongs_to :idea
  belongs_to :achievement_model

  validates :idea_id , presence: true
  validates :achievement_model_id, presence: true, :uniqueness => { :scope => :idea_id }

end
