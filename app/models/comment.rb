# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  commentable_id   :integer
#  commentable_type :string
#  title            :string
#  body             :text
#  subject          :string
#  user_id          :integer          not null
#  user_role        :string           not null
#  parent_id        :integer
#  lft              :integer
#  rgt              :integer
#  created_at       :datetime
#  updated_at       :datetime
#  user_name        :string
#  reported_at      :datetime
#

class Comment < ActiveRecord::Base
  acts_as_nested_set :scope => [:commentable_id, :commentable_type]

  validates :body, :presence => true
  validates :user_id, :presence => true
  validates :user_role, :presence => true

  
  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_votable

  belongs_to :commentable, :polymorphic => true

  def set_reported_date
    self.reported_at = self.created_at
    self.save
  end

  def reported_at_formated
    if self.reported_at.nil?
      created_at.strftime("%m/%d/%Y - %I:%M%p") 
    else
      self.reported_at.strftime("%m/%d/%Y - %I:%M%p") 
    end
  end

  #finds sender 
  def sender
    if user_role == "Entrepreneur"
      Entrepreneur.find(user_id)
    else
      Advisor.find(user_id)
    end
  end

  # NOTE: Comments belong to a user
  #belongs_to :user

  # Helper class method that allows you to build a comment
  # by passing a commentable object, a user_id, and comment text
  # example in readme
  def self.build_from(obj, user_id, user_role,user_name,comment,subject)
    new \
      :commentable => obj,
      :body        => comment,
      :subject     => subject,
      :user_id     => user_id,
      :user_role   => user_role,
      :user_name   => user_name
  end

  #helper method to check if a comment has children
  def has_children?
    self.children.any?
  end

  # Helper class method to lookup all comments assigned
  # to all commentable types for a given user.
  #scope :find_comments_by_user, lambda { |user|
    #where(:user_id => user.id).order('created_at DESC')
  #}

  # Helper class method to look up all comments for
  # commentable class name and commentable id.
  scope :find_comments_for_commentable, lambda { |commentable_str, commentable_id|
    where(:commentable_type => commentable_str.to_s, :commentable_id => commentable_id).order('created_at DESC')
  }

  # Helper class method to look up a commentable object
  # given the commentable class name and id
  def self.find_commentable(commentable_str, commentable_id)
    commentable_str.constantize.find(commentable_id)
  end
end
