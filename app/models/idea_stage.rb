# == Schema Information
#
# Table name: idea_stages
#
#  id              :integer          not null, primary key
#  stage_model_id  :integer
#  idea_process_id :integer
#  status          :integer          default(0)
#  start_date      :datetime
#  due_date        :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  is_pivot        :boolean          default(FALSE)
#

class IdeaStage < ActiveRecord::Base
  include PublicActivity::Common
  belongs_to :idea_process
  has_many :idea_activities,-> { order(created_at: :asc) }, dependent: :destroy
  belongs_to :stage_model

  # statuses
  enum status: [:pending, :start, :finish]

  # scope to get finish idea stages
  scope :finish,               -> { where(status: 2) }
  scope :search_by_stage_model, ->(stage_model_id){ where(stage_model_id:  stage_model_id)}



  # if the status is finished or started we have to set the due date
  before_update :set_dates


  def finished_activities
    idea_activities.where(status: 2)
  end
  
  def completed_percentage
    if idea_activities.any?
      completed = (finished_activities.count.to_f/idea_activities.count.to_f) * 100
      return completed.round
    else
      return 0
    end
  end

  def pending_activities
    idea_activities.where(status: 0)
  end

  def started_activities
    idea_activities.where(status: 1)
  end

  def finished_activities
    idea_activities.where(status: 2)
  end

  def model
    stage_model
  end

  def idea
    idea_process.idea
  end

  # def complete_percentage
  # idea_activities.
  # end
  validates :stage_model_id, presence: true
  validates :idea_process_id, presence: true
  validates :status, presence: true


  # method to verify if you have active all your activities
  def active_activities?
    # other activity is active?
    self.idea_activities.each do |activity|
      if activity.status != 'finish'
        true
        break
      else
        false
      end
    end
  end

  # build pivot
  def self.search_for_pivot(stage_model_id)
    self.search_by_stage_model(stage_model_id)
  end

  def self.build_stage_pivot(idea_stages)
    # generate a new element having as reference the last element used
    idea_stage = idea_stages.last
    # create a new element
    IdeaStage.new(idea_stage.dup.attributes)
  end

  # created the pivot based in the previous stages params: idea_stage_pivot: new_stage, idea_stages: all the stages found
  # idea_activities: all the activities model selected by the user
  def self.create_stage_pivot(idea_stage_pivot, idea_stages, idea_activities)
    # created variable
    created = true
    # if the new stage is saved we can
    # set value to pending
    idea_stage_pivot.status  = 0
    idea_stage_pivot.start_date = nil
    idea_stage_pivot.due_date = nil
    idea_stage_pivot.is_pivot = true
    # save the pivot
    if  idea_stage_pivot.save
      # we have multiple stages so we have to copy all the activities
      activities_found = IdeaActivity.where(idea_stage_id: idea_stages.ids, activity_model_id: idea_activities)
      most_recent_activities = activities_found.order("id DESC").to_a.uniq(&:activity_model_id)

      # for each activity create a copy
      most_recent_activities.each do |idea_activity|
        idea_activity_pivot = IdeaActivity.new(idea_activity.dup.attributes)
        idea_activity_pivot.idea_stage_id = idea_stage_pivot.id
        idea_activity_pivot.status = 0
        idea_activity_pivot.start_date = nil
        idea_activity_pivot.due_date = nil
        if idea_activity_pivot.save
          if !idea_activity.idea_artifacts.blank?
            idea_activity.idea_artifacts.each do |idea_artifact|
              idea_artifact_pivot = IdeaArtifact.new(idea_artifact.dup.attributes)
              idea_artifact_pivot.idea_activity_id = idea_activity_pivot.id
              idea_artifact_pivot.status = 1
              idea_artifact_pivot.start_date = Time.now.in_time_zone
              idea_artifact_pivot.due_date = nil
              idea_artifact.tags.each do |tag|
                idea_artifact_pivot.tag_list.add(tag.name)
              end
              idea_artifact_pivot.save
            end
          end
        else
          idea_stage_pivot.destroy
          created = false
          break
        end
      end
    # pivot not created
    else
      created = false
    end
    created
  end


  def self.stage_average(stage_model)
    # counter of all processes
    idea_stages = 0
    # idea_stage_time_sum
    idea_stage_sum = 0
    # select the stages with the condition
    idea_processes = IdeaProcess.all
    # each idea have one idea process
    idea_processes.each do |idea_process|
      # each idea process has his own stages
      idea_process.idea_stages.each do |idea_stage|
        # if the idea stage has the required stage model
        if idea_stage.stage_model_id == stage_model
          # if the idea_stage accomplish the condition
          if idea_stage.status == 'finish'
           idea_stage_sum = idea_stage_sum + TimeDifference.between(idea_stage.start_date, idea_stage.due_date).in_days
           idea_stages += 1
          end
        end
      end
    end
    # calculate the average
    average = idea_stage_sum/ (idea_stages.nonzero? || 1)
    return average
  end

  def self.stages_average_idea(idea_process)
    #
    stages_array = []

    # find and iterate over each model stage
    model_stages  = ProcessModel.find_by(id: idea_process.process_model_id).stage_models

    model_stages.each do |model_stage|

      # hash
      stage_hash = Hash.new

      idea_stage_sum = 0
      # iterate over all idea stages related to this stage model
      idea_process.idea_stages.each do |idea_stage|
        # if the idea stage has the required stage model
        if idea_stage.stage_model_id == model_stage.id
          # if the idea_stage accomplish the condition
          if idea_stage.status == 'finish'
            idea_stage_sum = idea_stage_sum + TimeDifference.between(idea_stage.start_date, idea_stage.due_date).in_days
          end
        end
      end

      # build the hash
      stage_hash["stage_name"] = model_stage.name
      stage_hash["duration"] = idea_stage_sum

      # add to the array
      stages_array << stage_hash
    end

    return stages_array
  end


  def self.ideas_completed(stage_id)
    idea_processes = []
    stages = self.where("stage_model_id = ? AND status = ?", stage_id, 2)
    stages.each do |stage|
      idea_processes << stage.idea_process.idea_id
    end

    #remove repeated elements and count
    idea_processes = idea_processes.uniq
    idea_processes.count
  end

  def self.completed_stages(idea_process)
    #
    stages_array = []

    # iterate over all idea stages
    idea_process.idea_stages.each do |idea_stage|
      # hash
      stage_hash = Hash.new
      # if the idea_stage accomplish the condition
      if idea_stage.status == 'finish'
        # build the hash
        stage_hash["stage_name"] = idea_stage.stage_model.name
        stage_hash["pivot"] = idea_stage.is_pivot
        stages_array << stage_hash
      end
    end
    return stages_array
  end

  # define which stage is executing an idea
  def self.current_stage_idea(idea_process)
    # return stage name
    stage_name = ''
    # iterate over all idea stages
    idea_process.idea_stages.each do |idea_stage|
      # hash
      stage_hash = Hash.new
      # if the idea_stage accomplish the condition
      if idea_stage.status == 'start'
        # build the hash
        stage_name = idea_stage.stage_model.name
      end
    end
    return stage_name
  end

  # define which activity is executing an idea
  def self.current_activity_idea(idea_process)
    # return stage name
    activities= []
    # iterate over all idea stages
    idea_process.idea_stages.each do |idea_stage|
      idea_stage.idea_activities.each do |idea_activity|
        if idea_activity.status == 'start'
          activities << idea_activity.activity_model.name
        end
      end
    end
    return activities
  end


  # number of ideas executing the current stage
  def self.current_ideas_number(stage_id)
    idea_processes = []
    stages = self.where("stage_model_id = ? AND status = ?", stage_id, 1)
    stages.each do |stage|
      idea_processes << stage.idea_process.idea_id
    end

    #remove repeated elements and count
    idea_processes = idea_processes.uniq
    return idea_processes.count
  end


  private

  def set_dates
    if self.status == 'finish'
      self.due_date = Time.now.in_time_zone
    elsif self.status == 'start' && self.start_date.nil?
      self.start_date = Time.now.in_time_zone
    end
  end

end
