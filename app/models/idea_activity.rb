# == Schema Information
#
# Table name: idea_activities
#
#  id                :integer          not null, primary key
#  activity_model_id :integer
#  idea_stage_id     :integer
#  status            :integer          default(0)
#  start_date        :datetime
#  due_date          :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deadline_date     :datetime
#

class IdeaActivity < ActiveRecord::Base
  include PublicActivity::Common
  # relatives
  belongs_to :idea_stage
  has_many :idea_artifacts,-> { order(created_at: :desc) }, dependent: :destroy
  belongs_to :activity_model


  # if the status is finished or started we have to set the due date
  before_update :set_due_date

  enum status: [:pending, :start, :finish]

  def model
    activity_model
  end

  def startup
    idea_stage.idea_process.idea.startup
  end

  def idea
    idea_stage.idea_process.idea
  end

  # commentable
  acts_as_commentable

  # validations
  validates :activity_model_id, presence: true
  validates :idea_stage_id, presence: true
  validates :status, presence: true

  # method to detect if we have at least one comment
  def comments?(advisor_id)
    #  by default we don't have any comment
    if self.comment_threads.empty?
      false
    elsif self.comment_threads.where("commentable_type = ? AND user_role = ? AND user_id = ?", 'IdeaActivity', 'Advisor', advisor_id).empty?
      false
    else
      true
    end
  end

  # method to detect if we have at least one deliverable
  def artifacts?
    #  by default we don't have any comment
    if self.idea_artifacts.empty?
      false
    else
      true
    end
  end

  # method to detect if we have all the artifacts finished
  def artifacts_finished?
    # other artifact  is still unfinished ?
    if self.idea_artifacts != nil
      # binding.pry
      self.idea_artifacts.each do |artifact|
        if artifact.status == 'finish'
          # if the artifact is finished
          true
        else
          false
          break
        end
      end
    else
      true
    end
  end

  def self.activity_average(activity_model)
    total_time = 0
    activities = self.where("activity_model_id = ? AND status = ?", activity_model, 2)
    activities.each do |activity|
      total_time = total_time + TimeDifference.between(activity.start_date, activity.due_date).in_days
    end
    # calculate the average
      average = total_time / (activities.count.nonzero? || 1)
      return average
  end

  # number of ideas executing the current activity
  def self.current_ideas_number(activity_id)
    idea_processes = []
    activities = self.where("activity_model_id = ? AND status = ?", activity_id, 1)
    activities.each do |activity|
      idea_processes << activity.idea_stage.idea_process.idea_id
    end

    #remove repeated elements and count
    idea_processes = idea_processes.uniq
    return idea_processes.count
  end


  protected

  def set_due_date
    if self.status == 'finish'
      self.due_date = Time.now.in_time_zone
    elsif self.status == 'start' && self.start_date.nil?
      self.start_date = Time.now.in_time_zone
    end
  end
end
