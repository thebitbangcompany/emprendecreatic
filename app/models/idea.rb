# == Schema Information
#
# Table name: ideas
#
#  id                 :integer          not null, primary key
#  name               :string           default(""), not null
#  description        :text             default(""), not null
#  problem_definition :text             default(""), not null
#  startup_id         :integer
#  is_private         :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  advisor_id         :integer
#  is_active          :boolean          default(FALSE)
#  avatar             :string
#  website            :string
#  idea_type          :string
#  facebook           :string
#  twitter            :string
#  drive_folder       :string
#  drive_folder_link  :string
#  deleted_at         :datetime
#

class Idea < ActiveRecord::Base

  include PublicActivity::Common

  #relatives
  belongs_to :startup
  belongs_to :advisor
  has_one :idea_process, dependent: :destroy
  has_many :images, dependent: :destroy
  has_many :idea_achievements, dependent: :destroy
  accepts_nested_attributes_for :images , reject_if: :all_blank, allow_destroy: true

  # add followable capabilities
  acts_as_followable
  # don't delete information
  acts_as_paranoid


  #validations
  validates :name, presence: true  , length: {minimum: 2 , maximum: 280}
  validates_uniqueness_of :name
  validates :description, presence: true , length: {minimum: 2}
  validates :idea_type , presence:true , length: {minimum: 2}
  validates :problem_definition, presence: true , length: {minimum: 2}
  validates :website , uniqueness: true  , format: {with: URI.regexp , message: I18n.t('activerecord.errors.models.idea.attributes.website.format')} , allow_blank: true
  # generate avatar uploader
  mount_uploader :avatar, AvatarUploader

  #validates social media
  validates :facebook , format: {with: /(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/ , message: I18n.t('activerecord.errors.models.idea.attributes.facebook.format')}, allow_blank: true
  validates :twitter , format: {with: /(https?:)?\/\/(www\.)?twitter.com\/(#!\/)?([^\/ ].)+/ , message: I18n.t('activerecord.errors.models.idea.attributes.twitter.format')}, allow_blank: true

  # verify if this idea has finished stages
  def has_finished_stages?
    finished_stages = false
    self.idea_process.idea_stages.each do |stage|
      if stage.status == 'finish'
        finished_stages = true
        break
      else
        finished_stages = false
      end
    end
    finished_stages
  end


  def has_assigned_process?
    if  self.idea_process.nil?
      false
    else
      true
    end
  end


  def all_stages_finished?
    finish = true
    self.idea_process.idea_stages.each do |stage|
      if stage.status != 'finish'
        # if other stage is started or pending the loop breaks
        finish = false
        break
      end
    end
    return finish
  end

  # get a list of stages
  def finish_stage_models
    stage_models = []
    self.idea_process.idea_stages.finish.each do |stage|
      stage_models << stage.model
    end
    stage_models.uniq
  end



  #activate a new idea in order to start a process
  def activate(process = nil, drive_folder_id = nil, drive_folder_link = nil ,advisor = nil)
    if self.idea_process == nil 
      self.idea_process = IdeaProcess.new(start_date: Time.now, process_model_id: process.id)
      process.stage_models.each do |stage|
        myStage = self.idea_process.idea_stages.build(stage_model_id: stage.id)
        myStage.save
        stage.activity_models.each do |activity|
            myActivity = myStage.idea_activities.build(activity_model_id: activity.id)
            myActivity.save
            #activity.artifact_models.each do |artifact|
              #myArtifact = myActivity.idea_artifacts.build(artifact_model_id:artifact.id)
              #myArtifact.save
            #end
        end
      end
      self.advisor = advisor
      self.drive_folder = drive_folder_id
      self.drive_folder_link = drive_folder_link
    end
    self.is_active = true
    self.save
    self.idea_process
  end

  def deactivate
    self.is_active = false 
    self.save
  end


  # def has_active_stages
  #   active = nil
  #   self.idea_process.idea_stages.each do |stage|
  #     if stage.status == 'start'
  #       # if other stage is started the loop breaks
  #       active = stage
  #       break
  #     else
  #       active = nil
  #     end
  #   end
  #   return active
  # end

  def self.find_by_stage(stage_id)
    ideas = []
    IdeaStage.where(stage_model_id: stage_id).each do |stage|
        if stage.status == 'start'
          idea = stage.idea_process.idea
          if idea.is_active
            ideas << idea
          end
        end
    end
    ideas.uniq
  end


  def self.find_by_activity(activity_id)
    ideas = []
    IdeaActivity.where(activity_model_id: activity_id).each do |activity|
      if activity.status == 'start'
        ideas << activity.idea_stage.idea_process.idea
      end
    end
    ideas.uniq
  end

  # instead of deleting, indicate the user requested a delete & timestamp it
  # def soft_delete
  #  update_attribute(:deleted_at, Time.current)
  # end

end

