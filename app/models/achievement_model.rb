# == Schema Information
#
# Table name: achievement_models
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  image       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AchievementModel < ActiveRecord::Base

  mount_uploader :image, AchievementUploader
  has_many :idea_achievements

  validates :name, presence: true, length: {minimum: 2 , maximum: 80}
  validates :description, presence: true, length: {minimum: 2, maximum: 280}
  validates :image , presence: true, file_size: { less_than_or_equal_to: 5.megabytes.to_i }
end
