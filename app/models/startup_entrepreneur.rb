# == Schema Information
#
# Table name: startup_entrepreneurs
#
#  id              :integer          not null, primary key
#  startup_id      :integer
#  entrepreneur_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  role            :string           default("member")
#  position        :string
#

class StartupEntrepreneur < ActiveRecord::Base


  #relatives
  belongs_to :startup
  belongs_to :entrepreneur

  #validation
  validates :entrepreneur_id, :uniqueness => {:scope=>:startup_id}

end
