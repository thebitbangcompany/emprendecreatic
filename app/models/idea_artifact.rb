# == Schema Information
#
# Table name: idea_artifacts
#
#  id                :integer          not null, primary key
#  artifact_model_id :integer
#  idea_activity_id  :integer
#  url               :string
#  status            :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string
#  support           :string
#  start_date        :datetime
#  due_date          :datetime
#

class IdeaArtifact < ActiveRecord::Base
  include PublicActivity::Common
  belongs_to :idea_activity
  belongs_to :artifact_model
  enum status: [:pending, :start, :corrections, :corrected, :finish]

  # upload file
  mount_uploader :support, ArtifactUploader

  # include tags for each artifact
  acts_as_taggable

  # if the status is finished or started we have to set the due date
  before_update :set_due_date
  before_create :set_start_date

  def model
    artifact_model
  end

  # validations
  validates :status, presence: true
  validates :idea_activity_id, presence: true
  validates :artifact_model_id, presence: true
  validates :name, presence: true, length: { minimum: 2, maximum: 140 }
  # we use a regex pattern to verify the google drive file url
  validates :url, presence: true #, format: { with: /https:\/\/drive\.google\.com\/open\?id=([\w\-]*)?/ }

  # validate the support file url if your artifact model type is file.
  # PENDING of verification
  # validates :support, file_size: { less_than_or_equal_to: 10.megabytes.to_i }, presence: true, if: "ArtifactModel.find(self.artifact_model_id).artifact_type == 'file'"

  protected

  def set_due_date
    if self.status == 'finish'
      self.due_date = Time.now.in_time_zone
    end
  end

  def set_start_date
    if self.status == 'start' && self.start_date.nil?
      self.start_date = Time.now.in_time_zone
    end
  end
end
