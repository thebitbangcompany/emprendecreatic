# == Schema Information
#
# Table name: entrepreneurs
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  avatar                 :string
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#  country                :string
#  city                   :string
#  deleted_at             :datetime
#

class Entrepreneur < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable ,
         :recoverable, :rememberable, :trackable, :validatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # you must change :avatar to your photo attribute
  mount_uploader :avatar, AvatarUploader

  #include some tags
  acts_as_taggable
  acts_as_messageable
  acts_as_reader
  
  #relatives
  #many to many associations
  has_many :startup_entrepreneurs , dependent: :destroy
  has_many :startups, through: :startup_entrepreneurs
  has_many :ideas, through: :startups
  #elasticsearch
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  #Active ideas for current entrepreneur
  def active_ideas
    ideas.where(is_active: true)
  end

  def name 
    "#{first_name} #{last_name}"
  end

  def mailboxer_email(object)
    return email
  end

  validates :first_name , presence: true , length: {minimum: 2}
  validates :last_name , presence: true , length: {minimum: 2}
  validates :phone , presence: true , length: {minimum: 6 , maximum: 21}, numericality: true # , format: { with: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/ }
  validates :email , presence: true


  validates :facebook , format: {with: /(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/ , message: I18n.t('activerecord.errors.models.entrepreneur.attributes.facebook.format')}, allow_blank: true
  validates :linkedin , format: {with: /(https?:)?\/\/(www\.)?linkedin.com\/(#!\/)?([^\/ ].)+/ , message: I18n.t('activerecord.errors.models.entrepreneur.attributes.linkedin.format')}, allow_blank: true
  validates :twitter , format: {with: /(https?:)?\/\/(www\.)?twitter.com\/(#!\/)?([^\/ ].)+/ , message: I18n.t('activerecord.errors.models.entrepreneur.attributes.twitter.format')}, allow_blank: true


  validates  :summary , length: {maximum: 280}, allow_blank: true
  validates  :profession , length: {maximum: 140}, allow_blank: true

  def activities
    entrepreneur_activities_names = ['idea.activate','idea.deactivate','idea_stage.update_status','idea_activity.update_status','idea_activity.comment','idea_artifact.update_status','idea_artifact.create', 'idea.create_achievement', 'idea.delete_achievement','idea.create_pivot','idea.finish_process']
    entrepreneur_startup_activities = ['startup.create_member','startup.delete_member','startup.update_member','startup.activate','startup.deactivate']
    @activities = PublicActivity::Activity.where(key: entrepreneur_activities_names).where(:recipient=>ideas).or(PublicActivity::Activity.where(key: entrepreneur_startup_activities).where(:recipient=>startups))
    @activities.where.not('owner_id= ? AND owner_type= ?',self.id,'Entrepreneur').where("created_at > ?", self.created_at).order('created_at DESC').limit(10)
  end

  def unread_activities_count
    activities.unread_by(self).count
  end

  def get_entrepreneur(user)
      Entrepreneur.all.each do |i|
        if i.id == user.id
          true
        else
          false
        end
      end
  end

  def self.search_with_elasticsearch(*args)
    __elasticsearch__.search(*args)
  end

  # soft delete
  # instead of deleting, indicate the user requested a delete & timestamp it
  def soft_delete
    update_attribute(:deleted_at, Time.current)
  end

  # ensure user account is active
  def active_for_authentication?
    super && !deleted_at
  end

  # provide a custom message for a deleted account
  def inactive_message
    !deleted_at ? super : :deleted_account
  end

end
