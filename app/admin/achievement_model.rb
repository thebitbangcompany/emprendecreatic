ActiveAdmin.register AchievementModel do

  menu priority:6 , label: I18n.t('achievements.active_admin.achievements')
  permit_params :name, :description, :image

  # actions management
  actions :all

  index do
    selectable_column
    id_column
    column  :name
    column  :description
    actions
  end

  filter :name , label: I18n.t('stage.active_admin.form.name')

  form do |f|
    f.semantic_errors
    f.inputs  I18n.t('achievements.active_admin.form.title') do
      f.input :name
      f.input :description
      f.input :image, :hint => link_to_if( f.object.image.present?, "#{File.basename(f.object.image.to_s)}" , f.object.image.url) ,:required => false, :as => :file
    end
    f.actions
  end




  show do

    attributes_table_for resource do
      row :name
      row :description
      row :image  do |image|
        if image.image.present?
          link_to  I18n.t('achievements.active_admin.show.download_image'), image.image.url  unless image.image == nil
        end
      end
    end
    active_admin_comments
  end
end
