ActiveAdmin.register PortalQuestion do
  #internacionalization
  config.clear_action_items!
  menu priority:5 , label: I18n.t('portal_question.active_admin.questions')
  actions :all , except: [:new , :create]
  permit_params :status , :reply

  index do
    selectable_column
    id_column
    column I18n.t('portal_question.active_admin.form.author') , :author
    column I18n.t('portal_question.active_admin.form.email'), :email
    column I18n.t('portal_question.active_admin.form.issue'), :issue
    column I18n.t('portal_question.active_admin.form.content'), :content
    column(I18n.t('portal_question.active_admin.form.status')){|b|
     if  b.status == 0
       I18n.t('portal_question.active_admin.form.status_type.pending')
     else
       I18n.t('portal_question.active_admin.form.status_type.finished')
     end
    }
  end


  action_item :edit , only: :show do
    link_to I18n.t('portal_question.active_admin.actions.solve_issue'), edit_admin_portal_question_path(params[:id])
  end

  action_item :destroy , only: :show do
    link_to I18n.t('portal_question.active_admin.actions.delete') , admin_portal_question_path(params[:id]) , method: :delete
  end

  filter :author, label: I18n.t('portal_question.active_admin.form.author')
  filter :email, label: I18n.t('portal_question.active_admin.form.email')
  filter :issue , label: I18n.t('portal_question.active_admin.form.issue')
  filter :content , label: I18n.t('portal_question.active_admin.form.content')
  filter :status , label: I18n.t('portal_question.active_admin.form.status')



  show do
    attributes_table do
      row  I18n.t('portal_question.active_admin.form.author')  do
        resource.author
      end
      row I18n.t('portal_question.active_admin.form.email') do
        resource.email
      end
      row I18n.t('portal_question.active_admin.form.issue') do
        resource.issue
      end
      row I18n.t('portal_question.active_admin.form.content') do
        resource.content
      end
      row I18n.t('portal_question.active_admin.form.reply') do
        resource.reply
      end
      row(I18n.t('portal_question.active_admin.form.status')){ |b|
      if b.status == 0
        I18n.t('portal_question.active_admin.form.status_type.pending')
      else
        I18n.t('portal_question.active_admin.form.status_type.finished')
      end
      }
    end
    active_admin_comments
  end


  # form
  form do |f|
    f.semantic_errors
    f.inputs  I18n.t('portal_question.active_admin.form.question') do
      f.input :reply , label: I18n.t('portal_question.active_admin.form.reply')
      f.input :status , label: I18n.t('portal_question.active_admin.form.status'),  :as => :select , collection: [[I18n.t('portal_question.active_admin.form.status_type.pending') , 0],[ I18n.t('portal_question.active_admin.form.status_type.finished'), 1 ]]
    end
    f.actions
  end



end
