# TODO
# apply internationalization

ActiveAdmin.register Idea do
  menu priority: 8 , label: I18n.t('idea.active_admin.principal_label')
  actions :all , except: [:new,:edit,:create,:update, :destroy]

  index do
    selectable_column
    id_column
    column I18n.t('idea.active_admin.name'), :name
    column I18n.t('idea.active_admin.website'), :website
    column I18n.t('idea.active_admin.type'), :idea_type
    column I18n.t('idea.active_admin.is_active'), :is_active
    actions dropdown: true do |idea|
      unless idea.is_active?
        item  I18n.t('idea.active_admin.delete') , delete_idea_admin_idea_path(id: idea.id) , method: :post , confirm: I18n.t('idea.active_admin.delete_confirmation')
      end
    end

  end

  # We have to complete this actions when the frontend is ready to change password and confirm emails

  #delete advisor account
  member_action :delete_idea, method: :post do
    idea = Idea.find(params[:id])
    idea.activities.each do |idea_activity|
      idea_activity.destroy!
    end
    idea.destroy
    redirect_to admin_ideas_path , notice: I18n.t('idea.active_admin.deleted_notice')
  end


  # actions
  action_item :delete, only: :show do
    link_to I18n.t('idea.active_admin.delete'), delete_idea_admin_idea_path(id: resource.id) , method: :post, confirm: I18n.t('idea.active_admin.delete_confirmation') if !resource.is_active?
  end


  # filters
  filter :name , label: I18n.t('idea.active_admin.name')
  filter :website, label: I18n.t('idea.active_admin.website')

  #
  show do
    attributes_table do
      row I18n.t('idea.active_admin.name')  do
        resource.name
      end
      row I18n.t('idea.active_admin.description') do
        resource.description
      end
      row I18n.t('idea.active_admin.website') do
        resource.website
      end
      row I18n.t('idea.active_admin.is_active') do
        resource.is_active
      end
    end
    active_admin_comments
  end

end
