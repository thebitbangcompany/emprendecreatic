# ActiveAdmin.register ProcessModelStep do
#   belongs_to :process_model
#   permit_params  :next_step_id,:previous_step_id,:process_model_id
#
#   # actions management
#   actions :all, except: [:destroy]
#
#   #internacionalization
#   index :title => I18n.t('process_step.active_admin.steps') do
#     selectable_column
#     id_column
#     column("#{I18n.t('process_step.active_admin.form.previous_step')}"){|b| b.stage_model_previous_step.name}
#     column("#{I18n.t('process_step.active_admin.form.next_step')}"){|b| b.stage_model_next_step.name}
#   end
#
#   #form
#   #internacionalizacion
#   form do |f|
#     f.semantic_errors
#     f.inputs  "#{I18n.t('process_step.active_admin.steps')}" do
#       f.input :previous_step_id , label: I18n.t('process_step.active_admin.form.previous_step'), as: :select , collection: StageModel.where(process_model_id: f.object.process_model_id)
#       f.input :next_step_id, label: I18n.t('process_step.active_admin.form.next_step') , as: :select , collection: StageModel.where(process_model_id: f.object.process_model_id)
#     end
#     f.actions
#   end
#
#   action_item :edit , only: :show do
#     link_to "#{I18n.t('process_step.active_admin.create_sequence')}" , new_admin_process_model_process_model_step_path(params[:process_model_id])
#   end
#
#
#   #internacionalization
#   show do
#     attributes_table do
#       row("#{I18n.t('process_step.active_admin.form.previous_step')}") { |b|  b.stage_model_previous_step }
#       row("#{I18n.t('process_step.active_admin.form.next_step')}") {|b| b.stage_model_next_step}
#     end
#     active_admin_comments
#   end
#
#
# end
