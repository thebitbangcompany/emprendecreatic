ActiveAdmin.register ProcessModel do
  permit_params :name, :description
  # change to process
  menu priority:4 , label:  I18n.t('process.active_admin.principal_label')

  # actions management
  actions :all, except: [:destroy]

  index :title => I18n.t('process.active_admin.index.title') do
    selectable_column
    id_column
    column I18n.t('process.active_admin.form.name') , :name
    column I18n.t('process.active_admin.form.description') , :description
    actions
  end


  # filters
  filter :name , label:  I18n.t('process.active_admin.form.name')


  sidebar I18n.t('stage.active_admin.stages'), only: [:show, :edit] do
    ul do
      process_model.stage_models.each do |stage|
        li link_to "#{stage.name}", admin_process_model_stage_model_path(process_model,stage)
      end
    end
    a link_to "#{I18n.t('stage.active_admin.create_stage')}", new_admin_process_model_stage_model_path(process_model), class: "create_child_button"
  end


  # sidebar I18n.t('process_step.active_admin.steps') , only: [:show , :edit] do
    # ul do
      # process_model.process_model_steps.each do |step|
        # li link_to "#{step.stage_model_previous_step.name} to #{step.stage_model_next_step.name}" , admin_process_model_process_model_step_path(process_model,step)
      # end
    # end
    # a link_to "#{I18n.t('process_step.active_admin.create_sequence')}" , new_admin_process_model_process_model_step_path(process_model), class: "create_child_button"
  # end



  # form
  form do |f|
    f.semantic_errors
    f.inputs  I18n.t('process.active_admin.form.process_details') do
      f.input :name , label: I18n.t('process.active_admin.form.name')
      f.input :description , label:  I18n.t('process.active_admin.form.description')
    end
    f.actions
  end


  show do
    attributes_table do
      row I18n.t('process.active_admin.form.name') do
        resource.name
      end
      row I18n.t('process.active_admin.form.description') do
        resource.description
      end
    end
    active_admin_comments
  end



end
