ActiveAdmin.register StageModel do
  belongs_to :process_model
  permit_params :name, :description, :image, :color, activity_models_attributes: [:id, :name, :description, {artifact_models_attributes: [:id, :name, :description, :support , :artifact_type]}]

  # actions management
  actions :all, except: [:destroy]

  index do
    selectable_column
    id_column
    column  I18n.t('stage.active_admin.form.name') , :name
    actions
  end

  filter :name , label: I18n.t('stage.active_admin.form.name')

  form do |f|
    f.semantic_errors
    f.inputs  I18n.t('stage.active_admin.form.stage_details') do
      f.input :name , label: I18n.t('stage.active_admin.form.name')
      f.input :description , label: I18n.t('stage.active_admin.form.description')
      f.input :image, label: I18n.t('stage.active_admin.form.image') ,:hint => link_to_if( f.object.image.present?, "#{File.basename(f.object.image.to_s)}" , f.object.image.url) ,:required => false, :as => :file
      f.input :color , label: "Color"
      f.has_many :activity_models , heading: I18n.t('activity.active_admin.activities'), allow_destroy: true, new_record: I18n.t('activity.active_admin.create_activity') do |a|
        a.input :name , label: I18n.t('activity.active_admin.form.name')
        a.input :description , label: I18n.t('activity.active_admin.form.description')
        a.has_many :artifact_models , heading: I18n.t('artifact.active_admin.artifacts'), allow_destroy: true, new_record: I18n.t('artifact.active_admin.create_artifact') do |b|
          b.input :name , label: I18n.t('artifact.active_admin.form.name')
          b.input :description , label: I18n.t('artifact.active_admin.form.description')
          b.input :artifact_type , label: I18n.t('artifact.active_admin.form.type'), :as => :select, :collection => [[I18n.t('artifact.active_admin.form.option_link'), 'url']], :include_blank => false
          if b.object.support.present?
            b.input :support, :hint => link_to("#{File.basename(b.object.support.to_s)}" , b.object.support.url) ,:required => false, :as => :file
          else
            b.input :support, :required => false, :as => :file
          end
        end
      end
    end
    f.actions
  end




  show do

    attributes_table_for resource do
      row :name
      row :description
      row :color
      row :image  do |image|
        if image.image.present?
          link_to  I18n.t('stage.active_admin.download_image'), image.image.url  unless image.image == nil
        end
      end
      row I18n.t('activity.active_admin.show.activities') do
        resource.activity_models.each do |activity|
          attributes_table_for activity do
            row :name
            row :description
            row I18n.t('artifact.active_admin.show.artifacts') do
              activity.artifact_models.each do |artifacts|
                attributes_table_for artifacts do
                  row :name
                  row :description
                  row :artifact_type
                  row :support  do |artifact|
                    if artifact.support.present?
                      link_to I18n.t('artifact.active_admin.show.download_file') , artifact.support.url  unless artifact.support == nil
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
    active_admin_comments
  end

end
