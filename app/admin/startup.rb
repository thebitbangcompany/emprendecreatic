# TODO
# apply internationalization

ActiveAdmin.register Startup do
  menu priority:7 , label: I18n.t('startup.active_admin.principal_label')
  actions :all , except: [:new,:edit,:create,:update, :destroy]

  index do
    selectable_column
    id_column
    column I18n.t('startup.active_admin.name') , :name
    column I18n.t('startup.active_admin.phone'), :phone
    column I18n.t('startup.active_admin.email'), :email
    column I18n.t('startup.active_admin.website'), :website
    column I18n.t('startup.active_admin.is_active'), :is_active
    actions dropdown: true do |startup|
      if startup.ideas.empty?
       unless startup.is_active?
         item  I18n.t('startup.active_admin.delete'), delete_startup_admin_startup_path(id: startup.id) , method: :post , confirm: I18n.t('startup.active_admin.delete_confirmation')
       end
      end
    end
  end

  # We have to complete this actions when the frontend is ready to change password and confirm emails
  #delete advisor account
  member_action :delete_startup, method: :post do
    startup = Startup.find(params[:id])
    startup.activities.each do |startup_activity|
      startup_activity.destroy!
    end
    if startup.activities.empty?
      startup.destroy
      redirect_to admin_startups_path , notice: I18n.t('startup.active_admin.deleted_notice')
    end
    #
  end


  # filters

  filter :email, label: I18n.t('startup.active_admin.email')
  filter :name , label: I18n.t('startup.active_admin.name')
  filter :phone, label: I18n.t('startup.active_admin.phone')

  #
  sidebar 'Ideas', only: [:show, :edit] do
    ul do
      startup.ideas.each do |idea|
        li link_to "#{idea.name}", admin_idea_path(idea.id)
      end
    end
  end


  action_item :delete, only: :show do
    if resource.ideas.empty?
      link_to I18n.t('startup.active_admin.delete'), delete_startup_admin_startup_path(id: resource.id), method: :post, confirm: I18n.t('startup.active_admin.delete_confirmation') if !resource.is_active?
    end
  end


  #
  show do

    attributes_table do
      row I18n.t('startup.active_admin.name') do
        resource.name
      end
      row I18n.t('startup.active_admin.phone') do
        resource.phone
      end
      row I18n.t('startup.active_admin.email') do
        resource.email
      end
      row I18n.t('startup.active_admin.website') do
        resource.website
      end
      row I18n.t('startup.active_admin.is_active') do
        resource.is_active
      end
    end
    active_admin_comments
  end

end
