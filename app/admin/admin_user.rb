ActiveAdmin.register AdminUser do
  menu priority:2 , label: I18n.t('admin.active_admin.principal_label')
  permit_params :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column I18n.t('admin.active_admin.form.email') ,  :email
    column I18n.t('admin.active_admin.form.current_sign_in_at') , :current_sign_in_at
    column I18n.t('admin.active_admin.form.sign_in_count'), :sign_in_count
    column I18n.t('admin.active_admin.form.created_at'),  :created_at
    actions
  end

  filter :email , label: I18n.t('admin.active_admin.form.email')

  show do
    attributes_table do
      row I18n.t('admin.active_admin.form.email')  do
        resource.email
      end
      row I18n.t('admin.active_admin.form.current_sign_in_at') do
        resource.current_sign_in_at
      end
      row I18n.t('admin.active_admin.form.sign_in_count') do
        resource.sign_in_count
      end
      row I18n.t('admin.active_admin.form.created_at') do
        resource.created_at
      end

    end
    active_admin_comments
  end



  form do |f|
    f.semantic_errors :admin_user
    f.inputs I18n.t('admin.active_admin.admin_details')  do
      f.input :email , label: I18n.t('admin.active_admin.form.email')
      f.input :password, label: I18n.t('admin.active_admin.form.password')
      f.input :password_confirmation, label:  I18n.t('admin.active_admin.form.password_confirmation')
    end
    f.actions
  end

end
