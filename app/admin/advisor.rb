ActiveAdmin.register Advisor do
  menu priority:3 , label:  I18n.t('advisor.active_admin.principal_label')
  actions :all , except: [:destroy]
  permit_params :email , :password , :password_confirmation , :first_name , :last_name , :phone, :is_expert

  index do
    selectable_column
    id_column
    column I18n.t('advisor.active_admin.form.name') , :name do |u|
      "#{u.first_name} " + "#{u.last_name}"
    end
    column I18n.t('advisor.active_admin.form.email'),  :email
    column I18n.t('advisor.active_admin.form.phone'), :phone
    column I18n.t('advisor.active_admin.form.current_sign_in_at'), :current_sign_in_at
    column I18n.t('advisor.active_admin.form.sign_in_count'), :sign_in_count
    column I18n.t('advisor.active_admin.form.status') , :status do |u|
      if u.deleted_at != nil
        I18n.t('advisor.active_admin.status.deleted')
      else
        I18n.t('advisor.active_admin.status.active')
      end
    end
    actions dropdown: true do |advisor|
      item  I18n.t('advisor.active_admin.send_welcome_instructions') ,  send_welcome_admin_advisor_path(id: advisor.id ), method: :post , :confirm=> I18n.t('messages.confirmation')
      item  I18n.t('advisor.active_admin.send_password_message') , send_password_intructions_admin_advisor_path(id: advisor.id ), method: :post , :confirm=> I18n.t('messages.confirmation')
      item  I18n.t('advisor.active_admin.delete_advisor') , delete_advisor_account_admin_advisor_path(id: advisor.id) , method: :post , confirm: I18n.t('messages.confirmation')
    end

  end

  # We have to complete this actions when the frontend is ready to change password and confirm emails

  #delete advisor account
  member_action :delete_advisor_account , method: :post do
    advisor = Advisor.find(params[:id])
    advisor.soft_delete
    redirect_to admin_advisors_path , notice: I18n.t('advisor.active_admin.advisor_deleted')
  end


  # change password
  member_action :send_password_intructions, method: :post do
    advisor = Advisor.find(params[:id])
    advisor.send_reset_password_instructions
    redirect_to admin_advisors_path , notice: I18n.t('advisor.active_admin.email_sent')
  end

  #send welcome email
  member_action :send_welcome, method: :post do
    advisor = Advisor.find(params[:id])
    AdvisorMailer.welcome_email(advisor).deliver_now
    redirect_to admin_advisors_path, notice:  I18n.t('advisor.active_admin.email_sent')
  end

  # complete this action when you have access to the frontend change password action
  # url = url+mail+reset-token
  controller do
    def create
      advisor = Advisor.new(params.require(:advisor).permit(:email,:password,:password_confirmation,:first_name,:last_name,:phone, :is_expert))
      url = 'generared url'
      if advisor.save
        AdvisorMailer.welcome_email(advisor).deliver_later
        advisor.send_reset_password_instructions
        # internacionalization
        flash[:notice] =  "#{I18n.t('advisor.active_admin.advisor_created')} #{advisor.email}"
        redirect_to admin_advisors_path
      else
        super
      end
    end
  end

  # filters

  filter :email, label: I18n.t('advisor.active_admin.form.email')
  filter :phone, label: I18n.t('advisor.active_admin.form.phone')

  # form

  form do |f|
    f.semantic_errors
    f.inputs  I18n.t('advisor.active_admin.form.advisor_details') do
      f.input :email
      if f.object.new_record?
        f.input :password , label: I18n.t('advisor.active_admin.form.password')
        f.input :password_confirmation , label: I18n.t('advisor.active_admin.form.password_confirmation')
      end
      f.input :first_name , label:  I18n.t('advisor.active_admin.form.first_name')
      f.input :last_name ,  label: I18n.t('advisor.active_admin.form.last_name')
      f.input :phone , label: I18n.t('advisor.active_admin.form.phone')
      f.input :is_expert, label: I18n.t('advisor.active_admin.form.is_expert'), as: :boolean
    end
    f.actions
  end




  show do
    attributes_table do
      row I18n.t('advisor.active_admin.form.email')  do
        resource.email
      end
      row I18n.t('advisor.active_admin.form.first_name') do
          resource.first_name
      end
      row I18n.t('advisor.active_admin.form.last_name') do
        resource.last_name
      end
      row I18n.t('advisor.active_admin.form.phone') do
        resource.phone
      end
      row I18n.t('advisor.active_admin.form.is_expert') do
        if resource.is_expert == true
          I18n.t('advisor.active_admin.is_expert_status.yes')
        else
          I18n.t('advisor.active_admin.is_expert_status.no')
        end
      end
    end
    active_admin_comments
  end

end
