# == Schema Information
#
# Table name: advisors
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  occupation             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  deleted_at             :datetime
#  avatar                 :string
#  is_expert              :boolean          default(FALSE), not null
#  authentication_token   :string
#

class AdvisorsController < ApplicationController

  before_action :set_advisor, only: [:edit, :update, :edit_account, :update_account, :is_user_advisor]
  before_action :authenticate_advisor! , except: [:show,:index]

  def edit
  end

  def report
    @comment = Comment.find(params['report']['id'])
    @activity = @comment.commentable
    @advisor = Advisor.find(@comment.user_id)
    render pdf: "report" , layout: 'report'  # Excluding ".pdf" extension.
  end

  def index
    @advisors = Advisor.all
  end

  def advisor_profile
    redirect_to current_advisor
  end

  def edit_account
  end

  def update
    respond_to do |format|
      if @advisor.update(advisor_params)
        format.html {redirect_to advisor_path(@advisor)}
        format.js
      else
        format.html {render :edit, notice: I18n.t('advisor.controller.uptade.error_message')}
      end
    end
  end

  def update_account
    if @advisor.change_password(advisor_params)
      sign_in @advisor , :bypass => true
      flash[:success] = 'Advisor updated'
      redirect_to advisor_path(@advisor)
      else
        render :edit_account, notice: I18n.t('advisor.controller.update_account.error_message')
      end
  end

  def show
    @advisor = Advisor.find(params[:id])
  end

  def all_startups
  end

  def following_ideas
    @active_following_ideas = []
    @unactive_following_ideas = []
    current_advisor.all_following.each do |idea|
      @active_following_ideas << idea if idea.is_active?  == true
      @unactive_following_ideas << idea if idea.is_active? == false
    end
  end

  def records
    @activity_records = PublicActivity::Activity.where(owner: current_advisor)
  end

  def advisor_params
    params.require(:advisor).permit(:id,
                                    :phone,
                                    :first_name,
                                    :last_name,
                                    :facebook,
                                    :linkedin,
                                    :twitter,
                                    :summary,
                                    :profession,
                                    :tag_list,
                                    :avatar,
                                    :occupation,
                                    :email,
                                    :current_password,
                                    :password,
                                    :password_confirmation)
  end

  def set_advisor
    @advisor = Advisor.find(current_advisor)
  end

  def is_user_advisor
    respond_to do |format|
      format.json { render json: @advisor.to_json }
    end
  end

end
