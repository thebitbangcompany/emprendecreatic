# app/controllers/pusher_demo_controller.rb
class PusherDemoController < ApplicationController
  def send_notification
    Pusher.trigger('test_channel', 'my_event', {
        message: params[:message]
    })

    @current_advisor = {id: 1, name: "Pepito"}
    respond_to do |format|

      format.json { render json: @current_advisor }

    end
  end
end