# == Schema Information
#
# Table name: comments
#
#  id               :integer          not null, primary key
#  commentable_id   :integer
#  commentable_type :string
#  title            :string
#  body             :text
#  subject          :string
#  user_id          :integer          not null
#  user_role        :string           not null
#  parent_id        :integer
#  lft              :integer
#  rgt              :integer
#  created_at       :datetime
#  updated_at       :datetime
#  user_name        :string
#  reported_at      :datetime
#

class CommentsController < ApplicationController

  before_filter :authenticate_emprende_user, except: [:update]
  before_filter :authenticate_advisor!, only: [:update]
  before_filter :set_activity, except: [:update]
  #this controller allow us create comments over activities
  #advisors and startup members can create comments.

  #update comments
  def update
    @comment = Comment.find(params[:id])
    reported_date = Time.zone.parse(comment_params[:reported_at])
    reported_date = reported_date.change(hour: Time.zone.now.hour, min: Time.zone.now.min)
    respond_to do |format|
      if @comment.update(reported_at: reported_date)
        format.json { respond_with_bip(@comment) }
      else
        binding.pry
        format.json { respond_with_bip(@comment) }
      end
    end
  end

  #create or delete comments by advisors
  def create
    if current_emprende_user.class.to_s == "Entrepreneur"
      authorize! :admin , @startup
    end
    @comment = Comment.build_from(@activity, current_emprende_user.id, current_emprende_user.class, current_emprende_user_full_name , comment_params[:body] , comment_params[:subject])
    @activity.create_activity :comment, owner: current_emprende_user, recipient: @activity.idea , parameters: {idea_activity: @activity }
    respond_to do |format|
      if @comment.save
        @comment.set_reported_date
        @comments_paginated = @activity.comment_threads.order('created_at DESC').paginate(page: params[:comments_page], per_page: 4)
        format.js
      else
        format.json {render json: { :error_message => @comment.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def index
    @comments_paginated = @activity.comment_threads.order('created_at DESC').paginate(page: params[:comments_page], per_page: 4)
  end

  def destroy
    if current_emprende_user.class.to_s == "Entrepreneur"
      authorize! :admin , @startup
    end
    @comment = Comment.find(params[:id])
    respond_to do |format|
      if @comment.destroy
        @comments_paginated = @activity.comment_threads.order('created_at DESC').paginate(page: params[:comments_page], per_page: 4)
        format.js
      else
        format.json {render json: { :error_message => @comment.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def comment_params
    params.require(:comment).permit(:body,
                                    :subject,
                                    :_destroy,
                                    :reported_at)
  end

  private

  def set_activity
    @activity = IdeaActivity.find(params[:idea_activity_id])
    @startup = @activity.startup
  end

end
