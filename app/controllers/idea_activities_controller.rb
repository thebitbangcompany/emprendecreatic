# == Schema Information
#
# Table name: idea_activities
#
#  id                :integer          not null, primary key
#  activity_model_id :integer
#  idea_stage_id     :integer
#  status            :integer          default(0)
#  start_date        :datetime
#  due_date          :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deadline_date     :datetime
#

class IdeaActivitiesController < ApplicationController
  # filter actions to be user before each request
  before_action :authenticate_emprende_user
  before_action :set_activity, except: [:activity_model_artifact_selected, :list_by_stage]

  # override to both sides
  # GET show
  def show
    if entrepreneur_signed_in?
      entrepreneur_show
      render :entrepreneur_show
    elsif advisor_signed_in?
      advisor_show
      render :advisor_show
    end
  end

  # only the advisor will have the chance to modify the status of each activity.
  # insert notification
  def advisor_change_status
    respond_to do |format|
      # redirects
      format.html { redirect_to idea_activity_show_path(@activity) }
      # conditions
      if change_status_params[:status] == 'finish'
        if !@activity.comments?(current_emprende_user.id)
          # doesn't have comments
          flash[:notice] = I18n.t('controllers.idea_activities.advisor_change_status.at_least_one_comment')
        # elsif !@activity.artifacts?
          # doesn't have artifacts
          # flash[:notice] = 'no es posible finalizar la actividad si no cuenta al menos con un entregable'
        elsif !@activity.artifacts_finished?
          flash[:notice] =  I18n.t('controllers.idea_activities.advisor_change_status.all_deliverables_finished')
        else
          # everything seems to be good
          advisor_update_status(@activity, change_status_params)
        end
      else
        # you sent another status
        advisor_update_status(@activity, change_status_params)
      end
    end
  end

  # Entrepreneur only can start an activity
  # insert notification
  def change_status
    if @activity.status == 'pending'
      respond_to do |format|
        if @activity.update(status: 1)
          @activity.create_activity :update_status, owner: current_emprende_user, recipient: @idea , parameters: {idea_activity: @activity}
          format.html { redirect_to idea_activity_show_path(@activity), notice:  I18n.t('controllers.idea_activities.change_status.activity_started') }
        else
          format.html { redirect_to idea_activity_show_path(@activity), notice: I18n.t('controllers.idea_activities.change_status.activity_change_status_error')}
        end
      end
    end
  end

  # this method will return all the information related to a model artifact that
  # the entrepreneur is gonna upload as a deliverable
  def activity_model_artifact_selected
    @artifact_model = ArtifactModel.find(params[:id])
    render json: @artifact_model
  end

  def list_by_stage
    @stage = StageModel.find(params[:id])
    @activities = @stage.activity_models
    render json: @activities
  end

  # parameters
  def change_status_params
    params.require(:idea_activity).permit(:status)
  end

  private

  def set_activity
    @activity = IdeaActivity.find(params[:idea_activity_id])
    @idea = @activity.idea
    @startup = @activity.startup
  end

  # view the activity by the side of each entrepreneur
  def entrepreneur_show
    authorize! :admin, @startup
    @comments_paginated = @activity.comment_threads.order('created_at DESC').paginate(page: params[:comments_page], per_page: 4)
    # new artifact
    @new_artifact = @activity.idea_artifacts.new
    # return artifacts
    @artifacts_paginated = @activity.idea_artifacts.order('created_at DESC').paginate(page: params[:artifacts_page], per_page: 3)
    # create a new deliverable
    @new_comment = Comment.build_from(@activity,
                                      current_emprende_user.id,
                                      current_emprende_user.class,
                                      current_emprende_user_full_name,
                                      '',
                                      '')
  end

  # view the activity by the side of each advisor
  def advisor_show
    # return comments
    @comments_paginated = @activity.comment_threads.order('created_at DESC').paginate(page: params[:comments_page], per_page: 4)
    # paginate artifacts
    @artifacts_paginated = @activity.idea_artifacts.order('created_at DESC').paginate(page: params[:artifacts_page], per_page: 2)
    # generate a new comment
    @new_comment = Comment.build_from(@activity,
                                      current_emprende_user.id,
                                      current_emprende_user.class,
                                      current_emprende_user_full_name,
                                      '',
                                      '')
  end

  def advisor_update_status(activity, change_status_params)
    if activity.update(change_status_params)
      activity.create_activity :update_status, owner: current_emprende_user, recipient: @idea , parameters: {idea_activity: activity}
      flash[:notice] = I18n.t('controllers.idea_activities.advisor_update_status.change_status_success')
    else
      flash[:notice] = I18n.t('controllers.idea_activities.advisor_update_status.change_status_error')
    end
  end
end
