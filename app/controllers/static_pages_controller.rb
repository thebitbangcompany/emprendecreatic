class StaticPagesController < ApplicationController
  layout 'visitors'
  def home
  end

  def not_found
  end

end
