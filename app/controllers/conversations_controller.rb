class ConversationsController < ApplicationController
  before_filter :authenticate_emprende_user
  helper_method :mailbox, :conversation

  def create
    email_target = conversation_params(:email)
    user_type = conversation_params(:type)
    if user_type == 'Entrepreneur'
      recipient = Entrepreneur.find_by(email: email_target)
    elsif user_type == 'Advisor'
      recipient = Advisor.find_by(email: email_target)
    else
      return
    end
    
    conversation = current_emprende_user.send_message(recipient, *conversation_params(:body, :subject)).conversation
    if conversation
      @message = I18n.t('controllers.conversations.create.success_message')
    else
      @message = I18n.t('controllers.conversations.create.error_message')
    end
    respond_to do |format|
      format.js 
    end

  end

  def reply
    current_emprende_user.reply_to_conversation(conversation, *message_params(:body, :subject))
    redirect_to conversation_path(conversation)
  end

  def send_trash
    ids = params[:ids]
    ids.each do |id|
      conversation_to_trash = Mailboxer::Conversation.find(id)
      conversation_to_trash.move_to_trash current_emprende_user
    end
    respond_to do |format|
      format.json { render json: {response: :ok}}
    end
  end

  def untrash
    ids = params[:ids]
    ids.each do |id|
      conversation_to_untrash = Mailboxer::Conversation.find(id)
      conversation_to_untrash.untrash current_emprende_user
    end
    respond_to do |format|
      format.json { render json: {response: :ok}}
    end
  end

  def inbox
    @inbox = mailbox.inbox.page(params[:page]).per_page(12)
    respond_to do |format|
      format.js
      format.html
    end
  end

  def sentbox
    @sentbox = mailbox.sentbox.page(params[:page]).per_page(12)
  end
  
  def trash
    @trash = mailbox.trash.page(params[:page]).per_page(12)
  end

  def show
    @user = nil
    conversation.mark_as_read current_emprende_user
    @receipts = conversation.receipts_for(current_emprende_user).sort_by &:created_at
    if conversation.last_message.sender.deleted_at.nil?
      @user = conversation.last_message.sender
    end
  end

  private

  def mailbox
    @mailbox ||= current_emprende_user.mailbox
  end

  def conversation
    @conversation ||= mailbox.conversations.find(params[:id])
  end

  def conversation_params(*keys)
    fetch_params(:conversation, *keys)
  end

  def message_params(*keys)
    fetch_params(:message, *keys)
  end

  def fetch_params(key, *subkeys)
    params[key].instance_eval do
      case subkeys.size
      when 0 then self
      when 1 then self[subkeys.first]
      else subkeys.map{|k| self[k] }
      end
    end
  end
end
