class SearchController < ApplicationController
  def search_entrepreneurs
    @entrepreneurs = []
    if  params[:q]
      #response = Entrepreneur.search_with_elasticsearch  query: { bool: { should: [{ match: { email:  params[:q] }},{ term: { first_name: params[:q]}},{ match: { last_name:  params[:q] }}], must_not: [{ids:{values:[current_emprende_user.id]}}]}}
      #response = Entrepreneur.search_with_elasticsearch  query: { bool: { should: [{ match: { email:  params[:q] }},{ term: { first_name: params[:q]}},{ match: { last_name:  params[:q] }}]}}
        response = Entrepreneur.search_with_elasticsearch query: {
          bool:{
            should:[
              { match: { email:  params[:q] }},{ match: { first_name: params[:q]}},{ match: { last_name:  params[:q] }}
            ]
            # ,
            # must:[
            #   {
            #     constant_score:{
            #       filter:{
            #         exists:{
            #           field: "first_name"
            #         }
            #       }
            #     }
            #   }
            # ]
          }
        }
      # response = Entrepreneur.search_with_elasticsearch   query: {
      #                                                       filtered:{
      #                                                         query:{
      #                                                           match_all: {}
      #                                                         },
      #                                                         filter:{
      #                                                           term:{
      #                                                             email: params[:q]
      #                                                           }
      #                                                         } 
      #                                                       }
      #                                                     }
      @entrepreneurs = response.results
    end
    respond_to do |format|
      format.json { render :json => @entrepreneurs}
    end
  end


  # this query method has to be modified
  def search_entrepreneurs_index
    @entrepreneurs = []
    if  params[:entrepreneur]
      response = Entrepreneur.search_with_elasticsearch query: {
          bool:{
              should:[
                  { match: { email:  params[:entrepreneur] }},{ match: { first_name: params[:entrepreneur]}},{ match: { last_name:  params[:entrepreneur] }}
              ]
          }
      }
      @entrepreneurs = response.results

      entrepreneurs_ids = []
      @entrepreneurs.each do |entrepreneur|
        entrepreneurs_ids << entrepreneur.id
      end

      # override entrepreneurs
      @query = params[:entrepreneur]
      @entrepreneurs = entrepreneurs_ids.collect {|i| Entrepreneur.where(id: i) }.flatten
    end
    respond_to do |format|
      format.js
    end
  end


end