# == Schema Information
#
# Table name: startup_entrepreneurs
#
#  id              :integer          not null, primary key
#  startup_id      :integer
#  entrepreneur_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  role            :string           default("member")
#  position        :string
#

class StartupEntrepreneursController < ApplicationController
  before_action :authenticate_entrepreneur!
  before_action :set_startup

  def index
    @startup_entrepreneurs = @startup.startup_entrepreneurs
  end

  # TODO create activity
  def update
    @startup_entrepreneur = @startup.startup_entrepreneurs.find(params[:id])
    if @startup_entrepreneur.update(startup_entrepreneurs_params)
      @startup.create_activity :update_member, owner: current_emprende_user, recipient: @startup 
      respond_to do |format|
        format.html
        format.json { respond_with_bip(@startup_entrepreneur) }
      end
    end
  end

  # TODO create activity
  def create
    unless @startup.entrepreneurs.where(id: startup_entrepreneurs_params["entrepreneur_id"]).any?
      @startup_entrepreneur = @startup.startup_entrepreneurs.new(startup_entrepreneurs_params)
      @startup.create_activity :create_member, owner: current_emprende_user, recipient: @startup 
      @startup_entrepreneur.save   
    else
      respond_to do |format|
        format.js {  render :partial => "create_error" }
      end
    end
  end

  # TODO create activity
  def destroy
    @startup_entrepreneur = @startup.startup_entrepreneurs.find(params[:id])
    unless @startup_entrepreneur.entrepreneur_id == current_entrepreneur.id
      @startup.create_activity :delete_member, owner: current_emprende_user, recipient: @startup
      @startup_entrepreneur.destroy
    else
      if @startup.startup_entrepreneurs.count == 1
        respond_to do |format|
          format.js {  render :partial => "destroy_last_user_error" }
        end
      else
        @startup_entrepreneur.destroy
        respond_to do |format|
          format.js {  render :partial => "destroy_self" }
        end
      end
    end
  end

  private 

  def startup_entrepreneurs_params
    params.require(:startup_entrepreneur).permit(:position,:entrepreneur_id)
  end

  def set_startup
    @startup = Startup.find(params[:startup_id])
  end
end
