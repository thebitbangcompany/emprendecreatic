class SessionsController < Devise::SessionsController

 before_filter :require_not_authenticated_in_other_scopes, :only => [:new, :create]


 def require_not_authenticated_in_other_scopes
   other_types = [:entrepreneur, :advisor] - [resource_name]
   other_types.each do |type|
     if self.send("#{type}_signed_in?")
       resource = warden.user(type)
       redirect_to after_sign_in_path_for(resource)
     end
   end
 end


end
