class ApplicationController < ActionController::Base
  include PublicActivity::StoreController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :configure_permitted_parameters, if: :devise_controller?
  protect_from_forgery with: :exception
  helper_method :current_emprende_user, :emprende_user_signed_in? , :unread_conversations

  def my_activities
    current_entrepreneur_user.activities.paginate :params => {:controller => activities }
  end
  
  def current_ability
    @current_ability ||= Ability.new(current_entrepreneur)
  end

  def authenticate_emprende_user
    unless emprende_user_signed_in?
      #flash[:error] = "You must be logged In"
      redirect_to root_path
    end
  end

  def emprende_user_signed_in?
     advisor_signed_in? || entrepreneur_signed_in?
  end

  def current_emprende_user
    return current_entrepreneur if entrepreneur_signed_in?
    return current_advisor if advisor_signed_in?
  end

  def current_emprende_user_full_name
    return "#{current_entrepreneur.first_name} #{current_entrepreneur.last_name}" if entrepreneur_signed_in?
    return "#{current_advisor.first_name} #{current_advisor.last_name}" if advisor_signed_in?
  end


  def unread_conversations
    current_emprende_user.mailbox.inbox(:unread=>true)
  end


  # access denied page
  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden }
      format.html { redirect_to  not_found_path, :alert => exception.message }
    end
  end


  protected

  def configure_permitted_parameters
    if resource_class == Entrepreneur
      devise_parameter_sanitizer.for(:sign_up) do |entrepreneur_params|
        entrepreneur_params.permit(:first_name, :last_name , :phone , :email, :password , :password_confirmation)
      end
      devise_parameter_sanitizer.for(:accept_invitation) { |u| u.permit(:first_name, :last_name , :phone , :email, :password , :password_confirmation,:invitation_token) }
    end
  end



end
