class SurveysController < ApplicationController

  before_action :authenticate_advisor!

  def show
    @questions = Question.all
    @answers = Answer.all
    @startupsAnswered = @answers.where(:answered => true, :advisor => current_advisor).group(:startup).count.values.count
    @startups = Startup.all.paginate(:page => params[:page], :per_page =>20)
    respond_to do |format|
      if @startups
        format.html
        format.js {render 'surveys/show', layout: false}
      end
    end
  end

  def qualifyStartup
    startup = Startup.find(params[:startup])
    if !params[:question].nil?
      question = Question.find(params[:question])
    end
    answer = params[:answer]

    if Integer(params[:typeSave]) == 2

      startup_has_answer = startup.answers.where(:advisor => current_advisor).first
      if startup_has_answer.nil?
        Answer.create(:startup => startup, :question => nil, :advisor => current_advisor, :answered => false, :value => nil)
      else
        startup_has_answer.destroy
      end
    else
      @answer = Answer.where(:question => question, :startup => startup, :advisor => current_advisor).limit(1)

      if @answer.empty?
        Answer.create(:startup => startup, :question => question, :advisor => current_advisor, :answered => true, :value => answer)
      else
        @answer.first.update_attribute(:value, answer)
      end

    end

    #startups calificadas por current advisor
    @startupsAnswered = Answer.all.where(:answered => true, :advisor => current_advisor).group(:startup).count.values.count
    respond_to do |format|
      format.json { render json: { "startupsAnswered" => @startupsAnswered } }
    end
  end
end
