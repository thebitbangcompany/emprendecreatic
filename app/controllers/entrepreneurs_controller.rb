# == Schema Information
#
# Table name: entrepreneurs
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  avatar                 :string
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#  country                :string
#  city                   :string
#  deleted_at             :datetime
#

class EntrepreneursController < ApplicationController 

  before_action :set_entrepreneur, only: [:edit, :update, :is_user_entrepreneur]
  before_action :authenticate_entrepreneur! , except: [:show, :index]

  def  edit

  end
  
  def index
    params[:page] = params[:page] || 1
    @entrepreneurs = Entrepreneur.all.where(deleted_at: nil).where.not(first_name: nil).paginate(:page => params[:page], :per_page => 50)
  end

  def entrepreneur_profile
    redirect_to current_entrepreneur
  end

  def update
    respond_to do |format|
      if @entrepreneur.update(entrepreneur_params)
        format.html {redirect_to entrepreneur_path(@entrepreneur)}
        format.js
      else
        # internacionalization
        format.html {render :edit, notice: I18n.t('entrepreneur.controller.update.error_notice')}
        format.js
      end
    end
  end

  def show
    @entrepreneur = Entrepreneur.find(params[:id])
  end


  def entrepreneur_params
    params.require(:entrepreneur).permit(:id,
                                    :phone,
                                    :first_name,
                                    :last_name,
                                    :facebook,
                                    :linkedin,
                                    :twitter,
                                    :summary,
                                    :profession,
                                    :tag_list,
                                    :country,
                                    :city,
                                    :avatar)
  end

  def is_user_entrepreneur
    respond_to do |format|
      format.json { render json: @entrepreneur.to_json }
    end
  end

  private

    def set_entrepreneur
        @entrepreneur = Entrepreneur.find(current_entrepreneur)
    end
end
