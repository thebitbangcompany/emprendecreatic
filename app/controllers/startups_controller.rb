# == Schema Information
#
# Table name: startups
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  vision     :string
#  mission    :string
#  email      :string
#  phone      :string
#  website    :string
#  industry   :string
#  is_active  :boolean          default(TRUE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  avatar     :string
#  facebook   :string
#  linkedin   :string
#  twitter    :string
#  city       :string
#  country    :string
#  deleted_at :datetime
#

class StartupsController < ApplicationController
  before_action :authenticate_advisor! , only:[:active_update,
                                               :advisor_index,
                                               :advisor_inactive]

  before_action :authenticate_entrepreneur! , except: [:active_update,
                                                       :advisor_index,
                                                       :advisor_inactive,
                                                       :show]

  before_action :set_entrepreneur, except: [:active_update,
                                            :advisor_index,
                                            :advisor_inactive,
                                            :show]


  before_action :set_startup , only: [:active_update,
                                      :edit,
                                      :update,
                                      :avatar]

  before_action :require_active_startup , only: [:edit,
                                                 :update,
                                                 :show]
  # advisor side
  # GET advisor_index
   def advisor_index
     @startups = Startup.all
   end

  # GET advisor_inactive_startups
  def advisor_inactive
    @startups = Startup.where(is_active: false)
  end

  # PUT active_update
  def active_update
    if params[:startup][:is_active].to_i == 0
      if @startup.deactivate_startup
        @startup.create_activity :deactivate, owner: current_advisor, recipient: @startup
        redirect_to startup_path(@startup),notice: I18n.t('controllers.startups.active_update.startup_desactivated')
      else
        render :advisor_show
      end
    elsif params[:startup][:is_active].to_i == 1
      if @startup.activate_startup
        @startup.create_activity :activate, owner: current_advisor, recipient: @startup
        redirect_to startup_path(@startup),notice: I18n.t('controllers.startups.active_update.startup_activated')
      else
        render :advisor_show
      end
    end



    # if @startup.update(advisor_startup_params)
      # if @startup.is_active?
      #   @startup.create_activity :activate, owner: current_advisor, recipient: @startup
      #   redirect_to startup_path(@startup),notice: "Esta startup ha sido desactivada"
      # else
      #   @startup.create_activity :deactivate, owner: current_advisor, recipient: @startup
      #   redirect_to startup_path(@startup),notice: "Esta startup ha sido activada"
    #   end
      
    # else
      # render :advisor_show
    # end
  end

  # entrepreneur side
  # validate
  # PUT avatar ( an entrepreneur wants to upload a pic)
  def avatar
    authorize! :admin , @startup
    if @startup.update(startup_params)
      respond_to do |format|
        format.js
      end
    else
      render :edit
      flash.now[:notice] = I18n.t('controllers.startups.avatar.error_message')
    end
  end

  # GET index
  # this index is not working because the view doesn't have any content
   def index
     @startups = @entrepreneur.startups
   end
  # GET new (an entrepreneur wants to create a startup)
  def new
    @startup = @entrepreneur.startups.new
  end
  # GET edit ( an entrepreneur wants to edit a startup )
  def edit
    authorize! :admin , @startup
  end
  # POST create ( an entrepreneur wants to create a startup )
  def create
    @startup = @entrepreneur.startups.new(startup_params)
    respond_to do |format|
      if @startup.save
        #validate if you include yourself
        unless @startup.entrepreneurs.find_by(id: current_entrepreneur.id)
          @startup.entrepreneurs << current_entrepreneur
        end
        # create a push notification
        @startup.create_activity :create, owner: current_entrepreneur, recipient: @startup
        #PusherManagement.send_notifications('advisor_notifications', 'advisor_event', { owner: current_entrepreneur.id, link: startup_path(@startup), target_id: @startup.id, target_class: @startup.class.name, content: current_entrepreneur.first_name + ' ' + current_entrepreneur.last_name + ' ha creado una nueva Startup: ' + @startup.name })
        # redirects
        format.html { redirect_to startup_path(@startup), notice:  I18n.t('controllers.startups.create.success_message')}
        format.json { render :entrepreneur_show, status: :created, location: @startup}
      else
        format.html { render :new , notice:  I18n.t('controllers.startups.create.error_message')}
        format.json { render json: @startup.errors, status: :unprocessable_entity }
      end
    end
  end
  # PUT update ( an entrepreneur wants to update a startup )
  def update
    authorize! :admin , @startup
    if @startup.update(startup_params)
      if @startup.entrepreneurs.count == 0
        @startup.entrepreneurs << current_entrepreneur
      end
      @startup.startup_entrepreneurs
      redirect_to startup_path(@startup) , notice:  I18n.t('controllers.startups.update.success_message')
    else
      render :edit
    end
  end


  # GET show ( a user wants to see a startup )
  def show
    @active_ideas = @startup.active_ideas.order(created_at: :desc)
    #it should be inactive
    @unactive_ideas = @startup.unactive_ideas.order(created_at: :desc)
    if entrepreneur_signed_in?
      entrepreneur_show
      render :entrepreneur_show
    elsif advisor_signed_in?
      # advisor_show
      render :advisor_show
    else
      render :visitor_show
    end
  end

  
private
  
  def entrepreneur_show
    @idea = @startup.ideas.new
  end
  
  # def advisor_show
  # end

  def advisor_startup_params
    params.require(:startup).permit(:is_active)
  end

  def startup_params
    params.require(:startup).permit(:id,
                                    :name,
                                    :phone,
                                    :email,
                                    :website,
                                    :linkedin,
                                    :facebook,
                                    :twitter,
                                    :avatar,
                                    :city,
                                    :country,
                                    :mission,
                                    :vision,
                                    :industry,
                                    startup_entrepreneurs_attributes:[
                                        :id,
                                        :entrepreneur_id,
                                        :position,
                                        :_destroy
                                    ])
  end

  def set_entrepreneur
    @entrepreneur = Entrepreneur.find(current_entrepreneur.id)
  end

  def set_startup
    @startup = Startup.find(params[:id])
  end

  def require_active_startup
    @startup = Startup.find(params[:id])
    unless advisor_signed_in?
      unless @startup.is_active?
        flash[:error] =  I18n.t('controllers.startups.require_active_startup.inactive_startup')
        redirect_to entrepreneur_path(current_entrepreneur)
      end
    end
  end
end
