# == Schema Information
#
# Table name: ideas
#
#  id                 :integer          not null, primary key
#  name               :string           default(""), not null
#  description        :text             default(""), not null
#  problem_definition :text             default(""), not null
#  startup_id         :integer
#  is_private         :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  advisor_id         :integer
#  is_active          :boolean          default(FALSE)
#  avatar             :string
#  website            :string
#  idea_type          :string
#  facebook           :string
#  twitter            :string
#  drive_folder       :string
#  drive_folder_link  :string
#  deleted_at         :datetime
#

class IdeasController < ApplicationController

  before_action :authenticate_advisor! , only:[ :activate, :edit_drive_folder, :deactivate,:advisor_follow_idea, :new_achievement, :create_achievement, :delete_achievement, :new_pivot, :create_pivot, :finish_process]

  before_action :authenticate_entrepreneur! , except: [:show, :activate, :edit_drive_folder, :deactivate,:advisor_follow_idea, :new_achievement, :create_achievement, :delete_achievement, :new_pivot, :create_pivot, :finish_process]

  before_action :set_startup

  before_action :set_entrepreneur , except: [:show, :activate, :edit_drive_folder, :deactivate,:advisor_follow_idea, :new_achievement, :create_achievement, :delete_achievement, :new_pivot, :create_pivot, :finish_process]

  before_action :require_active_idea , only: [:edit, :update, :show]
  # advisor side
  # activate idea
  # PUT activate
  def activate
    @idea = @startup.ideas.find_by(id: params[:id])
    process = ProcessModel.find(params[:process_id].to_i) if params[:process_id]
    if @idea.activate(process, params[:drive_folder], params[:drive_folder_link], current_advisor)
      # byebug
      # idea_followers = @idea.followers
      # idea_followers.each do |idea_follower|
      # PusherManagement.push_message('Idea', 'activate_idea', current_advisor, @idea.id, @idea, startup_idea_path(@startup, @idea), idea_followers)
      # end
      # @idea.create_activity :update, owner: current_advisor
      # PusherManagement.send_notifications('advisor_notifications', 'advisor_event', { owner: current_entrepreneur.id, link: advisor_show_startup_idea_path(@startup, @idea), target_id: @idea.id, target_class: @idea.class.name, content: current_entrepreneur.first_name + ' ' + current_entrepreneur.last_name + ' ha creado una nueva Idea: ' + @idea.name })
      @idea.create_activity :activate, owner: current_emprende_user, recipient: @idea
      redirect_to startup_idea_path(@startup, @idea) , notice: I18n.t('controllers.ideas.activate.idea_activate_successfully')
    else
      render :show
    end
  end

  def edit_drive_folder
    @idea = @startup.ideas.find_by(id: params[:id])
    if @idea.update(drive_folder_link: params[:edit_drive_folder_link], drive_folder: params[:edit_drive_folder])
      redirect_to startup_idea_path(@startup,@idea) , notice: I18n.t('controllers.ideas.edit_drive_folder.update_succeed')
    else
      redirect_to startup_idea_path(@startup,@idea) , notice: I18n.t('controllers.ideas.edit_drive_folder.update_error')
    end

  end


  
  #deactivate idea
  #PUT deactivate
  def deactivate
    @idea = @startup.ideas.find_by(id: params[:id])
    if @idea.deactivate
      @idea.create_activity :deactivate, owner: current_emprende_user, recipient: @idea
      redirect_to startup_idea_path(@startup, @idea) , notice: I18n.t('controllers.ideas.activate.idea_deactivated_successfully')
    else
      render :show
    end
  end

  # GET new_achievement
  def new_achievement
    @achievement_model = AchievementModel.all
    @idea = @startup.ideas.find_by(id: params[:id])
    @idea_achievement = @idea.idea_achievements.new
  end

  # TODO create activity
  # POST create_achievement
  def create_achievement
    @idea = @startup.ideas.find_by(id: params[:id])
    @idea_achievement = @idea.idea_achievements.new(idea_achievement_params)
    respond_to do |format|
      if @idea_achievement.save
        @idea.create_activity :create_achievement, owner: current_emprende_user, recipient: @idea , parameters: {idea_achievement: @idea_achievement}
        format.js {render 'idea_achievements/create'}
      else
        format.json {render json: { error_message: @idea_achievement.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  # TODO create activity
  def delete_achievement
    # pending
    @idea = @startup.ideas.find_by(id: params[:id])
    @idea_achievement = @idea.idea_achievements.find_by(id: params[:achievement_id])
    respond_to do |format|
      if @idea_achievement.destroy
        @idea.create_activity :delete_achievement, owner: current_emprende_user, recipient: @idea , parameters: {idea_achievement: @idea_achievement}
        format.js {render 'idea_achievements/delete_achievement'}
      else
        format.json {render json: { error_message: @idea_artifact.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  # generate pivot
  def new_pivot
    @idea = @startup.ideas.find_by(id: params[:id])
    @idea_stages = @idea.finish_stage_models

    # we are going to create the pivot
    @new_idea_stage = @idea.idea_process.idea_stages.new
  end

  # TODO create activity
  def create_pivot
    @idea = @startup.ideas.find_by(id: params[:id])
    # idea stage to generate pivot
    @idea_stages =  @idea.idea_process.idea_stages.search_for_pivot(params[:stage_model_id])

    # get the idea activities to be replied
    idea_activities = params[:activities_model_ids]
    idea_activities = idea_activities.split(",").map(&:to_i)

    # build the pivot
    @new_idea_stage = @idea.idea_process.idea_stages.build_stage_pivot(@idea_stages)

    #create notification
    @idea.create_activity :create_pivot, owner: current_emprende_user, recipient: @idea , parameters: {pivot: @new_idea_stage}
        
    respond_to do |format|
      if @idea.idea_process.idea_stages.create_stage_pivot(@new_idea_stage, @idea_stages, idea_activities)
        format.html { redirect_to startup_idea_path(@startup, @idea), notice: I18n.t('controllers.ideas.create_pivot.create_pivot_success')}
      else
        format.html { redirect_to startup_idea_path(@startup, @idea), notice: I18n.t('controllers.ideas.create_pivot.create_pivot_error')}
        format.json { render json: @idea.errors, status: :unprocessable_entity }
      end
    end
  end


  # finish process
  # TODO create activity
  def finish_process
    @idea = @startup.ideas.find_by(id: params[:id])
    respond_to do |format|
      if @idea.idea_process.due_date.nil?
        if @idea.all_stages_finished?
          if @idea.idea_process.update(due_date: Time.now.in_time_zone)
            @idea.create_activity :finish_process, owner: current_emprende_user, recipient: @idea 
    
            format.html { redirect_to startup_idea_path(@startup, @idea), notice: I18n.t('controllers.ideas.finish_process.process_finished_successfully')}
          else
            format.html { redirect_to startup_idea_path(@startup, @idea), notice: I18n.t('controllers.ideas.finish_process.process_finish_error')}
            format.json { render json: @idea.errors, status: :unprocessable_entity }
          end
        else
          format.html { redirect_to startup_idea_path(@startup, @idea), notice: I18n.t('controllers.ideas.finish_process.all_stages_are_not_finish')}
        end
      else
        format.html { redirect_to startup_idea_path(@startup, @idea), notice: I18n.t('controllers.ideas.finish_process.process_already_finish')}
      end
    end
  end

  def advisor_follow_idea
    #byebug
    @idea = @startup.ideas.find_by(id: params[:id])
    if current_advisor.following?(@idea)
      current_advisor.stop_following(@idea)
      flash.now[:notice] =  I18n.t('controllers.ideas.advisor_follow_idea.you_are_not_following')
    else
      current_advisor.follow(@idea)
      flash.now[:notice] =  I18n.t('controllers.ideas.advisor_follow_idea.you_are_following')
    end

    # redirect_to advisor_show_startup_idea_path(@startup, @idea)
  end

  # entrepreneur side

  # def entrepreneur_new
  #   authorize! :admin , @startup
  #   @idea = @startup.ideas.new
  # end

  # La notificación de creación de idea incluya el nombre de la startup “la empresa xxx ha creado la idea xxx”
  #POST create
  def create
    authorize! :admin , @startup
    @idea = @startup.ideas.new(entrepreneur_ideas_params)
    respond_to do |format|
      if @idea.save
        # create a push notification
        # @startup.create_activity :create, owner: current_entrepreneur
        # PusherManagement.send_notifications('advisor_notifications', 'advisor_event', { owner: current_entrepreneur.id, startup: @startup.id, content: current_entrepreneur.first_name + ' ' + current_entrepreneur.last_name + ' ha creado una nueva Startup: ' + @startup.name })
        # redirects
        # @idea.create_activity :create, owner: current_entrepreneur
        # PusherManagement.send_notifications('advisor_notifications', 'advisor_event', { owner: current_entrepreneur.id, link: advisor_show_startup_idea_path(@startup, @idea), target_id: @idea.id, target_class: @idea.class.name, content: current_entrepreneur.first_name + ' ' + current_entrepreneur.last_name + ' ha creado una nueva Idea: ' + @idea.name })
        # PusherManagement.push_message('Idea', 'create', current_entrepreneur, @idea.id, @idea, startup_idea_path(@startup, @idea))
        @idea.create_activity :create, owner: current_emprende_user, recipient: @idea
        format.html { redirect_to startup_path(@startup) , notice: I18n.t('views.startups.create.success')}
        format.js
      else
        # this is not being used
        # format.html { render :entrepreneur_new }
        format.json { render json: @idea.errors, status: :unprocessable_entity }
        format.js{ render 'ideas/registration/create_error'
        }
      end
    end
  end
  #Show Views
  #GET edit
  def edit
    authorize! :admin , @startup
    @idea = @startup.ideas.find_by(id: params[:id])
  end

  #PUT update
  def update
    authorize! :admin , @startup
    @idea = @startup.ideas.find_by(id: params[:id])
    if @idea.update(entrepreneur_ideas_profile_params)
      redirect_to startup_idea_path(@startup,@idea) , notice: I18n.t('controllers.ideas.update.update_succeed')
    else
      render :edit
    end
  end

  # GET edit_registration
  def edit_registration
    authorize! :admin , @startup
    @idea = @startup.ideas.find_by(id: params[:id])
  end

  # PUT update_registration
  def update_registration
    authorize! :admin , @startup
    @idea = @startup.ideas.find_by(id: params[:id])
    if @idea.update(entrepreneur_ideas_params)
      respond_to do |format|
        format.html {redirect_to startup_path(@startup) , notice: I18n.t('controllers.ideas.update_registration.update_succeed')}
        format.js
      end
    else
      render :edit_registration
    end
  end

  # PUT avatar
  def avatar
    authorize! :admin , @startup
    @idea = @startup.ideas.find_by(id: params[:id])
    if @idea.update(entrepreneur_ideas_profile_params)
      respond_to do |format|
        format.js
      end
    else
      flash.now[:notice] = I18n.t('controllers.ideas.avatar.upload_error')
      render :edit
    end
  end

  #GET show
  def show
    if entrepreneur_signed_in?
        entrepreneur_show
        render :entrepreneur_show
    elsif advisor_signed_in?
      advisor_show
      render :advisor_show
    else
      render :visitor_show
    end
  end

private

  def entrepreneur_show
    # authorize! :admin , @startup
    @idea = @startup.ideas.find_by(id: params[:id])
  end

  
  def advisor_show
    @processes = ProcessModel.all
    @idea = @startup.ideas.find_by(id: params[:id])
    # MARK remove one stage active at the same time
    # if @idea.idea_process
      # @stage_active = @idea.has_active_stages
    # end
  end

  def advisor_ideas_params
    params.require(:idea).permit(:is_active)
  end
  
  def entrepreneur_ideas_params
    params.require(:idea).permit(:name,
                                 :description,
                                 :problem_definition,
                                 :idea_type)
  end


  def idea_achievement_params
    params.require(:idea_achievement).permit(:achievement_model_id, :_destroy)
  end

  # def idea_pivot_params
    # params.require(:idea_stage).permit(:stage_model_id)
  # end

  def entrepreneur_ideas_profile_params
    params.require(:idea).permit(:name,
                                 :description,
                                 :problem_definition,
                                 :website,
                                 :avatar,
                                 :idea_type,
                                 :facebook,
                                 :twitter,
                                 images_attributes: [:id, :description, :image, :_destroy])
  end

  def set_startup
    @startup = Startup.find(params[:startup_id])
  end

  def set_entrepreneur
    @entrepreneur = Entrepreneur.find(current_entrepreneur.id)
  end

  def require_active_idea
    unless advisor_signed_in?
      @idea = @startup.ideas.find(params[:id])
      unless @idea.is_active?
        flash[:notice] = I18n.t('controllers.ideas.require_active_idea.inactive_idea')
        redirect_to startup_path(@startup)
      end
    end
  end

  # old method
  # we will require an active idea to complete the profile, this profile will have information about the idea
  # def require_active_idea
    # @entrepreneur = current_entrepreneur
    # @startup = @entrepreneur.startups.find_by(id: params[:startup_id])
    # @idea = @startup.ideas.find_by(id: params[:id])
    # unless @idea.is_active?
      # flash[:error] = I18n.t('views.startups.active_startup')
      # redirect_to entrepreneur_startup_path(@entrepreneur,@startup)
    # end
  # end
end
