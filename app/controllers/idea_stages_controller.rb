# == Schema Information
#
# Table name: idea_stages
#
#  id              :integer          not null, primary key
#  stage_model_id  :integer
#  idea_process_id :integer
#  status          :integer          default(0)
#  start_date      :datetime
#  due_date        :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  is_pivot        :boolean          default(FALSE)
#

#
class IdeaStagesController < ApplicationController
  before_action :authenticate_advisor!, only: [:update_status]
  before_action :set_idea

  def update_status
    respond_to do |format|

      # redirects to the view
      format.html { redirect_to startup_idea_path(@idea.startup, @idea) }

      if change_status_params[:status] != 'finish'
        if @idea_stage.update(change_status_params)
          # other stages are not currently active so we can update the current stage
          #@idea_stage.create_activity :update_status, parameters: {id: @idea_stage.id},owner: current_advisor, recipient: @idea
          @idea_stage.create_activity :update_status,owner: current_advisor, recipient: @idea, parameters: {idea_stage: @idea_stage}
          flash[:notice] = I18n.t('controllers.idea_stages.update_status.update_stage_successfully', stage: @idea_stage.model.name)
        else
          # we have an error
          flash[:notice] = I18n.t('controllers.idea_stages.update_status.update_stage_error')
        end
      else
        if !@idea_stage.active_activities?
          # you have to insert an error related to active activities
          flash[:notice] = I18n.t('controllers.idea_stages.update_status.activities_uncompleted', stage: @idea_stage.model.name)
        else
          if @idea_stage.update(change_status_params)
            # other stages are not currently active so we can update the current stage
            flash[:notice] = I18n.t('controllers.idea_stages.update_status.update_stage_successfully', stage: @idea_stage.model.name)
            @idea_stage.create_activity :update_status,owner: current_advisor, recipient: @idea, parameters: {idea_stage: @idea_stage}
          else
            # we have an error
            flash[:notice] = I18n.t('controllers.idea_stages.update_status.update_stage_error')
          end
        end
      end
    end
  end

  def change_status_params
    params.require(:idea_stage).permit(:status)
  end

  private

  def set_idea
    @idea_stage = IdeaStage.find(params[:id])
    @idea_process = @idea_stage.idea_process
    @idea = @idea_stage.idea
  end
end
