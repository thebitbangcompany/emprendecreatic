# == Schema Information
#
# Table name: idea_artifacts
#
#  id                :integer          not null, primary key
#  artifact_model_id :integer
#  idea_activity_id  :integer
#  url               :string
#  status            :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string
#  support           :string
#  start_date        :datetime
#  due_date          :datetime
#

class IdeaArtifactsController < ApplicationController
  before_action :authenticate_entrepreneur! , except: [:index,:change_status,:set_as_corrected,:set_as_finished]
  before_action :authenticate_advisor! , only: [:change_status,:set_as_corrected,:set_as_finished]
  before_action :set_activity

  # TODO by the side of the advisor
  def change_status
    @idea_artifact = @activity.idea_artifacts.find_by(id: params[:idea_artifact_id])
    respond_to do |format|
      if @idea_artifact.update(idea_artifact_status_params)
        #create a new notification
        @idea_artifact.create_activity :update_status, owner: current_emprende_user, recipient: @idea , parameters: {idea_activity: @idea_artifact.idea_activity, idea_artifact:@idea_artifact}
        format.js
        flash.now[:notice] = I18n.t('controllers.idea_artifacts.change_status.deliverable_updated_successfully')
      else
        format.json {render json: { error_message: @idea_artifact.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  #TODO by the side of the entrepreneur

  def new
    @idea_artifact = @activity.idea_artifacts.new
  end

  def create
    params[:idea_artifact][:status] = 'start'
    @idea_artifact = @activity.idea_artifacts.new(idea_artifact_params)
    respond_to do |format|
      if @idea_artifact.save
        @artifacts_paginated = @activity.idea_artifacts.order('created_at DESC').paginate(page: params[:artifacts_page], per_page: 2)
        @artifacts =  @activity.idea_artifacts
        @idea_artifact.create_activity :create, owner: current_emprende_user, recipient: @idea , parameters: {idea_activity: @idea_artifact.idea_activity, idea_artifact:@idea_artifact}
        format.js
      else
        format.json {render json: { error_message: @idea_artifact.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @idea_artifact = @activity.idea_artifacts.find_by(id: params[:id])
  end

  def destroy
    @idea_artifact = @activity.idea_artifacts.find_by(id: params[:id])
    respond_to do |format|
      if @idea_artifact.destroy
        @artifacts_paginated = @activity.idea_artifacts.order('created_at DESC').paginate(page: params[:artifacts_page], per_page: 2)
        @artifacts = @activity.idea_artifacts
        format.js
      else
        format.json {render json: { error_message: @idea_artifact.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def index
    @artifacts_paginated = @activity.idea_artifacts.order('created_at DESC').paginate(page: params[:artifacts_page], per_page: 2)
  end

  def update
    #params[:idea_artifact][:status] = 'corrected'
    @idea_artifact = @activity.idea_artifacts.find_by(id: params[:id])
    respond_to do |format|
      if @idea_artifact.update(idea_artifact_params)
        #create a new notification
        format.js
      else
        format.json {render json: { error_message: @idea_artifact.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  #set as corrections by entrepreneur
  def set_as_corrections
    @idea_artifact = IdeaArtifact.find(params[:artifact_id])
    respond_to do |format|
      unless @idea_artifact.status == 'finish'
        @idea_artifact.status = :corrections
        @idea_artifact.create_activity :update_status, owner: current_emprende_user, recipient: @idea , parameters: {idea_activity: @idea_artifact.idea_activity, idea_artifact:@idea_artifact}
        @idea_artifact.save
        format.js
      else
        flash.now[:notice] = I18n.t('controllers.idea_artifacts.set_as_corrections.deliverable_finished')
        format.js {"idea_artifacts/update.js.erb"}
      end
    end
  end

  #set as corrections by advisor
  def set_as_corrected
    @idea_artifact = IdeaArtifact.find(params[:artifact_id])
    respond_to do |format|
      unless @idea_artifact.status == 'finish'
        @idea_artifact.status = :corrected
        @idea_artifact.create_activity :update_status, owner: current_emprende_user, recipient: @idea , parameters: {idea_activity: @idea_artifact.idea_activity, idea_artifact:@idea_artifact}
        @idea_artifact.save
        format.js
      else
        flash.now[:notice] = I18n.t('controllers.idea_artifacts.set_as_corrected.deliverable_finished')
        format.js {"idea_artifacts/update.js.erb"}
      end
    end
  end

  #set as finished by entrepreneur
  def set_as_finished
    @idea_artifact = IdeaArtifact.find(params[:artifact_id])
    respond_to do |format|
      unless @idea_artifact.status == 'finish'
        @idea_artifact.status = :finish
        @idea_artifact.create_activity :update_status, owner: current_emprende_user, recipient: @idea , parameters: {idea_activity: @idea_artifact.idea_activity, idea_artifact:@idea_artifact}
        @idea_artifact.save
        format.js
      else
        flash.now[:notice] = I18n.t('controllers.idea_artifacts.set_as_finished.deliverable_finished')
        format.js {"idea_artifacts/update.js.erb"}
      end
    end
  end


  #parameters to create a new deliverable
  def idea_artifact_params
    params.require(:idea_artifact).permit(:artifact_model_id,
                                          :name,
                                          :url,
                                          :status,
                                          :support,
                                          :tag_list,
                                          :_destroy)
  end

  def idea_artifact_status_params
    params.require(:idea_artifact).permit(:status)
  end


  private

  def set_activity
    @activity = IdeaActivity.find(params[:idea_activity_id])
    @idea = @activity.idea
    @startup = @activity.startup
  end

  # def set_startup
  #  @idea = Idea.find(params[:idea_id])
  #  @startup = @idea.startup
  # end
end
