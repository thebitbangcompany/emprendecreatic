class ActivitiesController < ApplicationController
  before_action :authenticate_emprende_user
  layout :layout_by_resource

  def read
    path = params[:path]
    activity = PublicActivity::Activity.find(params[:id])
    activity.mark_as_read!(for: current_emprende_user)
    redirect_to path
  end

  def index
    params[:page] = params[:page] || 1
    @activities = current_emprende_user.activities.paginate(:page => params[:page], :per_page => 10)
  end

  def append_activities
    @activities = current_emprende_user.activities.paginate(:page => params[:page], :per_page => 5)
  end

  def update_activities
    @activities = current_emprende_user.activities.paginate(:page => params[:page], :per_page => 5)
  end

  def mark_all_as_read
    PublicActivity::Activity.mark_as_read! :all, :for => current_emprende_user
    redirect_to action: "index"
  end

  protected
    def layout_by_resource
    if devise_controller?
      "devise"
    end
  end
end
