class InvitationsController < ApplicationController
  before_action :authenticate_entrepreneur!

  # TODO create activity
  def create
    new_email = invitation_params[:email]
    startup_id = invitation_params[:startup_id]
    if Entrepreneur.find_by(email:new_email)
      flash.now[:notice] = I18n.t('controllers.invitations.create.email_in_use')
      render 'invitations/error'
    else
      @entrepreneur = Entrepreneur.invite!(:email => invitation_params['email'])
      @startup = Startup.find(startup_id)
      @startup_entrepreneur = @startup.startup_entrepreneurs.new(entrepreneur_id: @entrepreneur.id)
      @startup_entrepreneur.save
      @startup.create_activity :create_member, owner: current_emprende_user, recipient: @startup 
      flash.now[:notice] = "#{I18n.t('controllers.invitations.create.invitation_sent')} #{new_email}"
    end
  end

  def invitation_params
    params.require(:invitation).permit(:email,:startup_id)
  end
end
