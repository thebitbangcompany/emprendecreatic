module ApplicationHelper
  def full_title(page_title = '')
    base_title = 'Emprende Creatic'
    if page_title.empty?
      base_title
    else
      page_title + '|' + base_title
    end
  end

  def active_class_tag(action)
    if current_page?(action: action)
      return "active"
    end
  end

  def allow_manage_entrepreneur?(entrepreneur)
    current_entrepreneur == entrepreneur
  end

  def allow_manage_advisor?(advisor)
    current_advisor == advisor
  end

  def avatar_tag(user,styles="")
    if  !user.nil?
      if !user.avatar_url.nil?
        image_tag(user.avatar_url(:thumb), class: "img-responsive #{styles}")
      else
        image_tag( "default-user.png", class: "img-responsive #{styles}")
      end
    else
      image_tag( "default-user.png", class: "img-responsive #{styles}")
    end
  end

  def avatar_mini_tag(user,styles="")
    if user.avatar_url
      image_tag(user.avatar_url(:mini_thumb), class: "mini-navbar-thumb img-responsive #{styles}")
    else
      image_tag( "default-user.png", class: "mini-navbar-thumb img-responsive #{styles}")
    end
  end

  def startup_avatar_tag(startup,styles="")
    if startup.avatar_url
      image_tag(startup.avatar_url(:thumb), class: "avatar img-responsive #{styles}")
    else
      image_tag( "default-startup.png", class: "avatar img-responsive #{styles}")
    end
  end


  def full_name(user)
    user.first_name + ' ' +user.last_name
  end


  def country_name(country_code)
    country = ISO3166::Country[country_code]
    country.translations[I18n.locale.to_s] || country.name
  end

end