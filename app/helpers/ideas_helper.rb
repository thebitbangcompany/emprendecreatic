# == Schema Information
#
# Table name: ideas
#
#  id                 :integer          not null, primary key
#  name               :string           default(""), not null
#  description        :text             default(""), not null
#  problem_definition :text             default(""), not null
#  startup_id         :integer
#  is_private         :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  advisor_id         :integer
#  is_active          :boolean          default(FALSE)
#  avatar             :string
#  website            :string
#  idea_type          :string
#  facebook           :string
#  twitter            :string
#  drive_folder       :string
#  drive_folder_link  :string
#  deleted_at         :datetime
#

module IdeasHelper
  def avatar_achivement_tag(achivement,styles="")
    if user.avatar_url
      image_tag(user.avatar_url(:thumb), class: "img-responsive  #{styles}")
    else
      image_tag( "default-idea.png", class: "img-responsive   #{styles}")
    end
  end
  def avatar_idea_tag(user,styles="")
    if user.avatar_url
      image_tag(user.avatar_url(:thumb), class: "img-responsive  #{styles}")
    else
      image_tag( "default-idea.png", class: "img-responsive   #{styles}")
    end
  end
  def avatar_mini_idea_tag(user,styles="")
    if user.avatar_url
      image_tag(user.avatar_url(:mini_thumb), class: "img-responsive  #{styles}")
    else
      image_tag( "default-idea.png", class: "img-responsive mini_thumb  #{styles}")
    end
  end
end
