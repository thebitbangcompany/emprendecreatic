# == Schema Information
#
# Table name: startups
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  vision     :string
#  mission    :string
#  email      :string
#  phone      :string
#  website    :string
#  industry   :string
#  is_active  :boolean          default(TRUE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  avatar     :string
#  facebook   :string
#  linkedin   :string
#  twitter    :string
#  city       :string
#  country    :string
#  deleted_at :datetime
#

module StartupsHelper
  def avatar_startup_tag(user,styles="")
    if user.avatar_url
      image_tag(user.avatar_url(:thumb), class: "img-responsive img-circle #{styles}")
    else
      image_tag( "default_startup.png", class: "img-responsive  img-circle #{styles}")
    end
  end
  def avatar_mini_startup_tag(user,styles="")
    if user.avatar_url
      image_tag(user.avatar_url(:mini_thumb), class: "img-responsive img-circle #{styles}")
    else
      image_tag( "default_startup.png", class: "img-responsive mini_thumb img-circle #{styles}")
    end
  end
end
