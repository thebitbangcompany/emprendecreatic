# == Schema Information
#
# Table name: idea_artifacts
#
#  id                :integer          not null, primary key
#  artifact_model_id :integer
#  idea_activity_id  :integer
#  url               :string
#  status            :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string
#  support           :string
#  start_date        :datetime
#  due_date          :datetime
#

module IdeaArtifactsHelper
  def artifact_ribbon_tag(artifact)
    style = ""
    if artifact.status == "start"
      style = "light"
    elsif artifact.status == "corrections"
      style = "yellow"
    elsif artifact.status == "corrected"
      style = "base-alt"
    else
      style = "base-alt"
    end
  end
end
