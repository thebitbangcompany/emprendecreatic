module ConversationsHelper
  def unread_conversations_badge
    if unread_conversations.count > 0
      content_tag :span, unread_conversations.count, class: "badge pull-right badge-red"
    end
  end
  def unread_activities_badge
    if current_emprende_user.unread_activities_count > 0
      content_tag :span, current_emprende_user.unread_activities_count, class: "badge pull-right badge-red"
    end
  end
end
