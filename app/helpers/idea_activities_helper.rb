# == Schema Information
#
# Table name: idea_activities
#
#  id                :integer          not null, primary key
#  activity_model_id :integer
#  idea_stage_id     :integer
#  status            :integer          default(0)
#  start_date        :datetime
#  due_date          :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deadline_date     :datetime
#

module IdeaActivitiesHelper
  def status_icon(status)
    if status == "finish"
      image = image_tag "finish-icon.png"
    elsif status == "start"
      image = image_tag "clock-icon.png"
    end
    image
  end

  def activity_path(activity)
    if IdeaActivity.exists?(activity)
      idea_activity_show_path(activity.id)
    else
      "#"
    end
  end


end
