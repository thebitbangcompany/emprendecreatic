# == Schema Information
#
# Table name: idea_stages
#
#  id              :integer          not null, primary key
#  stage_model_id  :integer
#  idea_process_id :integer
#  status          :integer          default(0)
#  start_date      :datetime
#  due_date        :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  is_pivot        :boolean          default(FALSE)
#

module IdeaStagesHelper

  def fa_status(status)
    if status == "finish"
      "check-circle-o"
    elsif status == "start"
      "clock-o"
    else
      "circle"
    end
    
  end

  def stage_dates(stage)
    if stage.status == "finish"
      content_tag :span, "#{stage.due_date.strftime("%d %b %Y %H:%M:%S")}", class: "text-right stage-dates"
    elsif stage.status == "start"
      content_tag :span, "Fecha Inicio: #{stage.start_date.strftime("%d %b %Y %H:%M:%S")}", class: "stage-dates"
    end
  end

  def date_formated(date)
    date.strftime("%d %b %Y %H:%M:%S") if date
  end

  def idea_stage_tag(id)
    IdeaStage.find(id)
  end

end
