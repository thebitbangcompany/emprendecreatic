# == Schema Information
#
# Table name: entrepreneurs
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  avatar                 :string
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#  country                :string
#  city                   :string
#  deleted_at             :datetime
#

module EntrepreneursHelper
  def entrepreneur_message_button(entrepreneur)
    unless current_entrepreneur == entrepreneur
      button_tag "Enviar Mensaje", class: "btn btn-default btn-width" ,"data-target" => "#messageForm", "data-toggle" => "modal", "data-type" => "Entrepreneur", "data-email" => @entrepreneur.email , :type => "button","data-name"=>@entrepreneur.name 
    end
  end
  def avatar_idea_tag(user,styles="")
    if user.avatar_url
      image_tag(user.avatar_url(:thumb), class: "img-responsive  #{styles}")
    else
      image_tag( "default-idea.png", class: "img-responsive #{styles}")
    end
  end
  def entrepreneur_name_tag(entrepreneur)
    if entrepreneur.name == " "
      entrepreneur.email
    else
      entrepreneur.name
    end
  end
end
