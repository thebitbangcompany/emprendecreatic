# == Schema Information
#
# Table name: advisors
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  occupation             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  deleted_at             :datetime
#  avatar                 :string
#  is_expert              :boolean          default(FALSE), not null
#  authentication_token   :string
#

module AdvisorsHelper
  def advisor_message_button(advisor)
    unless current_advisor == advisor
      button_tag "Enviar Mensaje", class: "btn btn-default btn-width" ,"data-target" => "#messageForm", "data-toggle" => "modal", "data-type" => "Advisor", "data-email" => advisor.email , :type => "button","data-name"=>advisor.name 
    end
  end
  def advisor_is_owner_of(comment)
    if current_emprende_user.class.name == "Advisor" && comment.user_id == current_emprende_user.id
      return true
    end
  end
end
