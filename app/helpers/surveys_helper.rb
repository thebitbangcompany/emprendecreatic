module SurveysHelper

  def get_value_answer(startup, question, star)
    answer = startup.answers.where(question: question, advisor: current_advisor).first

    if !answer.nil? and star <= answer.value
      return true
    else
      return false
    end
  end

  def get_message_answer(startup)
    response = Hash.new
    answer = startup.answers.where(advisor: current_advisor).first

    if !answer.nil? && answer.answered
      response['msg'] = "Calificada"
      response['disabled'] = true
      response['status'] = "qualified"

    elsif !answer.nil? && !answer.answered
      response['msg'] = "La conozco"
      response['hidden'] = "hidden"
      response['status'] = "unknow"
    elsif answer.nil?
      response['msg'] ="No la conozco"
      response['status'] = "visible"
    end
    return response
  end

end
