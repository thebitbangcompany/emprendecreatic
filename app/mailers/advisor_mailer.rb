require 'socket'

class AdvisorMailer < ApplicationMailer


  # include internacialization
  def welcome_email(advisor)
    @advisor = advisor
    @url = APP_CONFIG['host']
    mail(to: @advisor.email, subject: I18n.t('advisor.mailer.welcome.subject'))
  end

end
