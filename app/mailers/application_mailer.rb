class ApplicationMailer < ActionMailer::Base
  default from: "admin@emprendecreatic.com"
  layout 'mailer'
end
