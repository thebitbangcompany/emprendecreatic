class PlatformMailer < ApplicationMailer

#reply email from admin to visitor's question

  def question_reply(question)
    @question = question.issue
    @reply = question.reply
    @url = APP_CONFIG['host']
    mail(to: question.email , subject: "Re: #{question.issue}")
  end



end
