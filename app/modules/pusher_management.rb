class PusherManagement
  def self.send_notifications (channel, event, message)
    Pusher.trigger(channel, event, {
      message: message
    })
  end

  # def self.push_message resource, action, owner, id, obj, link, target = []
  #   msg = { resource: resource,
  #           action: action,
  #           owner: owner,
  #           id: id,
  #           obj: obj,
  #           link: link,
  #           target: target}

  #   $redis.publish 'rt-change', msg.to_json
  # end
  def self.push_message(message)
    msg = { resource: message}
    $redis.publish 'rt-change', msg.to_json
  end

end