require 'grape-swagger'
require 'grape'

# Grape API configuration
class API < Grape::API
  prefix 'api'
  version 'v1', using: :path

  # helpers do
  # returns 401 if there's no current user
  # def authenticate!
  # error!('Unauthorized. Invalid or expired token.', 401) unless current_user
  # end
  # def current_user
  # find token. Check if valid.
  # token = ApiKey.where(access_token: params[:token]).first
  # if token && !token.expired?
  # @current_user = Candidate.find(token.user_id)
  # else
  # false
  # end
  # end
  # end
  mount V1::Modules::AccessToken
  mount V1::Modules::Root
  # Adds the swagger documentation to your
  # api. You only need this once, not in
  # every sub module
  add_swagger_documentation(
      base_path: "/",
      hide_documentation_path: true,
      api_version: 'v1',
      markdown: true
  )
end