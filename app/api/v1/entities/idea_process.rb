module V1
  module Entities
    class IdeaProcess < Grape::Entity
      expose :id, documentation: { type: "string", desc: "idea id", example: '1' }
      expose :process_model_id
      expose :idea_stages, using: API::V1::Entities::IdeaStage
    end
  end
end
