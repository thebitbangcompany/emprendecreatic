module V1
  module Entities
    class IdeaStage < Grape::Entity
      expose :id, documentation: { type: "string", desc: "idea id", example: '1' }
      expose :stage_model_id, documentation: { type: "string", desc: "idea stage id", example: '1'}
      expose :name, documentation: {type: "string", desc: "idea stage name", example: 'customer validation'} do |idea_stage, options|
        idea_stage.stage_model.name
      end
      expose :start_date, documentation: {type: "string", desc: "idea stage start date", example: '22-09-1991'} do |idea_stage, options|
        ::ActiveSupport::TimeZone['UTC'].parse(idea_stage.start_date.to_s).to_i
      end
      expose :due_date, documentation: {type: "string", desc: "idea stage due date", example: '22-09-1991'} do |idea_stage, options|
        ::ActiveSupport::TimeZone['UTC'].parse(idea_stage.due_date.to_s).to_i
      end
    end
  end
end