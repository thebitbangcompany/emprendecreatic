module V1
  module Entities
    class Idea < Grape::Entity
      expose :id, documentation: { type: "string", desc: "idea id", example: '1' }
      expose :name, documentation: {type: "string", desc: "idea name", example: "Emprende Creatic"}
      expose :startup_name, documentation: { type: "string" , desc: "startup_name", example: "TBBC" } do |idea, options|
          idea.startup.name
    end
    end
  end
end
