module V1
  module Entities
    class Process < Grape::Entity
      expose :id, documentation: { type: "string", desc: "process id", example: '1' }
      expose :name, documentation: {type: "string", desc: "process name", example: "Lean"}
      expose :stage_models, using: API::V1::Entities::StageModel
    end
  end
end
