module V1
  module Entities
    class StageModel < Grape::Entity
      expose :id, documentation: { type: "string", desc: "stage_model id", example: '1' }
      expose :name, documentation: {type: "string", desc: "stage_model name", example: "Customer discovery"}
    end
  end
end
