module V1
  module Entities
    class AccessToken < Grape::Entity
      expose :authentication_token, documentation: { type: "string", desc: "Real access token", example: "most_secure_password" }
    end
  end
end
