module V1
  module Entities
    class IdeaChart < Grape::Entity
      expose :id, documentation: { type: "string", desc: "idea id", example: '1' }
      expose :name, documentation: {type: "string", desc: "idea name", example: "Emprende Creatic"}
      expose :idea_process, using: API::V1::Entities::IdeaProcess
    end
  end
end
