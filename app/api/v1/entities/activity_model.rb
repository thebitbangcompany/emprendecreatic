module V1
  module Entities
    class ActivityModel < Grape::Entity
      expose :id, documentation: { type: "string", desc: "activity_model id", example: '1' }
      expose :name, documentation: {type: "string", desc: "activity_model name", example: "Lean canvas"}
    end
  end
end