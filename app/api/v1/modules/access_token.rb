module V1
  module Modules
    class AccessToken < Grape::API
      format :json
      content_type :json, 'application/json'

      #
      # Login
      #

      resource :auth do
        desc 'Returns a token by authenticating user email and password credentials', {
            # entity: V1::Entities::AccessToken,
            notes: <<-NOTE
            ### Description
                          It login the user and returns its current representation with a JWT. \n

                          ### Example successful response

                              {
                                "access_token": "the_most_secure_token"
                              }
            NOTE
        }
        params do
          requires :email, type: String
          requires :password, type: String
        end
        post 'login' ,http_codes: [[200, "Successful"], [400, "Invalid parameter in entry"], [404, "Not found"],]  do
          advisor = ::Advisor.where(email: params[:email]).last
          if !advisor.nil?
            if advisor.valid_password?(params[:password])
                  token = { authentication_token: advisor.create_authentication_token}
                  present token, with: V1::Entities::AccessToken
            else
              error!('Invalid email/password', 400)
            end
          else
            error!('Invalid email/password', 404)
          end
        end
      end
    end
  end
end