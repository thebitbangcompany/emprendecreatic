module V1
  module Modules
    class Process < Grape::API

      # before_validation do
        # advisor_authenticated
      # end

      format :json
      content_type :json, 'application/json'

      helpers do
        params :pagination do
          optional :page, type: Integer
          optional :per_page, type: Integer
        end
      end


        resource :processes  do

        desc 'returns all existent processes and their stages', {
            entity: V1::Entities::Process,
            notes: <<-NOTES
                        Returns all existent processes
            NOTES
        }

        get '/', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          processes  = ::ProcessModel.all
          present processes, with: V1::Entities::Process
        end


        desc 'returns information about the process executed by a startup', {
            entity: V1::Entities::IdeaChart,
            notes: <<-NOTES
                  returns information about the process executed by a startup
            NOTES
        }

        params do
          requires :idea_id, type: Integer, desc: 'Idea id.'
        end

        get '/get_idea_process', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          idea  = ::Idea.find_by_id(params[:idea_id])

          present idea, with: V1::Entities::IdeaChart
        end

        # returns information about the average of each process model
        desc 'returns the information related to the duration of each process', {
            notes: <<-NOTES
                              returns information about the duration of the  process executed by a startup
            NOTES
        }

        params do
          requires :process_id, type: Integer, desc: 'process id.'
        end

        get '/process_duration', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          process  = ::ProcessModel.find_by(id: params[:process_id])
          {"duration" => process.process_duration}
        end

        # returns information about the average of each stage of the process
        desc 'returns the information related to the average of each stage of the process', {
            #entity: V1::Entities::StageDuration,
            notes: <<-NOTES
                                          returns information about the duration of the  process executed by a startup
            NOTES
        }

        params do
          requires :process_id, type: Integer, desc: 'process id.'
        end

        get '/stages_process_duration', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          process  = ::ProcessModel.find_by(id: params[:process_id])
          process.stages_average
        end

        # returns information about the progress percentage of all ideas
        desc 'returns the information related to the progress percentaje of all ideas', {
            #entity: V1::Entities::StageDuration,
            notes: <<-NOTES
                                                      returns the information related to the progress percentaje of all ideas
            NOTES
        }

        params do
          requires :process_id, type: Integer, desc: 'process id.'
          use :pagination
        end

        get '/ideas_progress_percentage', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          page = params[:page] || 1
          per_page = params[:per_page] || 10
          process  = ::ProcessModel.find_by(id: params[:process_id])
          ideas_info = process.idea_progress_percentage.paginate(:page => page, :per_page => per_page)
          header 'total_pages', ideas_info.total_pages.to_s
          ideas_info
        end

        # returns information about the desertion of ideas in the process
        desc 'returns information about the desertion of ideas in the process', {
            #entity: V1::Entities::StageDuration,
            notes: <<-NOTES
                                                                  returns information about the desertion of ideas in the process
            NOTES
        }

        params do
          requires :process_id, type: Integer, desc: 'process id.'
          use :pagination
        end

        get '/ideas_desertion', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          page = params[:page] || 1
          per_page = params[:per_page] || 10

          #
          process  = ::ProcessModel.find_by(id: params[:process_id])
          ideas_info = process.process_desertion.paginate(:page => page, :per_page => per_page)
          header 'total_pages', ideas_info.total_pages.to_s
          ideas_info
        end

        # get the number of ideas per stage for a process
        desc 'returns the number of ideas per stage for a process', {
            notes: <<-NOTES
                                                                              returns the number of ideas per stage for a process
            NOTES
        }
        params do
          requires :process_id, type: Integer, desc: 'process id.'
        end

        get '/number_ideas_stages', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          #
          process  = ::ProcessModel.find_by(id: params[:process_id])
          ideas_number = process.number_ideas_stages
          ideas_number
        end

        # get ideas and the current stage
        desc 'returns ideas and the current stage', {
            notes: <<-NOTES
                                                                                          returns ideas and the current stage
            NOTES
        }
        params do
          requires :process_id, type: Integer, desc: 'process id.'
          use :pagination
        end

        get '/ideas_stages', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          page = params[:page] || 1
          per_page = params[:per_page] || 10
          #
          process  = ::ProcessModel.find_by(id: params[:process_id])
          ideas_info = process.ideas_information.paginate(:page => page, :per_page => per_page)
          header 'total_pages', ideas_info.total_pages.to_s
          ideas_info
        end


        # get ideas and the current activity
        desc 'returns ideas and the current activity', {
            notes: <<-NOTES
                                                                                                      returns ideas and the current stage
            NOTES
        }
        params do
          requires :process_id, type: Integer, desc: 'process id.'
          use :pagination
        end

        get '/ideas_activities', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          page = params[:page] || 1
          per_page = params[:per_page] || 10
          #
          process  = ::ProcessModel.find_by(id: params[:process_id])
          ideas_info = process.ideas_information.paginate(:page => page, :per_page => per_page)
          header 'total_pages', ideas_info.total_pages.to_s
          ideas_info
        end



      end

    end
  end
end