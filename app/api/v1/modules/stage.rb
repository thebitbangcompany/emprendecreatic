module V1
  module Modules
    class Stage < Grape::API
      # before_validation do
        # advisor_authenticated
      # end

      format :json
      content_type :json, 'application/json'


      resource :stage  do

        desc 'returns all existent ideas per stage', {
            entity: V1::Entities::Idea,
            notes: <<-NOTES
                                    returns all existent ideas per stage
            NOTES
        }

        params do
          requires :stage_id, type: Integer, desc: 'Stage id.'
        end

        get '/get_stage_ideas', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type 'text/json'
          ideas = ::Idea.find_by_stage(params[:stage_id])
          present ideas, with: V1::Entities::Idea
        end

        desc 'return all existent activities per stage', {
            entity: V1::Entities::ActivityModel,
            notes: <<-NOTES
                  returns all existent activities per stage
            NOTES
        }

        params do
          requires :stage_id, type: Integer, desc: 'Stage id.'
        end

        get '/get_activities_stage', http_codes: [[200, "Successful"], [401, "Unauthorized"]] do
          content_type 'text/json'
          activities = ::StageModel.find_by_id(params[:stage_id]).activity_models
          present activities, with: V1::Entities::ActivityModel
        end

        # returns information about the average of each activity of the stage
        desc 'returns the information related to the average of each activity of the stage', {
            #entity: V1::Entities::StageDuration,
            notes: <<-NOTES
                                                      returns information about the duration of the  process executed by a startup
            NOTES
        }

        params do
          requires :stage_id, type: Integer, desc: 'stage id.'
        end

        get '/activities_stage_duration', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          stage  = ::StageModel.find_by(id: params[:stage_id])
          stage.activities_average
        end


        # returns information about the ideas that have been completed the stages
        desc 'returns information about the ideas that have been completed the stages', {
            #entity: V1::Entities::StageDuration,
            notes: <<-NOTES
                                                                  returns information about the ideas that have been completed the stages
            NOTES
        }

        params do
          requires :process_id, type: Integer, desc: 'process id.'
        end

        get '/ideas_stage_completed', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type "text/json"
          process = ::ProcessModel.find_by(id: params[:process_id])
          {"total_ideas" => process.ideas_in_progress, "ideas_stage_completed" => process.ideas_stage_completed}
        end

        # get the number of ideas per stage for a process
        desc 'returns the number of ideas per activity for a stage', {
            notes: <<-NOTES
                                                                                          returns the number of ideas per activity for a stage
            NOTES
        }
        params do
          requires :stage_id, type: Integer, desc: 'stage id.'
        end

        get '/number_ideas_activities', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          #
          stage = ::StageModel.find_by(id: params[:stage_id])
          ideas_number = stage.number_ideas_activities
          ideas_number
        end

      end
    end
  end
end