module V1
  module Modules
    class Activity < Grape::API
      # before_validation do
      # advisor_authenticated
      # end

      format :json
      content_type :json, 'application/json'


      resource :activity  do

        desc 'returns all existent ideas per stage', {
            entity: V1::Entities::Idea,
            notes: <<-NOTES
                                                returns all existent ideas per stage
            NOTES
        }

        params do
          requires :activity_id, type: Integer, desc: 'Stage id.'
        end

        get '/get_activity_ideas', http_codes: [ [200, "Successful"], [401, "Unauthorized"] ] do
          content_type 'text/json'
          ideas = ::Idea.find_by_activity(params[:activity_id])
          present ideas, with: V1::Entities::Idea
        end

      end
    end
  end
end