module V1
  module Modules
    # class to  expose an API
    class Root < Grape::API
      format :json

      rescue_from :all, :backtrace => true
      # error_formatter :json, API::ErrorFormatter

      before do
        error!('401 Unauthorized', 401) unless advisor_authenticated
      end

      helpers do

        # avoid warden auth
        # def warden
          # env['warden']
        # end

        def advisor_authenticated
          begin
            token = request.headers['Authorization'].split(' ').last
            decoded_token =  Base64.decode64(token)
            @advisor = ::Advisor.where(authentication_token: decoded_token).last
          rescue
            false
          end
        end

        # def current_user
          # warden.user || @advisor
        # end
      end

      # here my api information
      mount V1::Modules::Process
      mount V1::Modules::Stage
      mount V1::Modules::Activity

    end
  end
end