$(document).ready ->
  $body = $('body')
  $(document).on
    ajaxStart: ->
      $body.addClass 'loading'
      return
    ajaxStop: ->
      $body.removeClass 'loading'
      return
