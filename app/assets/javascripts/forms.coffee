$(document).on 'shown.bs.modal' ,->
  $("#new_idea").enableClientSideValidations()
  $(".simple_form.edit_idea").enableClientSideValidations()
  $('.entrepreneur_deliverable_form').enableClientSideValidations()


$(document).ready ->
  $(".btn-register-idea").on "click", (e)->
    $("#new_idea").trigger "reset"
    return
  $(".entrepreneur_artifact_activity_button").on "click", (e)->
    $(".entrepreneur_deliverable_form").trigger "reset"
    return
  $(".simple_form.new_comment").enableClientSideValidations()