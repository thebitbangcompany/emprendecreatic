// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require turbolinks
//= require jquery
//= require jquery_ujs
//= require jquery.remotipart
//= require jquery.turbolinks
//= require jquery.form
//= require bootstrap-sprockets
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl
//= require jquery-ui
//= require cocoon
//= require pusher.min
//= require bootstrap-switch
//= require video
//= require socket.io-1.4.5
//= require rails.validations
//= require rails.validations.simple_form
//= require best_in_place
//= require best_in_place.jquery-ui
//= require tag-it
//= require magnific-popup
//= require_self
//= require_tree .

/*
$(document).ready(function(){
 $(".dropdown-button").dropdown({ hover: true });
 //$(".button-collapse").sideNav();
});
*/


jQuery(function($) {
    $("tr[data-link]").click(function() {
        window.location = $(this).data('link');
    });
});