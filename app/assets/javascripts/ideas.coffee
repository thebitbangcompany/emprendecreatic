# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('.test-popup-link').magnificPopup type: 'image'
  # load Drive API credentials
  # we must migrate these credentials to other account.
  # project this is a browser key
  developerKey = 'AIzaSyDDXu0AlhyvV90ctK0n2zJ-TjBAk_tR2p8'
  # The Client ID obtained from the Google Developers Console. Replace with your own Client ID.
  clientId = '236642561331-mhrkpikiqeoircd0kv7f1nacb2helj1h.apps.googleusercontent.com'
  # Replace with your own App ID. (Its the first number in your Client ID)
  appId = '236642561331'
  # Scope to use to access user's photos.
  scope = [ 'https://www.googleapis.com/auth/drive' ]

  # is the picker loaded
  pickerApiLoaded = false
  # toker after the user accepts connect our application with google drive
  oauthToken = undefined
  # Use the API Loader script to load google.picker and gapi.auth.

  onApiIdeaLoad = ->
    gapi.load 'auth', 'callback': onAuthApiIdeaLoad
    gapi.load 'picker', 'callback': onPickerApiLoad
    return

  onAuthApiIdeaLoad = ->
    window.gapi.auth.authorize {
      'client_id': clientId
      'scope': scope
      'immediate': false
    }, handleAuthIdeaResult
    return

  onPickerApiLoad = ->
    pickerApiLoaded = true
    createIdeaPicker()
    return

  handleAuthIdeaResult = (authResult) ->
    if authResult and !authResult.error
      oauthToken = authResult.access_token
      createIdeaPicker()
    return

  # Create and render a Picker
  createIdeaPicker = ->
    if pickerApiLoaded and oauthToken

      docsView = (new (google.picker.DocsView)).setIncludeFolders(true).setMimeTypes('application/vnd.google-apps.folder').setSelectFolderEnabled(true)

      picker = (new (google.picker.PickerBuilder)).addView(docsView)
      .setOAuthToken(oauthToken).setDeveloperKey(developerKey)
      .setCallback(pickerIdeaCallback)
      #.setCallback(pickerEditCallback)
      .build()
      picker.setVisible true
    return

  # Create and render a Picker for edit
  onApiIdeaEditLoad = ->
    gapi.load 'auth', 'callback': onAuthApiIdeaEditLoad
    gapi.load 'picker', 'callback': onPickerIdeaEditApiLoad
    return

  onAuthApiIdeaEditLoad = ->
    window.gapi.auth.authorize {
      'client_id': clientId
      'scope': scope
      'immediate': false
    }, handleAuthIdeaEditResult
    return

  onPickerIdeaEditApiLoad = ->
    pickerApiLoaded = true
    createEditIdeaPicker()
    return

  handleAuthIdeaEditResult = (authResult) ->
    if authResult and !authResult.error
      oauthToken = authResult.access_token
      createEditIdeaPicker()
    return


  createEditIdeaPicker = ->
    if pickerApiLoaded and oauthToken
      docsView = (new (google.picker.DocsView)).setIncludeFolders(true).setMimeTypes('application/vnd.google-apps.folder').setSelectFolderEnabled(true)
      picker = (new (google.picker.PickerBuilder)).addView(docsView)
      .setOAuthToken(oauthToken).setDeveloperKey(developerKey)
      .setCallback(pickerEditCallback)
      .build()
      picker.setVisible true
    return

  # create a callback for this action
  # we need a callback for the picker to get the file link
  pickerIdeaCallback = (data) ->
    url = ''
    fieldId = ''
    if data.action == google.picker.Action.PICKED
      console.log data
      fileId = data.docs[0].id
      url = data.docs[0].url
      $('#active_idea_drive_folder_input').val fileId
      $('#active_idea_drive_folder_link_input').val url

    # notification to my user
    if url != ''  &&  fileId  != ''
       $('#folder-picker-div').html ''
       success_div = $('<div></div>').addClass('folder-drive-picker-success').hide()
       success_alert_div = $('<div></div>').addClass('folder-drive-picker-form-alert-div alert-success').attr 'role', 'alert'
       success_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
       success_message_span = $('<span></span>').text('Se ha añadido la carpeta exitosamente')
       success_div.append success_icon_span
       success_div.append success_message_span
       success_div.append success_alert_div
       $('#folder-picker-div').append success_div
       success_div.show 'slow'
    else
      $('#folder-picker-div').html ''
      success_div = $('<div></div>').addClass('folder-drive-picker-error').hide()
      success_alert_div = $('<div></div>').addClass('folder-drive-picker-form-alert-div alert-danger').attr 'role', 'alert'
      success_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
      success_message_span = $('<span></span>').text('Se ha presentado un error al añadir la carpeta, por favor verifica nuevamente')
      success_div.append success_icon_span
      success_div.append success_message_span
      success_div.append success_alert_div
      $('#folder-picker-div').append success_div
      success_div.show 'slow'
    return


  # click the link file
  $('#activateIdeaModal').on 'show.bs.modal', (e) ->
    $('#link_drive_folder').click ->
      # we are loading the drive api
      onApiIdeaLoad()
      return
    return

  # verify if you provide the drive link
  $('#activate_startup_idea_form').submit ->

    file_id  = $.trim($('#active_idea_drive_folder_input').val())
    file_url = $.trim($('#active_idea_drive_folder_link_input').val())

    if $('#link_drive_folder').length
      if  file_url == '' &&  file_id == ''
        $('#folder-picker-div').html ''
        success_div = $('<div></div>').addClass('folder-drive-picker-error').hide()
        success_alert_div = $('<div></div>').addClass('folder-drive-picker-form-alert-div alert-danger').attr 'role', 'alert'
        success_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
        success_message_span = $('<span></span>').text('Se ha presentado un error al añadir la carpeta, por favor verifica nuevamente')
        success_div.append success_icon_span
        success_div.append success_message_span
        success_div.append success_alert_div
        $('#folder-picker-div').append success_div
        success_div.show 'slow'
        return false
      else
        $('#folder-picker-div').html ''
        return true
    else
      return true
    return

  # Jesus Muñoz Methods

  $("#ideaCheckbox").bootstrapToggle({
    on: 'Activa',
    off: 'Desactivada'
  });
  $("#checkboxContainer").on "click", ->
    is_active = $("#ideaCheckbox").prop("checked")
    if is_active 
      $("#deactivateIdeaModal").modal('show')
      $("#deactivateIdeaModal").on 'hidden.bs.modal', (e)->
        $("#ideaCheckbox").bootstrapToggle('on')
    else
      $("#activateIdeaModal").modal('show')
      $("#activateIdeaModal").on 'hidden.bs.modal', (e)->
        $("#ideaCheckbox").bootstrapToggle('off')

  # ajax function to call the activities list
  queryActivitiesList = (parameters) ->
    activities_list_request = $.ajax(
      type: 'GET'
      url: '/activities_list'
      data: parameters
      dataType: 'json')
    activities_list_request.done (msg) ->

      checkbox_id = 1
      container = $('#idea_activities_pivot_form')

      # clear container
      container.html ''
      # create a title for this section
      activities_list_title = $('<h4>').text('Selecciona las actividades que deseas incluir')
      activities_list_title.addClass 'checkbox_list_section_title'
      container.append(activities_list_title)

      # insert elements
      $.each msg, (i, item) ->
        div = document.createElement('div')
        div.className = 'row'
        row = container.append(div)
        # generate checkbox
        $('<input />',
          type: 'checkbox'
          class: 'checkbox_activities_pivot_form'
          id: 'cb' + checkbox_id
          value: item.id).appendTo row
        $('<label />',
          'for': 'cb' + checkbox_id
          class: 'checkbox_label_activities_pivot_form'
          text: item.name).appendTo row
        # increment
        checkbox_id = checkbox_id + 1
        return
      return
    activities_list_request.fail (jqXHR, textStatus) ->
      alert 'Request failed: ' + textStatus
      return
    return


  # click the link file
  $('#pivotIdeaModal').on 'show.bs.modal', (e) ->
    $('#idea_pivot_form_errors').html ''
    selected_stage = $('.idea_pivot_select_button').val()
    parameters =
      id: selected_stage
    queryActivitiesList(parameters)

    # when the dropdown changes
    $('.idea_pivot_select_button').change ->
      console.log 'change'
      selected_stage = $(this).val()
      parameters =
        id: selected_stage
      queryActivitiesList(parameters)
      return

    # check how many elements are checked and include them in the form
    checkItems = ->
      elements_checked = []
      $('.checkbox_activities_pivot_form:checked').each ->
        elements_checked.push @value
        return
      elements_checked
    #checkItems function finish

    # before submit form
    $('.idea_pivot_form').submit (e) ->
      checked_items = 0
      self = this
      e.preventDefault()

      # query checked items
      activities_selected = checkItems()
      checked_items  = activities_selected.length

      if checked_items != 0
       #
       $('#idea_pivot_form_errors').html ''
       # insert the selected activities in the field form
       $('#activities_model_ids').val activities_selected
       self.submit()
       # end if

      # here we are going to include the error message when you haven't selected any item
      $('#idea_pivot_form_errors').html ''
      error_div = $('<div></div>').addClass('idea-activities-pivot-error').hide()
      error_alert_div = $('<div></div>').addClass('idea-activities-pivot-error-alert-div alert-danger').attr 'role', 'alert'
      error_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
      error_message_span = $('<span></span>').text('No has seleccionado ninguna actividad')
      error_div.append error_icon_span
      error_div.append error_message_span
      error_div.append error_alert_div
      $('#idea_pivot_form_errors').append error_div
      error_div.show 'slow'

      false
      return
    # close modal
    return


  # when you try to edit the drive folder

  $('#edit-drive-link').click ->
    $('#EditDriveLinkModal').modal 'show'
    return

  # create a callback for this action
  # we need a callback for the picker to get the file link
  pickerEditCallback = (data) ->
    url = ''
    fieldId = ''
    if data.action == google.picker.Action.PICKED
      console.log data
      fileId = data.docs[0].id
      url = data.docs[0].url
      $('#edit_drive_folder_input').val fileId
      $('#edit_drive_folder_link_input').val url

    # notification to my user
    if url != ''  &&  fileId  != ''
      $('#edit-folder-picker-div').html ''
      success_div = $('<div></div>').addClass('folder-drive-picker-success').hide()
      success_alert_div = $('<div></div>').addClass('folder-drive-picker-form-alert-div alert-success').attr 'role', 'alert'
      success_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
      success_message_span = $('<span></span>').text(' Se ha añadido la carpeta exitosamente')
      success_div.append success_icon_span
      success_div.append success_message_span
      success_div.append success_alert_div
      $('#edit-folder-picker-div').append success_div
      success_div.show 'slow'
    else
      $('#edit-folder-picker-div').html ''
      success_div = $('<div></div>').addClass('folder-drive-picker-error').hide()
      success_alert_div = $('<div></div>').addClass('folder-drive-picker-form-alert-div alert-danger').attr 'role', 'alert'
      success_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
      success_message_span = $('<span></span>').text('Se ha presentado un error al añadir la carpeta, por favor verifica nuevamente')
      success_div.append success_icon_span
      success_div.append success_message_span
      success_div.append success_alert_div
      $('#edit-folder-picker-div').append success_div
      success_div.show 'slow'
    return


  # click the link file
  $('#EditDriveLinkModal').on 'show.bs.modal', (e) ->
    $('#edit_link_drive_folder').click ->
      # we are loading the drive api
      onApiIdeaEditLoad()
      return
    return

  # verify if you provide the drive link
  $('#edit_drive_link_form').submit ->

    file_id  = $.trim($('#edit_drive_folder_input').val())
    file_url = $.trim($('#edit_drive_folder_link_input').val())

    if $('#edit_link_drive_folder').length
      if  file_url == '' &&  file_id == ''
        $('#edit-folder-picker-div').html ''
        success_div = $('<div></div>').addClass('folder-drive-picker-error').hide()
        success_alert_div = $('<div></div>').addClass('folder-drive-picker-form-alert-div alert-danger').attr 'role', 'alert'
        success_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
        success_message_span = $('<span></span>').text('Se ha presentado un error al añadir la carpeta, por favor verifica nuevamente')
        success_div.append success_icon_span
        success_div.append success_message_span
        success_div.append success_alert_div
        $('#edit-folder-picker-div').append success_div
        success_div.show 'slow'
        return false
      else
        $('#edit-folder-picker-div').html ''
        return true
    else
      return true
    return

  $('.activate-idea-button').click ->
    confirmation = confirm '¿ Realmente desea activar este modelo de negocio ?'
    if confirmation == true
      form =  $('#activate_startup_idea_form')
      form.submit()
    else
      return false
    return
