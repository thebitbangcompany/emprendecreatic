$(document).ready ->

  $('#achievementIdeaModal').on 'show.bs.modal', (e) ->
    # clear error div
    $('#idea_achievement_form_errors').html ''
    # ajax request error
    $('.idea_achievement_form').on 'ajax:error', (e, xhr, status, error) ->
      errors = xhr.responseJSON.error_message
      errors_div = $('<div></div>').addClass('idea_achievement_errors').hide()
      $.each errors, (i, item) ->
        error_alert_div = $('<div></div>').addClass('ajax-form-alert-div-achievement alert-danger').attr 'role', 'alert'
        error_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
        error_message_span = $('<span></span>').text('Se ha presentado un error: ').append item
        errors_div.append error_icon_span
        errors_div.append error_message_span
        errors_div.append error_alert_div
        return
      $('#idea_achievement_form_errors').append errors_div
      errors_div.show 'slow'
      return
    return