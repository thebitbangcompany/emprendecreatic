$(document).ready ->
  $("#new_image").fileupload
    dataType: "script"
    add: (e, data) ->
      types = /(\.|\/)(jpe?g|png)$/i
      file = data.files[0]
      if types.test(file.type) || types.test(file.name)
        data.context = $(tmpl("template-upload", file))
        $('#imagesUpload').show()
        $('#imagesUpload').html('')
        $('#imagesUpload').append(data.context)
        data.submit()
      else
        alert("#{file.name} no es una imagen en formato jpeg  o  png")
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        if progress == 100
          $('#imagesUpload').hide()
        data.context.find('.bar').css('width', progress + '%')

  $("#addIdeaImage").on "click",(e)->
    e.preventDefault()
    $("#new_image_field").click()

  $("#new_image").hide()