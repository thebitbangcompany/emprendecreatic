$(document).ready ->
  # Entrepreneur Avatar
  $("#entrepreneurAvatarForm").fileupload
    dataType: "script"
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.bar').css('width', progress + '%')

  $("#entrepreneurAvatarlink").on "click",(e)->
    e.preventDefault()
    $("#entrepreneur_avatar").click()

  $("#entrepreneur_avatar").hide()

  #Startup Avatar 
  $("#startupAvatarForm").fileupload
    dataType: "script"
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.bar').css('width', progress + '%')

  $("#startupAvatarlink").on "click",(e)->
    e.preventDefault()
    $("#startup_avatar").click()

  $("#startup_avatar").hide()

  #Idea Avatar
  $("#ideaAvatarForm").fileupload
    dataType: "script"
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.bar').css('width', progress + '%')

  $("#ideaAvatarlink").on "click",(e)->
    e.preventDefault()
    $("#idea_avatar").click()

  $("#idea_avatar").hide()

  #Advisor Avatar

  $("#advisorAvatarForm").fileupload
    dataType: "script"
    progress: (e, data) ->
      if data.context
        progress = parseInt(data.loaded / data.total * 100, 10)
        data.context.find('.bar').css('width', progress + '%')

  $("#advisorAvatarlink").on "click",(e)->
    e.preventDefault()
    $("#advisor_avatar").click()

  $("#advisor_avatar").hide()

  # add: (e, data) ->
  #   types = /(\.|\/)(gif|jpe?g|png)$/i
  #   file = data.files[0]
  #   if types.test(file.type) || types.test(file.name)
  #     data.context = $(tmpl("template-upload", file))
  #     $('#new_painting').append(data.context)
  #     data.submit()
  #   else
  #     alert("#{file.name} is not a gif, jpeg, or png image file")
  # progress: (e, data) ->
  #   if data.context
  #     progress = parseInt(data.loaded / data.total * 100, 10)
  #     data.context.find('.bar').css('width', progress + '%')