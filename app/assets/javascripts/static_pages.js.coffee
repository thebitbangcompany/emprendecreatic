# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  $('#layerslider').layerSlider
    pauseOnHover: true
    autoPlayVideos: false
    skinsPath: '/skins/'
    responsive: false
    responsiveUnder: 1280
    layersContainer: 1280
    skin: 'borderlessdark3d'
    hoverPrevNext: true
    slideDelay: 5000

  $('.jquery-alert').delay(2000).fadeOut(1000)
  $(document).on 'click','.jquery-alert', ->
    $('.jquery-alert').fadeOut(1000)

  $(document).ajaxStart ->
    $("#loading-mailbox-ajax").show()
  $(document).ajaxStop ->
    $("#loading-mailbox-ajax").hide()
