# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  # hide elements
  # when the document is ready we should hide the element
  # $('#entrepreneur_deliverable_form_div_new').hide()
  # $('#entrepreneur_deliverable_form_div_edit').hide()
  # $('#artifact_model_file').empty()
  # $('#artifact_model_file').hide()
  #this method allows advisor to change the status of activities
  $(".radio_activity_status").on 'change', ->
    cnfrm = confirm('¿ Realmente desea cambiar el estado de la actividad ?')
    form =  $(this).closest 'form'
    if cnfrm != true
      form.resetForm()
    else
      form.submit()
    return



  #this method provides the ability to disable the submit button when the activity has been started
  activity_status = $('#entrepreneur_activate_activity').val()
  switch activity_status
    when 'start'
      $('#entrepreneur_activate_activity').removeClass().addClass 'btn btn-warning'
      $('#entrepreneur_activate_activity').prop 'disabled', true
    when 'finish'
      $('#entrepreneur_activate_activity').removeClass().addClass 'btn btn-danger'
      $('#entrepreneur_activate_activity').prop 'disabled', true

  #this method allows entrepreneur to get the files associated to an artifact
  QueryArtifactModel = (parameters) ->
    artifact_model_request = $.ajax(
      type: 'GET'
      url: '/entrepreneur_get_artifact_model/'
      data: parameters
      dataType: 'html')
    artifact_model_request.done (msg) ->
      #clear the div
      $('.artifact_model_file').empty()
      #read the response content
      artifact_model = $.parseJSON(""+msg+"")
      # create a link  to save the information.
      artifact_link = location.protocol+'//'+location.host+artifact_model['support']['url']
      # create the elements to show the information about the artifact
      artifact_link_element = $('<a></a>',
        id: 'artifact_model_file_link'
        target: '_blank'
        class: 'row'
        name: 'artifact_file_link'
        href: artifact_link
        text: 'Descargar archivo adjunto')
      artifact_description_element = $('<p></p>',
        id: 'artifact_model_description'
        class: 'row'
        text: artifact_model['description']
      )
      artifact_name_element = $('<h3></h3>',
        id: 'artifact_model_name'
        class: 'row'
        text: artifact_model['name']
      )
      # put the content
      $('.artifact_model_file').append(artifact_name_element)
      $('.artifact_model_file').append(artifact_description_element)
      $('.artifact_model_file').append(artifact_link_element)
      # show the div
      $('.artifact_model_file').show()
      return
    artifact_model_request.fail (jqXHR, textStatus) ->
      $('.artifact_model_file').empty()
      alert 'Request failed'
      return
    return


  #generate artifacts
  #TODO
  #$('.artifact_model_select_button_entrepreneur').click ->
  $(document).on 'click', '.artifact_model_select_button_entrepreneur', ->
    artifact_model_id = $(this).val()
    parameters =
      id: artifact_model_id
    QueryArtifactModel parameters
    return

   # create a custom query to display the artifact model when the document is ready
  $('#artifact_modal').on 'show.bs.modal', (e) ->
    artifact_preselected = $("#idea_artifact_artifact_model_id option:selected").val()
    if artifact_preselected != ''
      parameters =
        id: artifact_preselected
      QueryArtifactModel parameters
    return

  # shows artifact model description on a modal
  $(document).on 'click', '.artifact-description-link', ()->
    $("#artifactDescription").modal('show')
    title = $(this).data('title')
    description = $(this).data('description')
    $("#artifactDescriptionTitle").html(title)
    $("#artifactDescriptionBody").html(description)

  $('#backtoIdea').hide()

$(window).scroll ->
  if ($(this).scrollTop()>60)
    $('#backtoIdea').fadeIn();
  else
    $('#backtoIdea').fadeOut();
    
  
