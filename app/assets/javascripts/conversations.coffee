# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $(document).on 'show.bs.modal', '#messageForm', (event) ->
    button = $(event.relatedTarget)
    # Button that triggered the modal
    type = button.data('type')
    email = button.data('email')
    name = button.data('name')
    # Extract info from data-* attributes
    # If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    # Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    modal = $(this)
    modal.find('.modal-title').text 'Nuevo mensaje para: ' + name
    modal.find("#conversation_subject").val ""
    modal.find("#conversation_body").val ""
    modal.find("#conversation_type").val type
    modal.find("#conversation_email").val email
    return
  $(document).on "submit", "#conversationForm", (event)->
    $("#messageForm .message-body").html('Enviando mensaje...')
  $(document).on "click", ".delete-slection",(event)->
    event.preventDefault()
    searchIDs = $("#mailbox input:checkbox:checked").map(->
      return $(this).closest(".list-group-item").data("id")
    ).get(); 
    $("#trashing-elements").show()
    if searchIDs.length == 0
      alert "Debe seleccionar como minimo una conversación"
      return
    else
      $.ajax({
        type: "POST",
        url: "/send_trash",
        data: { ids: searchIDs },
        success:(data) ->
          location.reload();
          return false
        error:(data) ->
          alert "Hubo un error"
          return false
      })
  $(document).on "click", ".untrash-slection",(event)->
    event.preventDefault()
    searchIDs = $("#trash input:checkbox:checked").map(->
      return $(this).closest(".list-group-item").data("id")
    ).get(); 
    $("#trashing-elements").show()
    if searchIDs.length == 0
      alert "Debe seleccionar como minimo una conversación"
      return
    else
      $.ajax({
        type: "POST",
        url: "/untrash",
        data: { ids: searchIDs },
        success:(data) ->
          location.reload();
          return false
        error:(data) ->
          alert "Se ha presentado un error"
          return false
      })
  $(document).on "click", ".refresh-button", ->
    location.reload();
  $("#all-conversations-checkbox").change ->
    if $(this).is(":checked")
      $(".conversations-list ").find(':checkbox').prop('checked', true);
    else
      $(".conversations-list ").find(':checkbox').prop('checked', false);

  currentPage = 1
  $('#conversationPaginate').on "click" ,(event)->
    event.preventDefault()
    currentPage++
    $.ajax
      type: "GET"
      dataType: "script"
      url: window.location.pathname
      data: {page: currentPage}
      success: (data)->
        return false
      error: (data)->
        return false
  $('.message-inbox-icon').on "click" ,(event)->
    event.preventDefault()
    $(".message-inbox-icon").dropdown()
