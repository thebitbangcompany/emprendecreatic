# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $("[data-toggle=tooltip]").tooltip();
  #$("[data-toggle=popover]").popover();
  Accordion = (el, multiple) ->
    @el = el or {}
    @multiple = multiple or false
    # Variables privadas
    links = @el.find('.link')
    # Evento
    links.on 'click', {
      el: @el
      multiple: @multiple
    }, @dropdown
    return

  Accordion::dropdown = (e) ->
    $el = e.data.el
    $this = $(this)
    $next = $this.next()
    $next.slideToggle()
    $this.parent().toggleClass 'open'
    if !e.data.multiple
      $el.find('.submenu').not($next).slideUp().parent().removeClass 'open'
    return

  accordion = new Accordion($('#accordion'), false)

  # search entrepreneurs index
  $('#entrepreneur_search').on 'input', ->
    # # do something
    # setTimeout (->
    #   $('#search_entrepreneurs_form').submit()
    #   return
    # ), 2000
    # #
    # return
  return
  
