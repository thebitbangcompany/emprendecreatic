# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->

  $.datepicker.setDefaults
    dateFormat: 'yy-mm-dd'
  
  # tag field
  $("#commentTags").tagit()

  # load tags
  # previousTags = $('#comment_tag_list_field').val().split(' ')
  # console.log previousTags
  # reload tag list
  # previousTags.forEach (value) ->
    # $("#commentTags").tagit("createTag", value)
    # return

  # magane tags activities
  # when the tag is created copy rewrite the tags
  $('#commentTags').tagit afterTagAdded: (event, ui) ->
    # do something special
    currentTags = $('#commentTags').tagit('assignedTags')
    $('#comment_tag_list_field').val(currentTags)
    console.log ui.tag
    return

  # then the tag is deleted rewrite the tags
  $('#commentTags').tagit afterTagRemoved: (event, ui) ->
    # do something special
    currentTags = $('#commentTags').tagit('assignedTags')
    $('#comment_tag_list_field').val(currentTags)
    return


  return