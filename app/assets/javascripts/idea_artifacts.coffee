# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->

  # load the folder id of this idea
  # receive the folder id
  parent_folder = $('.drive-folder-information-link').data('folder-id')


  # load Drive API credentials
  # we must migrate these credentials to other account.
  # project this is a browser key
  developerKey = 'AIzaSyDDXu0AlhyvV90ctK0n2zJ-TjBAk_tR2p8'
  # The Client ID obtained from the Google Developers Console. Replace with your own Client ID.
  clientId = '236642561331-mhrkpikiqeoircd0kv7f1nacb2helj1h.apps.googleusercontent.com'
  # Replace with your own App ID. (Its the first number in your Client ID)
  appId = '236642561331'
  # Scope to use to access user's photos.
  scope = [ 'https://www.googleapis.com/auth/drive' ]

  # is the picker loaded
  pickerApiLoaded = false
  # toker after the user accepts connect our application with google drive
  oauthToken = undefined
  # Use the API Loader script to load google.picker and gapi.auth.

  onApiLoad = ->
    gapi.load 'auth', 'callback': onAuthApiLoad
    gapi.load 'picker', 'callback': onPickerApiLoad
    return

  onAuthApiLoad = ->
    window.gapi.auth.authorize {
      'client_id': clientId
      'scope': scope
      'immediate': false
    }, handleAuthResult
    return

  onPickerApiLoad = ->
    pickerApiLoaded = true
    createPicker()
    return

  handleAuthResult = (authResult) ->
    if authResult and !authResult.error
      oauthToken = authResult.access_token
      createPicker()
    return

  # Create and render a Picker
  createPicker = ->
    if pickerApiLoaded and oauthToken

      if parent_folder != null && parent_folder != ''
        uploadView = (new (google.picker.DocsUploadView)).setIncludeFolders(true)
        myFolderView = (new (google.picker.DocsView)).setIncludeFolders(true).setParent(parent_folder)

      else
        uploadView = (new (google.picker.DocsUploadView)).setIncludeFolders(true)
        myFolderView = (new (google.picker.DocsView)).setIncludeFolders(true)
                                                     .setSelectFolderEnabled(true)


      picker = (new (google.picker.PickerBuilder)).addView(myFolderView)
                                                  .addView(google.picker.ViewId.DOCUMENTS)
                                                  .addView(google.picker.ViewId.PRESENTATIONS)
                                                  .addView(google.picker.ViewId.SPREADSHEETS)
                                                  .addView(google.picker.ViewId.FOLDERS)
                                                  .addView(google.picker.ViewId.PDFS)
                                                  .addView(uploadView)
                                                  .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
                                                  .setOAuthToken(oauthToken).setDeveloperKey(developerKey)
                                                  .setCallback(pickerCallback)
                                                  .build()
      picker.setVisible true
    return

  # we need a callback for the picker to get the file link
  pickerCallback = (data) ->
    url = 'nothing'
    if data.action == google.picker.Action.PICKED
      console.log data
      fileId = data.docs[0].id
      url = data.docs[0].url
      $('#url_drive_file_field').val url
      console.log data
      # notification to my user
      if url != null && url != ''
        $('#artifact-drive-picker-div').html ''
        success_div = $('<div></div>').addClass('drive-picker-success').hide()
        success_alert_div = $('<div></div>').addClass('drive-picker-form-alert-div alert-success').attr 'role', 'alert'
        success_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
        success_message_span = $('<span></span>').text('Archivo añadido exitosamente')
        success_div.append success_icon_span
        success_div.append success_message_span
        success_div.append success_alert_div
        $('#artifact-drive-picker-div').append success_div
        success_div.show 'slow'
    return


  # this method allow advisors to change an artifact status
  $(document).on 'change', '.radio_artifact_status', ->
    # $(".radio_artifact_status").on 'change', ->
    console.log 'bug'
    form =  $(this).closest 'form'
    form.submit()
    return

  $('#artifact_modal').on 'show.bs.modal', (e) ->

    # tag field
    $("#artifactTags").tagit()

    # load tags
    previousTags = $('#artifact_tag_list_field').val().split(' ')
    # reload tag list
    previousTags.forEach (value) ->
      $("#artifactTags").tagit("createTag", value)
      return
    # magane tags activities
    # when the tag is created copy rewrite the tags
    $('#artifactTags').tagit afterTagAdded: (event, ui) ->
      # do something special
      currentTags = $('#artifactTags').tagit('assignedTags')
      $('#artifact_tag_list_field').val(currentTags)
      console.log ui.tag
      return
    # then the tag is deleted rewrite the tags
    $('#artifactTags').tagit afterTagRemoved: (event, ui) ->
      # do something special
      currentTags = $('#artifactTags').tagit('assignedTags')
      $('#artifact_tag_list_field').val(currentTags)
      return

    # ajax request error
    $('.entrepreneur_deliverable_form').on 'ajax:error', (e, xhr, status, error) ->
      errors = xhr.responseJSON.error_message
      errors_div = $('<div></div>').addClass('artifacts_errors').hide()
      $.each errors, (i, item) ->
        error_alert_div = $('<div></div>').addClass('ajax-form-alert-div-artifact alert-danger').attr 'role', 'alert'
        error_icon_span = $('<span></span>').addClass('glyphicon glyphicon-exclamation-sign').attr 'aria-hidden', true
        error_message_span = $('<span></span>').text(' Error: ').append item
        errors_div.append error_icon_span
        errors_div.append error_message_span
        errors_div.append error_alert_div
        return
      $('#artifact_form_errors_div').append errors_div
      errors_div.show 'slow'
      return

    $('#entrepreneur_artifact_activity_button').click ->
      currentTags = $('#artifactTags').tagit('assignedTags')
      $('#artifact_tag_list_field').val(currentTags)
      $('#artifacts_errors').hide()
      $('#artifact_form_errors_div').html ''
      return

    $('#link_drive_file').click ->
      # we are loading the drive api
      onApiLoad()
      return
    return
  return


