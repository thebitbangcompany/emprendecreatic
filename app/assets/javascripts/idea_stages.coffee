# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $("#btnSaveIdea").on "click", (e)->
    e.preventDefault()
    $(".simple_form").submit()
  $(".radio_stage_status").on 'change', (evt) ->

   radioClass = $(this).attr('class').split(' ')[1]
   previousRadio = $("input:radio."+radioClass+":unchecked").attr('id')
   console.log previousRadio
   confirmation = confirm '¿Realmente desea cambiar el estado de esta etapa?'
   if confirmation == true
    form =  $(this).closest 'form'
    form.submit()
   else
    $('#'+previousRadio).prop 'checked', true
   return
  return
