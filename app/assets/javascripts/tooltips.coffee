$(document).ready ->
  complete = ""
  status = ""
  name = ""
  status_id = ""
  start_date = ""
  due_date = ""
  $('.idea-stage').mouseover ->
    complete = $(this).data('complete')
    name = $(this).data('title')
    status_id = $(this).data('status-id')
    status = $(this).data('status')
    start_date = $(this).data('start-date')
    due_date = $(this).data('due-date')
  $('.idea-stage').tooltipster
    content: ->
      return '<div class="idea-tootip"><h3 class="tooltipster-title">Etapa: '+name+'</h3><div class="tooltip-content"><p class="state-text-'+status_id+'">Estado: '+status+'</p><p>'+complete+' actividades completadas</p><p><strong>Fecha Inicio:</strong> '+start_date+'</p><p><strong>Fecha de Finalización:</strong> '+due_date+'</p></div>'
    position: 'top'
    multiple: true
    contentAsHTML: true
    maxWidth: 500
    interactive: true
    theme: 'tooltipster-light'
  return

  
$(window).scroll ->
  if ($(this).scrollTop()>60)
    $('#backtoIdea').fadeIn();
  else
    $('#backtoIdea').fadeOut();
    
  