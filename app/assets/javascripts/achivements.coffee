$(document).ready ->
  name = ""
  description = ""
  $('.idea_achievement').mouseover ->
    name = $(this).data('name')
    description = $(this).data('description')
  $('.idea_achievement').tooltipster
    content: ->
      return '<div class="idea-tootip"><h3 class="tooltipster-title">'+name+'</h3><p>'+description+'</p></div>'
    position: 'top'
    multiple: true
    contentAsHTML: true
    maxWidth: 500
    interactive: true
    theme: 'tooltipster-light'
  return