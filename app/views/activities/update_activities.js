$("#notificationsContainer").html("<%=j render(partial: '/layouts/navbar/notification_alerts', locals: {activities: @activities})%>");
$('#my-notifications').scroll(function() {
  var $this, height, isScrolledToEnd, scroll;
  $this = $(this);
  height = this.scrollHeight - $this.height();
  scroll = $this.scrollTop();
  isScrolledToEnd = scroll === height;
  $('.scroll-pos').text(scroll);
  $('.scroll-height').text(height);
  var page = parseInt($("#currentActivityPage").data('page'));
  if (isScrolledToEnd) {
    page++;
    $("#currentActivityPage").data('page',page);
    $('#notificationsLoader').html("<div class='text-center'><img src=<%=asset_path('loading-small.gif')%> alt='Actualizando anteriores...' title='Actualizando...' /></div>");
    $.ajax({
        type: 'GET',
        url: '/append_activities',
        data: {page: page},
        success: function(data){
           
        },
        dataType: 'script'
    });
    $('body').removeClass('loading');
  }
});

$('#my-notifications').on('mouseover', function(e) {
  document.body.style.overflow='hidden'
});
$('#my-notifications').on('mouseout', function(e) {
  document.body.style.overflow='auto'
});