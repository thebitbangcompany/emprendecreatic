// var io = require('socket.io').listen(5001),
//     redis = require('redis').createClient();

// console.log("Listening on 5001... ");

// redis.subscribe('rt-change');

// var ad_notif = io.of('/notifications');
// //var en_notif = io.of('/entrepreneur_notifications');

// ad_notif.on('connection', function(socket){
//     console.log('an user connected');
//     socket.join('notifications_room');
//     redis.on('message', function(channel, message){
//         console.log('notification sended');
//         socket.to('notifications_room').emit('rt-change', JSON.parse(message));
//     });

//     socket.on('disconnect', function() {
//         console.log("desconectado");
//       socket.leave('notifications_room');
//    });
// });

// en_notif.on('connection', function(socket){
//     console.log('an entrepreneur connected');
//     socket.join('entrepreneur_room');
//     redis.on('message', function(channel, message){
//         socket.to('entrepreneur_room').emit('rt-change', JSON.parse(message));
//     });
// });


//Requires and main server objects
var redis = require('redis');
var socketio = require('socket.io');
var io = socketio.listen(5001);


//This object will contain all the channels being listened to.
var global_channels = {};
 

//Server Logic goes here
io.on('connection', function(socketconnection){
    console.log("User connected");
    //All the channels this connection subscribes to
    socketconnection.connected_channels = {}
     
    //Subscribe request from client
    socketconnection.on('subscribe', function(channel_name){
        console.log("Subs");
        //Set up Redis Channel
        if (global_channels.hasOwnProperty(channel_name)){
            //If channel is already present, make this socket connection one of its listeners
            global_channels[channel_name].listeners[socketconnection.id] = socketconnection;
        }
        else{
            //Else, initialize new Redis Client as a channel and make it subscribe to channel_name
            global_channels[channel_name] = redis.createClient();
            global_channels[channel_name].subscribe(channel_name);
            global_channels[channel_name].listeners = {};
            //Add this connection to the listeners
            global_channels[channel_name].listeners[socketconnection.id] = socketconnection;
            //Tell this new Redis client to send published messages to all of its listeners
            global_channels[channel_name].on('message', function(channel, message){
                Object.keys(global_channels[channel_name].listeners).forEach(function(key){
                    global_channels[channel_name].listeners[key].send(message);
                });
            });
        }
         
        socketconnection.connected_channels[channel_name] = global_channels[channel_name];
    });
    //Unsubscribe request from client
    socketconnection.on('unsubscribe', function(channel_name){
        console.log("User unsubscribed");
        if (socketconnection.connected_channels.hasOwnProperty(channel_name)){
        //If this connection is indeed subscribing to channel_name
        //Delete this connection from the Redis Channel's listeners
        delete global_channels[channel_name].listeners[socketconnection.id];
        //Delete channel from this connection's connected_channels
        delete socketconnection.connected_channels[channel_name];
        }
    });
    //Disconnect request from client
    socketconnection.on('disconnect', function(){
        console.log("User disconnected");
        //Remove this connection from listeners' lists of all channels it subscribes to
        Object.keys(socketconnection.connected_channels).forEach(function(channel_name){
        delete global_channels[channel_name].listeners[socketconnection.id];
        });
    });
});