class AddImageToStageModels < ActiveRecord::Migration
  def change
    add_column :stage_models, :image, :string
  end
end
