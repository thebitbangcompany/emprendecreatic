class CreateIdeaActivity < ActiveRecord::Migration
  def change
    create_table :idea_activities do |t|
      t.integer :activity_model_id
      t.integer :idea_stage_id
      t.integer :status,default: 0
      t.datetime :start_date
      t.datetime :due_date
      t.timestamps null: false
    end
  end
end
