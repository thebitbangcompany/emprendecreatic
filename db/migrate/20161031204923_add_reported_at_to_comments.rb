class AddReportedAtToComments < ActiveRecord::Migration
  def change
    add_column :comments, :reported_at, :datetime
  end
end
