class CreateStartups < ActiveRecord::Migration
  def change
    create_table :startups do |t|
      t.string :name , null: false , default: ""
      t.string :vision
      t.string :mission
      t.string :email
      t.string :phone
      t.string :website
      t.string :industry
      t.boolean :is_active, null: false  , default: true
      t.timestamps null: false
    end
  end
end
