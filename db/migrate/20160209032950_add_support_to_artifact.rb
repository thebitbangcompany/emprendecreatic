class AddSupportToArtifact < ActiveRecord::Migration
  def change
    add_column :artifact_models, :support, :string
  end
end
