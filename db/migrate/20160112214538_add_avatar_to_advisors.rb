class AddAvatarToAdvisors < ActiveRecord::Migration
  def change
    add_column :advisors, :avatar, :string
  end
end
