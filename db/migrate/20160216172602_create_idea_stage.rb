class CreateIdeaStage < ActiveRecord::Migration
  def change
    create_table :idea_stages do |t|
      t.integer :stage_model_id
      t.integer :idea_process_id
      t.integer :status ,default: 0
      t.datetime :start_date
      t.datetime :due_date
      t.timestamps null: false
    end
  end
end
