class CreateIdeaProcess < ActiveRecord::Migration
  def change
    create_table :idea_processes do |t|
      t.integer :process_model_id
      t.datetime :start_date
      t.datetime :due_date
      t.integer :idea_id
      t.timestamps null: false
    end
  end
end
