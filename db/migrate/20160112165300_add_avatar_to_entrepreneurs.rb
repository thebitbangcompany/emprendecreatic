class AddAvatarToEntrepreneurs < ActiveRecord::Migration
  def change
    add_column :entrepreneurs, :avatar, :string
  end
end
