class AddAvatarToStartups < ActiveRecord::Migration
  def change
    add_column :startups, :avatar, :string
  end
end
