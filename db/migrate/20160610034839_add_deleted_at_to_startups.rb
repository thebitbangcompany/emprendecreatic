class AddDeletedAtToStartups < ActiveRecord::Migration
  def change
    add_column :startups, :deleted_at, :datetime
    add_index :startups, :deleted_at
  end
end
