class AddTypeToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :idea_type, :string
  end
end
