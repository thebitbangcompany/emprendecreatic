class AddAdvisorIdToIdea < ActiveRecord::Migration
  def change
    add_column :ideas, :advisor_id, :integer
  end
end
