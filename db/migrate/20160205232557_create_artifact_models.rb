class CreateArtifactModels < ActiveRecord::Migration
  def change
    create_table :artifact_models do |t|
      t.string :name , null: false , default: ""
      t.string :description , null: false , default: ""
      t.integer :activity_model_id, index: true
      t.string :artifact_type , default: ""

      t.timestamps null: false
    end
  end
end
