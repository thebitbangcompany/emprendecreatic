class AddDeletedAtColumnToAdvisors < ActiveRecord::Migration
  def change
    add_column :advisors, :deleted_at, :datetime
  end
end
