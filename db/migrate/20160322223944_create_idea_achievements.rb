class CreateIdeaAchievements < ActiveRecord::Migration
  def change
    create_table :idea_achievements do |t|
      t.integer :idea_id
      t.integer :achievement_model_id

      t.timestamps null: false
    end
  end
end
