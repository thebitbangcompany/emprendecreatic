class AddIsPivotToIdeaStages < ActiveRecord::Migration
  def change
    add_column :idea_stages, :is_pivot, :boolean, default: false
  end
end
