class CreateProcessModelSteps < ActiveRecord::Migration
  def change
    create_table :process_model_steps do |t|
      t.integer :next_step_id , index: true , null: false
      t.integer :previous_step_id , index: true , null: false
      t.integer :process_model_id , index: true , null: false
      t.timestamps null: false
    end
  end
end
