class AddPositionToStartupEntrepreneurs < ActiveRecord::Migration
  def change
    add_column :startup_entrepreneurs, :position, :string
  end
end
