class AddDriveFolderToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :drive_folder, :string
    add_column :ideas, :drive_folder_link, :string
  end
end
