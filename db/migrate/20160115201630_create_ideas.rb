class CreateIdeas < ActiveRecord::Migration
  def change
    create_table :ideas do |t|
      t.string :name , null: false , default:''
      t.text :description , null: false , default: ''
      t.text :problem_definition , null: false , default: ''
      t.integer :startup_id
      t.boolean :is_private , null: false , default: false
      t.timestamps null: false
    end
  end
end
