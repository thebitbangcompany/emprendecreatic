class RemoveSkillsFromEntrepreneurs < ActiveRecord::Migration
  def change
    remove_column :entrepreneurs, :skills, :string
  end
end
