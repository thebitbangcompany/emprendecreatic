class AddIsExpertToAdvisors < ActiveRecord::Migration
  def change
    add_column :advisors, :is_expert, :boolean, :null => false, default: false
  end
end
