class CreateIdeaArtifact < ActiveRecord::Migration
  def change
    create_table :idea_artifacts do |t|
      t.integer :artifact_model_id
      t.integer :idea_activity_id
      t.string :url
      t.integer :status,default: 0
      t.timestamps null: false
    end
  end
end
