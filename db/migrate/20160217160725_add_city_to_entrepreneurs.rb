class AddCityToEntrepreneurs < ActiveRecord::Migration
  def change
    add_column :entrepreneurs, :city, :string
  end
end
