class AddAuthTokenToAdvisors < ActiveRecord::Migration
  def self.up
    change_table :advisors do |t|
      t.string :authentication_token
    end

    add_index  :advisors, :authentication_token, :unique => true
  end

  def self.down
    remove_column :advisors, :authentication_token
  end
end
