class AddColorToStageModel < ActiveRecord::Migration
  def change
    add_column :stage_models, :color, :string
  end
end
