class CreateStageModels < ActiveRecord::Migration
  def change
    create_table :stage_models do |t|
      t.string :name, null: false , default: ""
      t.string :description , null: false , default: ""
      t.integer :process_model_id , index: true
      t.timestamps null: false
    end
  end
end
