class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :image
      t.string :description
      t.integer :idea_id
      t.timestamps null: false
      t.index :idea_id
    end
  end
end
