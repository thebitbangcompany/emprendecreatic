class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
    	t.belongs_to :question, index: true
    	t.belongs_to :startup, index: true
    	t.belongs_to :advisor, index: true
    	t.float :value
    	t.boolean :answered
      t.timestamps null: false
    end
  end
end
