class AddSocialMediaToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :facebook, :string
    add_column :ideas, :twitter, :string
  end
end
