class CreatePortalQuestions < ActiveRecord::Migration
  def change
    create_table :portal_questions do |t|
      t.string :issue , null: false , default: ""
      t.string :content , null: false , default: ""
      t.string :email , null: false , default: ""
      t.string :author , null: false , default: ""
      t.integer :status , null: false , default: 0
      t.timestamps null: false
    end
  end
end
