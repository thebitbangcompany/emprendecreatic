class CreateActivityModels < ActiveRecord::Migration
  def change
    create_table :activity_models do |t|
      t.string :name , null: false , default: ""
      t.string :description , null: false , default: ""
      t.integer :stage_model_id , index: true
      t.timestamps null: false
    end
  end
end
