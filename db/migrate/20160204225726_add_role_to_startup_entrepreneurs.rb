class AddRoleToStartupEntrepreneurs < ActiveRecord::Migration
  def change
    add_column :startup_entrepreneurs, :role, :string, :default => 'member'
  end
end
