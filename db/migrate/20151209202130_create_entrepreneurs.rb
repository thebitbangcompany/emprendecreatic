class CreateEntrepreneurs < ActiveRecord::Migration
  def change
    create_table :entrepreneurs do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :linkedin
      t.string :twitter
      t.string :facebook
      t.string :summary
      t.string :profession
      t.string :skills

      t.timestamps null: false
    end
  end
end
