class AddLocationToStartups < ActiveRecord::Migration
  def change
    add_column :startups, :city, :string
    add_column :startups, :country, :string
  end
end
