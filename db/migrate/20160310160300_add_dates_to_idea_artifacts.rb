class AddDatesToIdeaArtifacts < ActiveRecord::Migration
  def change
    add_column :idea_artifacts, :start_date, :datetime
    add_column :idea_artifacts, :due_date, :datetime
  end
end
