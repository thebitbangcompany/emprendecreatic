class AddSocialMediaToStartups < ActiveRecord::Migration
  def change
    add_column :startups, :facebook, :string
    add_column :startups, :linkedin, :string
    add_column :startups, :twitter, :string
  end
end
