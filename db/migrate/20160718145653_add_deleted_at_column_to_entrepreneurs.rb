class AddDeletedAtColumnToEntrepreneurs < ActiveRecord::Migration
  def change
    add_column :entrepreneurs, :deleted_at, :datetime
  end
end
