class CreateStartupEntrepreneurs < ActiveRecord::Migration
  def change
    create_table :startup_entrepreneurs do |t|
      t.integer :startup_id
      t.integer :entrepreneur_id
      t.timestamps null: false
    end
  end
end
