class AddCountryToEntrepreneurs < ActiveRecord::Migration
  def change
    add_column :entrepreneurs, :country, :string
  end
end
