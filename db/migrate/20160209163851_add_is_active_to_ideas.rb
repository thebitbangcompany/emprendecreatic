class AddIsActiveToIdeas < ActiveRecord::Migration
  def change
    add_column :ideas, :is_active, :boolean, default: false
  end
end
