class RemoveSkillsFromAdvisors < ActiveRecord::Migration
  def change
    remove_column :advisors, :skills, :string
  end
end
