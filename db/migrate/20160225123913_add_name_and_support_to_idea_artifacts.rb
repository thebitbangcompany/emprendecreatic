class AddNameAndSupportToIdeaArtifacts < ActiveRecord::Migration
  def change
    add_column :idea_artifacts, :name, :string
    add_column :idea_artifacts, :support, :string
  end
end
