# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
# cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
# Mayor.create(name: 'Emanuel', city: cities.first)
# required
# require 'as-duration'
require 'chronic'

# clear seed
# AdminUser.destroy_all
# PortalQuestion.destroy_all
# Entrepreneur.destroy_all
# Advisor.destroy_all
# AchievementModel.destroy_all

# Create an administrator user
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

# Create four entrepreneurs
# first entrepreneur
entrepreneur_a =  Entrepreneur.create!(first_name: 'Sami', last_name: 'Enriquez',
                                       phone: '3016624148', email: 'fsami.c915@gmail.com',
                                       password: '12345678', password_confirmation: '12345678', confirmed_at: DateTime.now.in_time_zone)
# second entrepreneur
entrepreneur_b =  Entrepreneur.create!(first_name: 'Javier', last_name: 'Suarez',
                                       phone: '3928282928', email: 'fluisjavier.suarezmeza@gmail.com',
                                       password: '12345678', password_confirmation: '12345678', confirmed_at: DateTime.now.in_time_zone)

# third entrepreneur
entrepreneur_c =  Entrepreneur.create!(first_name: 'Jesus', last_name: 'Muñoz',
                                       phone: '3016624147', email: 'fjesusduardo2028@gmail.com',
                                       password: '12345678', password_confirmation: '12345678', confirmed_at: DateTime.now.in_time_zone)
# fourth entrepreneur
entrepreneur_d =  Entrepreneur.create!(first_name: 'Esteban', last_name: 'Ceron',
                                       phone: '3928282927', email: 'frestebance@gmail.com',
                                       password: '12345678', password_confirmation: '12345678', confirmed_at: DateTime.now.in_time_zone)

# Create two advisors
# first advisor
advisor_a = Advisor.create!(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name,
                            phone: '3016624141', email: 'advisor@gmail.com',
                            password: '12345678', password_confirmation: '12345678', is_expert: true)
# second advisor
advisor_b = Advisor.create!(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name,
                            phone: '3016624146',
                            email: 'advisor_b@gmail.com', password: '12345678', password_confirmation: '12345678', is_expert: true )

# Create some page questions
5.times do
  PortalQuestion.create!(issue: Faker::Lorem.sentence, content: Faker::Lorem.sentence,
                         email: Faker::Internet.email, status: 0, author: Faker::Name.name)
  PortalQuestion.create!(issue: Faker::Lorem.sentence, content: Faker::Lorem.sentence,
                         email: Faker::Internet.email, status: 1, author: Faker::Name.name)
end

# generate achievements
2.times do
  AchievementModel.create!(name: Faker::University.name, description: Faker::Lorem.sentence(1), image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'app', 'assets', 'images', 'achievement_trophy.png')))
end



#
# Generate Fake Process A
#
# ProcessModel.destroy_all
# Create the process for the StartUp Creation Model (SCM)
process = ProcessModel.create!(name: 'Fake StartUp Creation Model', description: 'Aplicación Lean para CreaTIC Corporación')
#
# Create Stage of the SCM: Customer Discovery
#
customer_discovery_stage = StageModel.create!(name: 'Customer Discovery Fake',
                                              description: 'Proceso de validación donde el emprendedor debe constatar que existe un grupo de clientes para quienes vale la pena resolver un problema.',
                                              image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'descubrimiento-de-clientes.png')),
                                              process_model_id: process.id,
                                              color: "#66B645")
#
#

# Create the first Activity for 'Customer Discovery'
inscription_activity = ActivityModel.create!(name: 'Paquete de inscripción Fake', description: 'Inicia y has parte del Cluster CreaTIC, para esto es necesario que firmes unos compromisos y formalices tu vinculacion al proceso de creacion de StartUp. Solicita los documentos a tu Asesor.', stage_model_id: customer_discovery_stage.id)
ArtifactModel.create!(name: 'Pagaré Fake', description: 'Documento que debes firmar junto con tu Asesor', artifact_type: 'url',   activity_model_id: inscription_activity.id)

#
# Create the Stage of the SCM: Customer Validation
#
customer_validation_stage = StageModel.create!(name: 'Customer Validation Fake',
                                               description: 'Proceso de validacion de una solución desarrollada con early adopters',
                                               image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'validacion-de-clientes.png')),
                                               process_model_id: process.id,
                                               color: "#E41F26")
# Create the first Activity for 'Customer Validation'
first_prototype_activity = ActivityModel.create!(name: 'Prototipo V1.0 Fake', description: 'Es hora de desarrollar tu primer prototipo, para ello debes utlizar practicas agiles de desarrollo. El CDT esta dispuesto a apoyarte con esta actividad, solicita la informacion con tu Asesor', stage_model_id: customer_validation_stage.id)
ArtifactModel.create!(name: 'Desarrollo de Prototipo Fake', description: 'A partir del producto que desarrollaste, indícanos de que se trata y enséñanos como luce.', artifact_type: 'url',  activity_model_id: first_prototype_activity.id)


#
# Create the Stage of the SCM: Customer Creation
#
customer_creation_stage = StageModel.create!(name: 'Customer Creation  Fake',
                                             description: 'Definicion de un road map de ventas escalable y repetibles. Una vez validada la solución a nivel de prototipo y métricas con early adopters debe procederse a la construcción de un producto dirigido a clientes masivos.',
                                             image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'creacion-de-clientes.png')),
                                             process_model_id: process.id,
                                             color: "#80298F")

# Create the first Activity for 'Customer Creation'
product_features_activity = ActivityModel.create!(name: 'Definir funciones del producto Fake', description: 'Ya que has validado tu prototipo, es hora de iniciar con la construccion del producto dirigido a clientes masivos, para ello lo primero que debes hacer es definir cuales seran las principales funciones de tu producto de acuerdo a la necesidades de tu cliente', stage_model_id: customer_creation_stage.id)
ArtifactModel.create!(name: 'Funciones del producto Fake', description: 'Documento de definición de funciones del producto', artifact_type: 'url',  activity_model_id: product_features_activity.id)

#
# Create the fifth Stage of the SCM: Company Building
#
company_building_stage = StageModel.create!(name: 'Company Building  Fake',
                                            description: 'Esta es la etapa de estructuración de la startup como una compañía de respuesta ágil y con una estrategia de gestión y crecimiento orientada a convertirse en una empresa sostenible en el tiempo.',
                                            image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'desarrollo-de-la-empresa.png')),
                                            process_model_id: process.id,
                                            color: "#FCB912")

# Create the first Activity for 'Company Building'
demand_creation_activity = ActivityModel.create!(name: 'Creación de demanda Fake', description: 'Identifica a tus clientes potenciales, y llevalos a ser clientes que compran', stage_model_id: company_building_stage.id)
ArtifactModel.create!(name: 'Documento de Creación de demanda Fake', description: 'Descripción documento de creación de demanda', artifact_type: 'url',   activity_model_id: demand_creation_activity.id)


# activate two of the ideas with that process
# generate active ideas
2.times do
  startup = entrepreneur_a.startups.create!(name: Faker::Company.name, phone: "#{Faker::Number.number(10)}", email: Faker::Internet.email)
  startup_b = entrepreneur_b.startups.create!(name: Faker::Company.name, phone: "#{Faker::Number.number(10)}", email: Faker::Internet.email)

  # seed unactive ideas
  6.times do
    idea_a = startup.ideas.create!(name: Faker::Company.name, description: Faker::Lorem.sentence(1), problem_definition: Faker::Lorem.sentence(1), is_private: true, is_active: false, idea_type: 'product')

    # activate the idea
    idea_a.activate(process, '0B7NTUabCCsAsbzlWdGZtRUNKUUU', 'https://drive.google.com/open?id=0B7NTUabCCsAsbzlWdGZtRUNKUUU' ,advisor_a)

    idea_a.idea_process.idea_stages.each do |idea_stage|
      idea_stage.update(status: 'finish' , start_date: Faker::Time.between(Chronic.parse("#{Faker::Number.number(1)} months ago"), Chronic.parse(" 2 days ago") , :all) , due_date: Faker::Time.between(Chronic.parse("1 days ago"), Date.today, :all) )
      idea_stage.idea_activities.each do |idea_activity|
        idea_activity.update(status: 'finish' , start_date: Faker::Time.between(Chronic.parse("#{Faker::Number.number(1)} months ago"),Chronic.parse("2 days ago"), :all) , due_date: Faker::Time.between(Chronic.parse("1 days ago"), Date.today, :all) )
      end
    end
  end
end




# fake process b
process_b = ProcessModel.create!(name: 'Fake StartUp Creation Model B', description: 'Aplicación Lean para CreaTIC Corporación')
#
# Create Stage of the SCM: Customer Discovery
#
customer_discovery_stage_b = StageModel.create!(name: 'Customer Discovery Fake b',
                                              description: 'Proceso de validación donde el emprendedor debe constatar que existe un grupo de clientes para quienes vale la pena resolver un problema.',
                                              image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'descubrimiento-de-clientes.png')),
                                              process_model_id: process_b.id,
                                              color: "#66B645")
#
#

# Create the first Activity for 'Customer Discovery'
inscription_activity_b = ActivityModel.create!(name: 'Paquete de inscripción Fake b', description: 'Inicia y has parte del Cluster CreaTIC, para esto es necesario que firmes unos compromisos y formalices tu vinculacion al proceso de creacion de StartUp. Solicita los documentos a tu Asesor.', stage_model_id: customer_discovery_stage_b.id)
ArtifactModel.create!(name: 'Pagaré Fake b', description: 'Documento que debes firmar junto con tu Asesor', artifact_type: 'url',   activity_model_id: inscription_activity_b.id)

#
# Create the Stage of the SCM: Customer Validation
#
customer_validation_stage_b = StageModel.create!(name: 'Customer Validation Fake b',
                                               description: 'Proceso de validacion de una solución desarrollada con early adopters',
                                               image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'validacion-de-clientes.png')),
                                               process_model_id: process_b.id,
                                               color: "#E41F26")
# Create the first Activity for 'Customer Validation'
first_prototype_activity_b = ActivityModel.create!(name: 'Prototipo V1.0 Fake b', description: 'Es hora de desarrollar tu primer prototipo, para ello debes utlizar practicas agiles de desarrollo. El CDT esta dispuesto a apoyarte con esta actividad, solicita la informacion con tu Asesor', stage_model_id: customer_validation_stage_b.id)
ArtifactModel.create!(name: 'Desarrollo de Prototipo Fake b', description: 'A partir del producto que desarrollaste, indícanos de que se trata y enséñanos como luce.', artifact_type: 'url',   activity_model_id: first_prototype_activity_b.id)


#
# Create the Stage of the SCM: Customer Creation
#
customer_creation_stage_b = StageModel.create!(name: 'Customer Creation  Fake b',
                                             description: 'Definicion de un road map de ventas escalable y repetibles. Una vez validada la solución a nivel de prototipo y métricas con early adopters debe procederse a la construcción de un producto dirigido a clientes masivos.',
                                             image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'creacion-de-clientes.png')),
                                             process_model_id: process_b.id,
                                             color: "#80298F")

# Create the first Activity for 'Customer Creation'
product_features_activity_b = ActivityModel.create!(name: 'Definir funciones del producto Fake b', description: 'Ya que has validado tu prototipo, es hora de iniciar con la construccion del producto dirigido a clientes masivos, para ello lo primero que debes hacer es definir cuales seran las principales funciones de tu producto de acuerdo a la necesidades de tu cliente', stage_model_id: customer_creation_stage_b.id)
ArtifactModel.create!(name: 'Funciones del producto Fake b', description: 'Documento de definición de funciones del producto', artifact_type: 'url',   activity_model_id: product_features_activity_b.id)

#
# Create the fifth Stage of the SCM: Company Building
#
company_building_stage_b = StageModel.create!(name: 'Company Building  Fake b',
                                            description: 'Esta es la etapa de estructuración de la startup como una compañía de respuesta ágil y con una estrategia de gestión y crecimiento orientada a convertirse en una empresa sostenible en el tiempo.',
                                            image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'desarrollo-de-la-empresa.png')),
                                            process_model_id: process_b.id,
                                            color: "#FCB912")

# Create the first Activity for 'Company Building'
demand_creation_activity_b = ActivityModel.create!(name: 'Creación de demanda Fake b', description: 'Identifica a tus clientes potenciales, y llevalos a ser clientes que compran', stage_model_id: company_building_stage_b.id)
ArtifactModel.create!(name: 'Documento de Creación de demanda Fake', description: 'Descripción documento de creación de demanda', artifact_type: 'url',   activity_model_id: demand_creation_activity_b.id)


# activate two of the ideas with that process
# generate active ideas
2.times do
  startup_b = entrepreneur_c.startups.create!(name: Faker::Company.name, phone: "#{Faker::Number.number(10)}", email: Faker::Internet.email)

  # seed unactive ideas
  6.times do
    idea_b = startup_b.ideas.create!(name: Faker::Company.name, description: Faker::Lorem.sentence(1), problem_definition: Faker::Lorem.sentence(1), is_private: true, is_active: false, idea_type: 'service')

    # activate the idea
    idea_b.activate(process_b, '0B7NTUabCCsAsbzlWdGZtRUNKUUU', 'https://drive.google.com/open?id=0B7NTUabCCsAsbzlWdGZtRUNKUUU' ,advisor_b)

    idea_b.idea_process.idea_stages.each do |idea_stage|
      idea_stage.update(status: 'finish' , start_date: Faker::Time.between(Chronic.parse("#{Faker::Number.number(1)} months ago"), Chronic.parse("#{Faker::Number.number(1)} days ago"), :all) , due_date: Faker::Time.between(Chronic.parse("2 days ago"), Date.today, :all) )
      idea_stage.idea_activities.each do |idea_activity|
        idea_activity.update(status: 'finish' , start_date: Faker::Time.between(Chronic.parse("#{Faker::Number.number(1)} months ago"), Chronic.parse("#{Faker::Number.number(1)} days ago") , :all) , due_date: Faker::Time.between(Chronic.parse("2 days ago"), Date.today, :all) )
      end
    end
  end
end

