namespace :creatic_process do
  desc "Create admin user for emprende creatic"
  task create_admin: :environment do
    # AdminUser.destroy_all
    AdminUser.create!(email: 'romaqz22@gmail.com', password: 'password', password_confirmation: 'password')
  end

  desc "Create startup creation model for emprende creatic"
  task create_process: :environment do
    #
    # ProcessModel.destroy_all
    # Create the process for the StartUp Creation Model (SCM)
    process = ProcessModel.create!(name: 'StartUp Creation Model', description: 'Aplicación Lean para CreaTIC Corporación')
    #
    # Create Stage of the SCM: Customer Discovery
    #
    customer_discovery_stage = StageModel.create!(name: 'Customer Discovery',
                                                  description: 'Proceso de validación donde el emprendedor debe constatar que existe un grupo de clientes para quienes vale la pena resolver un problema.',
                                                  image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'descubrimiento-de-clientes.png')),
                                                  process_model_id: process.id,
                                                  color: "#66B645")


    #
    #

    # Create the first Activity for 'Customer Discovery'
    inscription_activity = ActivityModel.create!(name: 'Paquete de inscripción', description: 'Inicia y has parte del Cluster CreaTIC, para esto es necesario que firmes unos compromisos y formalices tu vinculacion al proceso de creacion de StartUp. Solicita los documentos a tu Asesor.', stage_model_id: customer_discovery_stage.id)
    ArtifactModel.create!(name: 'Pagaré', description: 'Documento que debes firmar junto con tu Asesor', artifact_type: 'url',   activity_model_id: inscription_activity.id)
    ArtifactModel.create!(name: 'Carta de instrucciones', description: 'Documento que debes firmar junto con tu Asesor', artifact_type: 'url',   activity_model_id: inscription_activity.id)
    ArtifactModel.create!(name: 'Acuerdo de compromiso', description: 'Documento que debes firmar junto con tu Asesor', artifact_type: 'url',   activity_model_id: inscription_activity.id)
    ArtifactModel.create!(name: 'Acuerdo de confidencialidad', description: 'Documento que debes firmar junto con tu Asesor', artifact_type: 'url',   activity_model_id: inscription_activity.id)

    # Create the second Activity for 'Customer Discovery'
    entrepreneurship_diagnosis_activity = ActivityModel.create!(name: 'Diagnóstico del emprendimiento', description: 'Primero debemos conocer en que estado se encuentra tu emprendimiento.', stage_model_id: customer_discovery_stage.id)
    ArtifactModel.create!(name: 'Diagnostico emprendimiento', description: 'Preguntas que debes contestar junto con tu Asesor', artifact_type: 'url',   activity_model_id: entrepreneurship_diagnosis_activity.id)

    # create the third Activity for 'Customer Discovery'
    competitive_intelligence_activity = ActivityModel.create!(name: 'Inteligencia competitiva', description: 'Es importante que conozcas el contexto alrededor de tu idea y para ello es necesario que realices, antes que cualquier otra cosa que estés pensando, un ejercicio de Inteligencia competitiva.', stage_model_id: customer_discovery_stage.id)
    ArtifactModel.create!(name: 'Documento de Inteligencia competitiva-IT', description: 'Registra palabras claves para realizar tu búsqueda, organiza tus resultados en la matriz de competidores y matriz comparativa', artifact_type: 'url',   activity_model_id: competitive_intelligence_activity.id)
    ArtifactModel.create!(name: 'Informe de vigilancia Tecnológica - VT', description: 'Este informe sera realizado por la Unidad de Vigilancia del Cluster CreaTIC. Este documento no es obligatorio', artifact_type: 'url',   activity_model_id: competitive_intelligence_activity.id)

    # Create the fourth Activity for 'Customer Discovery'
    lean_canvas_activity = ActivityModel.create!(name: 'Lean Canvas', description: 'Identifica problemas, quienes lo padecen y como vas a plantear una solución, así generaras tu modelo de negocio Lean Canvas. Manos a la obra!', stage_model_id: customer_discovery_stage.id)
    ArtifactModel.create!(name: 'Lean Canvas', description: 'Con esta herramientas diseñas tu modelo de negocio', artifact_type: 'url',   activity_model_id: lean_canvas_activity.id)

    # Create the fifth Activity for 'Customer Discovery'
    interview_activity = ActivityModel.create!(name: 'Entrevista', description: 'Es importante que valides lo que planteaste en tu modelo de negocio, debes asegurarte de que el problema realmente existe, para ello debes realizar una serie de entrevistas', stage_model_id: customer_discovery_stage.id)
    # Create the  Artifact for Inscription activity
    ArtifactModel.create!(name: 'Entrevista de Validación del Problema', description: 'Formato de como debes realizar tu entrevista', artifact_type: 'url',   activity_model_id: interview_activity.id)
    ArtifactModel.create!(name: 'Tabulación de entrevistas', description: 'Tabulación de tus entrevistas para ver los resultados mas evidentes', artifact_type: 'url',   activity_model_id: interview_activity.id)

    # Create the sixth Activity for 'Customer Discovery'
    learned_lessons_activity = ActivityModel.create!(name: 'Lecciones aprendidas', description: 'Es hora de la retroalimentación!!!Después de analizar los resultados obtenidos en las entrevistas debes identificar las lecciones aprendidas mas importantes, para así refinar tu modelo de negocio con datos verídicos', stage_model_id: customer_discovery_stage.id)
    ArtifactModel.create!(name: 'Lecciones aprendidas', description: 'Lista lo que aprendiste y los nuevos hallazgos', artifact_type: 'url',   activity_model_id: learned_lessons_activity.id)

    # Create the seventh Activity for 'Customer Discovery'
    minimal_viable_product_activity = ActivityModel.create!(name: 'Producto minimo viable', description: 'Estructura tu solución para resolver el problema que identificaste que existe, teniendo en cuenta lo que aprendiste en la anterior actividad', stage_model_id: customer_discovery_stage.id)
    ArtifactModel.create!(name: 'Producto minimo viable', description: 'A partir del producto mínimo viable que creaste, indícanos de que se trata y enséñanos como luce.', artifact_type: 'url',   activity_model_id: minimal_viable_product_activity.id)
    ArtifactModel.create!(name: 'Tablero de validacion', description: 'Con esta herramienta puedes validar tu producto', artifact_type: 'url',   activity_model_id: minimal_viable_product_activity.id)

    #
    # Create the Stage of the SCM: Customer Validation
    #
    customer_validation_stage = StageModel.create!(name: 'Customer Validation',
                                                   image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'validacion-de-clientes.png')),
                                                   description: 'Proceso de validacion de una solución desarrollada con early adopters',
                                                   process_model_id: process.id,
                                                   color: "#E41F26")
    # Create the first Activity for 'Customer Validation'
    first_prototype_activity = ActivityModel.create!(name: 'Prototipo V1.0', description: 'Es hora de desarrollar tu primer prototipo, para ello debes utlizar practicas agiles de desarrollo. El CDT esta dispuesto a apoyarte con esta actividad, solicita la informacion con tu Asesor', stage_model_id: customer_validation_stage.id)
    ArtifactModel.create!(name: 'Desarrollo de Prototipo', description: 'A partir del producto que desarrollaste, indícanos de que se trata y enséñanos como luce.', artifact_type: 'url',   activity_model_id: first_prototype_activity.id)
    ArtifactModel.create!(name: 'Kanban - Plantilla de seguimiento', description: 'Con esta herramientas puedes seguir el desarrollo de tu prototipo', artifact_type: 'file',   activity_model_id: first_prototype_activity.id)
    ArtifactModel.create!(name: 'Product Backlog', description: 'Define tus historias de usuario y priorízalas', artifact_type: 'url',   activity_model_id: first_prototype_activity.id)

    # Create the second Activity for 'Customer Validation'
    report_activity = ActivityModel.create!(name: 'Informe de (Modelo 2A 3R)', description: 'Llego la hora de determinar el exito del prototipo que desarrollaste. Para conseguirlo, es importante que definas unas metricas clave que te permitiran monitorear la aceptacion de tu solucion por parte del mercado objetivo.', stage_model_id: customer_validation_stage.id)
    ArtifactModel.create!(name: 'Modelo 2A 3R', description: 'Selecciona las métricas mas acorde a tu solución y evalúalas', artifact_type: 'url',   activity_model_id: report_activity.id)

    # Create the third Activity for 'Customer Validation'
    funding_sources_activity = ActivityModel.create!(name: 'Fuentes de financiación', description: 'Si estas en este punto tu idea ha sido aceptada en el mercado y estas en la capacidad de obtener otras formas de financiacion que haga crecer tu solucion', stage_model_id: customer_validation_stage.id)
    ArtifactModel.create!(name: 'Fuentes de financiación', description: 'Base de fuentes de financiación.', artifact_type: 'url',   activity_model_id: funding_sources_activity.id)


    #
    # Create the Stage of the SCM: Customer Creation
    #
    customer_creation_stage = StageModel.create!(name: 'Customer Creation',
                                                 description: 'Definicion de un road map de ventas escalable y repetibles. Una vez validada la solución a nivel de prototipo y métricas con early adopters debe procederse a la construcción de un producto dirigido a clientes masivos.',
                                                 image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'creacion-de-clientes.png')),
                                                 process_model_id: process.id,
                                                 color: "#80298F")

    # Create the first Activity for 'Customer Creation'
    product_features_activity = ActivityModel.create!(name: 'Definir funciones del producto', description: 'Ya que has validado tu prototipo, es hora de iniciar con la construccion del producto dirigido a clientes masivos, para ello lo primero que debes hacer es definir cuales seran las principales funciones de tu producto de acuerdo a la necesidades de tu cliente', stage_model_id: customer_creation_stage.id)
    ArtifactModel.create!(name: 'Funciones del producto', description: 'Documento de definición de funciones del producto', artifact_type: 'url',   activity_model_id: product_features_activity.id)

    # Create the second Activity for 'Customer Creation'
    marketing_relationships_activity = ActivityModel.create!(name: 'Plan de marketing', description: 'Es importante que definas la manera en como vas a llegar a tu mercado objetivo', stage_model_id: customer_creation_stage.id)
    ArtifactModel.create!(name: 'Plan de marketing', description: 'Descripción plan de marketing', artifact_type: 'url',   activity_model_id: marketing_relationships_activity.id)

    # Create the third Activity for 'Customer Creation'
    sales_roadmap_activity = ActivityModel.create!(name: 'Road map', description: 'En esta actividad es necesario que plasmes los pasos a seguir para que se incrementen tus ventas', stage_model_id: customer_creation_stage.id)
    ArtifactModel.create!(name: 'Hoja ruta de ventas', description: 'Descripción hoja ruta de ventas.', artifact_type: 'url',   activity_model_id: sales_roadmap_activity.id)

    # Create the fourth Activity for 'Customer Creation'
    customer_tracking_activity = ActivityModel.create!(name: 'Seguimiento de clientes', description: 'No debes olvidar que es necesario realizar un seguimiento a tus clientes y analizar su comportamiento para que logres crecer con tu producto', stage_model_id: customer_creation_stage.id)
    ArtifactModel.create!(name: 'Informe de (Modelo 2A 3R)', description: 'Es igual que el anterior informe AARRR, pero para este caso lo manejaras por cohortes', artifact_type: 'url',   activity_model_id: customer_tracking_activity.id)

    # Create the fifth Activity for 'Customer Creation'
    user_experience_activity = ActivityModel.create!(name: 'Validación de experiencia de usuario', description: 'Para conocer si el cliente esta satisfecho realmente con tu producto, es necesario que valides la experiencia con el mismo, logrando asi mejorar continuamente tu producto', stage_model_id: customer_creation_stage.id)
    ArtifactModel.create!(name: 'Validacion de experiencia de usuario', description: 'Descripción validacion de experiencia de usuario', artifact_type: 'url',   activity_model_id: user_experience_activity.id)

    # Create the sixth Activity for 'Customer Creation'
    company_portfolio_activity = ActivityModel.create!(name: 'Portafolio de la empresa', description: 'Inicia tu compaña comercial, es la hora de vender tu producto!!! Para ello es necesario que crees un portafolio que sea atractivo para los clientes', stage_model_id: customer_creation_stage.id)
    ArtifactModel.create!(name: 'Portafolio', description: 'Descripción portafolio de la empresa.', artifact_type: 'url',   activity_model_id: company_portfolio_activity.id)


    #
    # Create the fifth Stage of the SCM: Company Building
    #
    company_building_stage = StageModel.create!(name: 'Company Building',
                                                description: 'Esta es la etapa de estructuración de la startup como una compañía de respuesta ágil y con una estrategia de gestión y crecimiento orientada a convertirse en una empresa sostenible en el tiempo.',
                                                image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'lib', 'assets', 'images', 'desarrollo-de-la-empresa.png')),
                                                process_model_id: process.id,
                                                color: "#FCB912")

    # Create the first Activity for 'Company Building'
    demand_creation_activity = ActivityModel.create!(name: 'Creación de demanda', description: 'Identifica a tus clientes potenciales, y llevalos a ser clientes que compran', stage_model_id: company_building_stage.id)
    ArtifactModel.create!(name: 'Documento de Creación de demanda', description: 'Descripción documento de creación de demanda', artifact_type: 'url',   activity_model_id: demand_creation_activity.id)

    # Create the second Activity for 'Company Building'
    positioning_promotion_activity = ActivityModel.create!(name: 'Plan de posicionamiento y promoción', description: 'Identifica oportunidades donde logres desenvolverte con tu producto, de esta manera te daras a concer y obtendras un posicionamiento en el mercado', stage_model_id: company_building_stage.id)
    ArtifactModel.create!(name: 'Plan de posicionamiento y promoción', description: 'Descripción documento que consigna el plan de posicionamiento y promoción', artifact_type: 'url',   activity_model_id: positioning_promotion_activity.id)

    # Create the third Activity for 'Company Building'
    organizational_structure_activity = ActivityModel.create!(name: 'Estructura organizacional', description: 'Para lograr tener sinergia con tu equipo, define una estructura organizacion que te ayudaran a convertirte en una compañia de respuesta agil, ademas en esta actividad para conseguir el crecimiento que deseas debes realizar tus proyecciones en ventas.', stage_model_id: company_building_stage.id)
    ArtifactModel.create!(name: 'Estructura organizacional', description: 'Documento que consigna la estructura organizacional', artifact_type: 'url',   activity_model_id: organizational_structure_activity.id)
    ArtifactModel.create!(name: 'Valoración de la inversión', description: 'Documento que consigna la valoración de la inversión', artifact_type: 'url',   activity_model_id: organizational_structure_activity.id)

    # Create the fourth Activity for 'Company Building'
    international_portfolio_activity = ActivityModel.create!(name: 'Portafolio internacional de la empresa', description: 'Piensa en grande, tu producto puede ser un gran exito a nivel global', stage_model_id: company_building_stage.id)
    # Create the Artifact for international portfolio activity
    ArtifactModel.create!(name: 'Portafolio internacional', description: 'Documento Portafolio internacional de la emrpesa', artifact_type: 'url',   activity_model_id: international_portfolio_activity.id)
    end

  task create_survey: :environment do
    Question.delete_all
    questions = Question.all
    if questions.count == 0
      question_one = Question.create!(name: 'Crecimiento económico', description: 'Incremento sostenido en las ventas realizadas a través del tiempo.')
      question_two = Question.create!(name: 'Consolidación del producto', description: 'Facilidad para replicar la solución/producto en nuevos clientes y mantener el porcentaje de participación del mercado.')
      question_three = Question.create!(name: 'Fortaleza del equipo de trabajo', description: 'Personal comprometido y con capacidades relevantes para el desarrollo del portafolio de productos/soluciones de la empresa.')
      question_four = Question.create!(name: 'Exitoso', description: 'empresa con buen posicionamiento en el mercado y gran perspectiva de mantener y captar mayor porcentaje a corto y mediano plazo')
    end
  end

  task entrepreneur_import: :environment do
    Entrepreneur.import force:true
  end
end
