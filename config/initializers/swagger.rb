GrapeSwaggerRails.options.url      = 'api/v1/swagger_doc.json'
GrapeSwaggerRails.options.app_name = 'EmprendeCreatic'
GrapeSwaggerRails.options.app_url  = '/'
GrapeSwaggerRails.options.api_auth     = 'basic' # Or 'bearer' for OAuth
GrapeSwaggerRails.options.api_key_name = 'Authorization'
GrapeSwaggerRails.options.api_key_type = 'header'