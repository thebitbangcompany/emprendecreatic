PublicActivity::Activity.module_eval do
  acts_as_readable 
  after_create :send_push_notification
  def send_push_notification
    PusherManagement.push_message('new_notification')
  end
end