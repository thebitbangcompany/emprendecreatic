Rails.application.routes.draw do

  get 'messages/inbox'

  get 'messages/show'

  devise_for :entrepreneurs, :controllers => {:sessions => 'sessions', :registrations => 'entrepreneurs/registrations' }
  #devise_for :advisors , :controllers => { :registrations => 'registrations' }
  devise_for :advisors , :controllers => {:sessions => 'sessions'}
  as :advisor do
    get 'advisors/edit' => 'advisors#edit_account', :as => 'edit_advisor_registration'
    put 'advisors' => 'advisors#update_account', :as => 'advisor_registration'
    get 'advisors/records' => 'advisors#records', :as => 'advisor_records'
    post 'advisors/report' => 'advisors#report', :as => 'advisor_report'
  end
  devise_for :admin_users, ActiveAdmin::Devise.config

  get "advisors" => "advisors#index" , as: "advisors_list"
  get "entrepreneurs" => "entrepreneurs#index" , as: "entrepreneurs_list"

  get "inbox"=>"conversations#inbox",as: "inbox"
  get "sentbox"=>"conversations#sentbox",as: "sentbox"
  get "trash"=>"conversations#trash",as: "trash"
  post "send_trash"=>"conversations#send_trash",as: "send_trash"
  post "untrash"=>"conversations#untrash",as: "untrash"

  get "conversations/:id"=>"conversations#show",as: "show_conversation"

  ActiveAdmin.routes(self)

  # after  auth entrepreneur
  authenticated :entrepreneur do
    root :to => "entrepreneurs#entrepreneur_profile" , :as => "authenticated_entrepreneur_root"
  end

  #after  auth entrepreneur
  authenticated :advisor do
    root :to => "advisors#advisor_profile" , :as => "authenticated_advisor_root"
  end
  #Advisor admin views
  get "following_ideas" => "advisors#following_ideas", :as => "following_ideas"

  root 'static_pages#home'

  get 'send_notification' => 'pusher_demo#send_notification', as: 'send_notification'

  get 'advisor_logged_in' => 'advisors#is_user_advisor', as: 'is_user_advisor'

  get 'entrepreneur_logged_in' => 'entrepreneurs#is_user_entrepreneur', as: 'is_user_entrepreneur'

  get 'not_found' => 'static_pages#not_found'

  #**********************# Mailboxer routes #********************#
  resources :conversations, only: [:index, :show, :new, :create] do
    member do
      post :reply
      post :trash
      post :untrash
    end
  end

  #get 'advisors/edit_profile/:id' => 'advisors#edit', as: 'advisor_edit_profile'

  #get 'advisors/:id' => 'advisors#show', as: 'advisor_show_profile'

  resources :advisors, only: [:edit, :update, :show]
  resources :entrepreneurs , only: [:edit , :update , :show]

  resources :activities

  #get 'advisors/:id/ideas/' => 'advisor_startups#list', as: 'advisor_startups'
  #get 'advisors/:id/startups/:startup_id/ideas/:idea_id' => 'advisor_startups#show', as: 'show_advisor_idea'
  #post 'advisors/:id/startups/:startup_id' => 'advisor_startups#change_startup_status', as: 'change_startup_status_advisor'

  # resources :advisor_startups , only: [:index, :show, :update]

  # resources :startups, only: [] do
  #   resources :advisor_ideas , only: [:update , :show ]
  # end

  resources :startups , only:[:show,:new,:create,:edit,:update,:index] do
    
    collection do
       get 'advisor_index'
       get 'advisor_inactive'
    end
    member do
      put 'avatar'
      put 'active_update'
    end
    resources :startup_entrepreneurs, only:[:index,:update,:create,:destroy] 

    # to ideas controller
    resources :ideas , only:[:create,:update,:edit,:show] do
      member do
        put 'avatar'
        put 'activate'
        put 'edit_drive_folder'
        put 'deactivate'
        get 'edit_registration'
        get 'new_achievement'
        delete 'achievement/:achievement_id', :to => 'ideas#delete_achievement', as: 'delete_achievement'
        get 'new_pivot'
        post 'create_pivot'
        get 'finish_process'
        post 'create_achievement'
        put 'update_registration'
        post 'advisor_follow_idea'
      end
    end
  end


  resources :idea_stages, only:[] do
    put 'update_status' , on: :member
  end

  resources :images
  
  resources :idea_activities, only:[] do
    # personalized show to avoid conflicts with will paginate
    get 'show'
    # personalized status update route
    put 'change_status'
    put 'advisor_change_status'
    # manage comments comments_controller
    resources :comments, only:[:destroy, :create, :index,:update]
    # manage artifacts idea_artifacts_controller
    resources :idea_artifacts, except:[:show] do
      put 'change_status'
    end

  end
  resources :comments , only:[:update]

  #artifact states routes
  put 'set_as_corrections' => "idea_artifacts#set_as_corrections" ,as: 'set_artifact_as_corrections'
  put 'set_as_corrected' => "idea_artifacts#set_as_corrected" ,as: 'set_artifact_as_corrected'
  put 'set_as_finished' => "idea_artifacts#set_as_finished" ,as: 'set_artifact_as_finished'

  #invitations routes
  post "invitations"=> "invitations#create", as: "invitations"
  # show external profile
    #get 'startup/:id' => 'startups#startup_profile', as: 'startup_visitor_profile'


  #general requests
  #before upload an artifact we select the type of artifact and we get access to the support file as entrepreneur
  get 'entrepreneur_get_artifact_model' => 'idea_activities#activity_model_artifact_selected' , as: 'entrepreneur_get_artifact_model'
  # get the list of activities filtered by stage
  get 'activities_list' => 'idea_activities#list_by_stage', as: 'get_activities_list'

  get 'mark_all_as_read' => "activities#mark_all_as_read"
  #swagger configuration
  mount API => '/'
  mount GrapeSwaggerRails::Engine => '/api_doc'

  #elastic search routes
  get 'search_entrepreneurs', to: 'search#search_entrepreneurs'
  get 'search_entrepreneurs_index', to: 'search#search_entrepreneurs_index'

  #set activity as read
  post 'activity_read', to: 'activities#read'
  get 'update_activities', to: 'activities#update_activities'
  get 'append_activities', to: 'activities#append_activities'

  #survey
  get 'survey' => 'surveys#show', :as => 'show_survey'
  post 'start_survey' => 'surveys#start', :as => 'start_survey'
  post 'qualifyStartup' => 'surveys#qualifyStartup', :as => 'qualifyStartup'

end
