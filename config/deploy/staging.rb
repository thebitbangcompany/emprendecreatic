set :stage, :staging
set :branch , 'staging'
set :deploy_to, '/home/emprende/code/EmprendeCreatic'

# Replace 127.0.0.1 with your server's IP address!
server '104.44.139.108', user: 'emprende', roles: %w{web app}, my_property: :my_value
