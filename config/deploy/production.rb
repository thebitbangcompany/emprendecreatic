set :stage, :production
set :branch , 'master'
set :deploy_to, '/home/deploy/code/EmprendeCreatic'
set :bundle_binstubs, nil

# Replace 127.0.0.1 with your server's IP address!
server '23.96.45.189', user: 'deploy', roles: %w{web app}, my_property: :my_value