# config valid only for current version of Capistrano
#lock '3.4.0'

set :application, 'EmprendeCreatic'
set :repo_url, 'git@bitbucket.org:thebitbangcompany/emprendecreatic.git'

set :deploy_to, '/home/emprende/code/EmprendeCreatic'

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}

set :nodenv_type, :user # or :system, depends on your nodenv setup
set :nodenv_node, '4.2.2'
set :nodenv_prefix, "NODENV_ROOT=#{fetch(:nodenv_path)} NODENV_VERSION=#{fetch(:nodenv_node)} #{fetch(:nodenv_path)}/bin/nodenv exec"
set :nodenv_map_bins, %w{node npm lineman}
set :nodenv_roles, :all # default value
set :rbenv_path , '$HOME/.rbenv'

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end

    after 'deploy:publishing', 'deploy:restart'
    after 'deploy:finishing', 'deploy:cleanup'

  end

  after :publishing, 'deploy:restart'

end
