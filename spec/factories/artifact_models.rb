# == Schema Information
#
# Table name: artifact_models
#
#  id                :integer          not null, primary key
#  name              :string           default(""), not null
#  description       :string           default(""), not null
#  activity_model_id :integer
#  artifact_type     :string           default("")
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  support           :string
#

FactoryGirl.define do
  factory :artifact_model do
    name {Faker::Company.name}
    description {Faker::Lorem.sentence(1)}
    artifact_type "file"
    association :activity_model
  end
end
