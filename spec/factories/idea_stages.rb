# == Schema Information
#
# Table name: idea_stages
#
#  id              :integer          not null, primary key
#  stage_model_id  :integer
#  idea_process_id :integer
#  status          :integer          default(0)
#  start_date      :datetime
#  due_date        :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  is_pivot        :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :idea_stage do
  	stage_model_id 1
  	idea_process_id 1
    start_date Time.now
    due_date Time.now
  end
end
