# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  question_id :integer
#  startup_id  :integer
#  advisor_id  :integer
#  value       :float
#  answered    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :answer do
  	association :startup
  	association :question
  	association :advisor
  	answered true
    value 4
  end
end
