# == Schema Information
#
# Table name: questions
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :question do
    name {Faker::Company.name}
    description {Faker::Lorem.sentence(1)}
  end
end
