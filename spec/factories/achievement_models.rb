# == Schema Information
#
# Table name: achievement_models
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  image       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :achievement_model do
    name Faker::Lorem.word
    description Faker::Lorem.paragraph
    image {Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'images', 'test_image.png')) }
  end
end
