# == Schema Information
#
# Table name: stage_models
#
#  id               :integer          not null, primary key
#  name             :string           default(""), not null
#  description      :string           default(""), not null
#  process_model_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  image            :string
#  color            :string
#

FactoryGirl.define do
  factory :stage_model do
    association :process_model
    name {Faker::Company.name}
    description {Faker::Lorem.sentence(1)}
  end

end
