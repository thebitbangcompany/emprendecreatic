# == Schema Information
#
# Table name: process_models
#
#  id          :integer          not null, primary key
#  name        :string           default(""), not null
#  description :string           default(""), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :process_model do
    name {Faker::Company.name}
    description{Faker::Lorem.sentence(1)}
  end

end
