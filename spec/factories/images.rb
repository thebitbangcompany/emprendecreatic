# == Schema Information
#
# Table name: images
#
#  id          :integer          not null, primary key
#  image       :string
#  description :string
#  idea_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryGirl.define do
  factory :image do
    image "MyString"
    description "MyString"
    idea_id 1
  end
end
