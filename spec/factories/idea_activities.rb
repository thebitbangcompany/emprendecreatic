# == Schema Information
#
# Table name: idea_activities
#
#  id                :integer          not null, primary key
#  activity_model_id :integer
#  idea_stage_id     :integer
#  status            :integer          default(0)
#  start_date        :datetime
#  due_date          :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deadline_date     :datetime
#

FactoryGirl.define do
  factory :idea_activity do
    activity_model_id 1
  	idea_stage_id 1
    start_date Time.now
    due_date Time.now
  end
end
