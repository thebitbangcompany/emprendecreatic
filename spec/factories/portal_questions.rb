# == Schema Information
#
# Table name: portal_questions
#
#  id         :integer          not null, primary key
#  issue      :string           default(""), not null
#  content    :string           default(""), not null
#  email      :string           default(""), not null
#  author     :string           default(""), not null
#  status     :integer          default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  reply      :string
#

FactoryGirl.define do
  factory :portal_question do
    issue {Faker::Lorem.sentence}
    content {Faker::Lorem.characters(280)}
    email {Faker::Internet.email}
    author {Faker::Name.name}
    status 0
  end

end
