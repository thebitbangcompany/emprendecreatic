# == Schema Information
#
# Table name: startups
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  vision     :string
#  mission    :string
#  email      :string
#  phone      :string
#  website    :string
#  industry   :string
#  is_active  :boolean          default(TRUE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  avatar     :string
#  facebook   :string
#  linkedin   :string
#  twitter    :string
#  city       :string
#  country    :string
#  deleted_at :datetime
#

FactoryGirl.define do
  factory :startup do
    name {Faker::Company.name}
    vision {Faker::Company.catch_phrase}
    mission {Faker::Company.bs}
    email {Faker::Internet.email}
    phone {Faker::Number.number(10)}
    website {Faker::Internet.url}
    industry {Faker::Company.catch_phrase}
    is_active false

    #after(:create) do |startup|
    #  startup.entrepreneurs << FactoryGirl.create(:entrepreneur)
    #end

  end

end
