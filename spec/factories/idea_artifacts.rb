# == Schema Information
#
# Table name: idea_artifacts
#
#  id                :integer          not null, primary key
#  artifact_model_id :integer
#  idea_activity_id  :integer
#  url               :string
#  status            :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string
#  support           :string
#  start_date        :datetime
#  due_date          :datetime
#

FactoryGirl.define do
  factory :idea_artifact do
    name 'test file'
    association :artifact_model
    association :idea_activity
  	url "https://drive.google.com/open?id=0B7NTUabCCsAsMy1LRUl6ZXVycFlFcDNQZFk4V2t5UzlxM1hv"
    support { Rack::Test::UploadedFile.new(File.join(Rails.root, 'documentation', 'emprende_process', 'artifacts', 'Certificado_afiliacion_CC1085250874.pdf')) }
  end
end
