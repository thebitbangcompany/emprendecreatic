# == Schema Information
#
# Table name: advisors
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  occupation             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  deleted_at             :datetime
#  avatar                 :string
#  is_expert              :boolean          default(FALSE), not null
#  authentication_token   :string
#

FactoryGirl.define do
  factory :advisor do
    first_name {Faker::Name.first_name}
    last_name {Faker::Name.last_name}
    phone '38383837373'
    linkedin {Faker::Internet.url('linkedin.com', '/estebance')}
    twitter {Faker::Internet.url('twitter.com', '/estebance')}
    facebook {Faker::Internet.url('facebook.com', '/estebance')}
    summary {Faker::Lorem.paragraph(1)}
    profession {Faker::Lorem.characters(140)}
    tag_list 'programming, ruby, rails'
    email {Faker::Internet.email}
    password 'password123'
    password_confirmation 'password123'
    occupation {Faker::Lorem.sentence(1)}
  end

end
