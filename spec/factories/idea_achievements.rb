# == Schema Information
#
# Table name: idea_achievements
#
#  id                   :integer          not null, primary key
#  idea_id              :integer
#  achievement_model_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

FactoryGirl.define do
  factory :idea_achievement do
    association :idea
    association :achievement_model
  end
end
