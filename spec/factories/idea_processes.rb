# == Schema Information
#
# Table name: idea_processes
#
#  id               :integer          not null, primary key
#  process_model_id :integer
#  start_date       :datetime
#  due_date         :datetime
#  idea_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

FactoryGirl.define do
  factory :idea_process do
    process_model_id 1
    idea_id 1
    start_date Time.now
    due_date Time.now
  end
end
