# == Schema Information
#
# Table name: activity_models
#
#  id             :integer          not null, primary key
#  name           :string           default(""), not null
#  description    :string           default(""), not null
#  stage_model_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  has_deadline   :boolean          default(FALSE), not null
#

FactoryGirl.define do
  factory :activity_model do
    association :stage_model
    name {Faker::Company.name}
    description {Faker::Lorem.sentence(1)}
  end

end
