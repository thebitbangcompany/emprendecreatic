# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  question_id :integer
#  startup_id  :integer
#  advisor_id  :integer
#  value       :float
#  answered    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Answer, type: :model do
  describe "Associations" do
    it { should belong_to(:question)}
    it { should belong_to(:startup)}
    it { should belong_to(:advisor)}
  end

  it 'has a valid factory' do
    expect(build(:answer)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

    	it 'is invalid without question' do
        expect(build(:answer, question_id: nil)).to_not be_valid
      end

      it 'is invalid without a startup' do
        expect(build(:answer, startup_id: nil)).to_not be_valid
      end

      it 'is invalid without a advisor' do
        expect(build(:answer, advisor_id: nil)).to_not be_valid
      end

      it 'is invalid without a answered' do
        expect(build(:answer, answered: nil)).to_not be_valid
      end

      it 'is valid without a value' do
        expect(build(:answer, value: nil)).to be_valid
      end

    end
	end
end
