# == Schema Information
#
# Table name: process_models
#
#  id          :integer          not null, primary key
#  name        :string           default(""), not null
#  description :string           default(""), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe ProcessModel, type: :model do

  it 'has a valid factory' do
    expect(build(:process_model)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a name' do
        expect(build(:process_model, name: nil)).to_not be_valid
      end

    end

    context 'length' do

      it 'is invalid if the description is too short' do
        expect(build(:process_model , description: "#{Faker::Lorem.characters(1)}")).to_not be_valid
      end


      it 'is invalid if the description is too long' do
        expect(build(:process_model , description: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end

      it 'is invalid if the name is too short' do
        expect(build(:process_model , name: "#{Faker::Lorem.characters(1)}")).to_not be_valid
      end

      it 'is invalid if the name is too long' do
        expect(build(:process_model , name: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end

    end

  end


  describe 'relationship' do
    it {should  have_many(:stage_models)}
    it {should  have_many(:idea_processes)}
  end

end
