# == Schema Information
#
# Table name: idea_achievements
#
#  id                   :integer          not null, primary key
#  idea_id              :integer
#  achievement_model_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'rails_helper'

RSpec.describe IdeaAchievement, type: :model do

  it 'has a valid factory' do
    expect(build(:idea_achievement)).to be_valid
  end
  #
  describe 'validations' do
    #
    context 'presence' do
      #
      it 'is invalid without an idea_id ' do
        expect(build(:idea_achievement ,idea_id: nil)).to_not be_valid
      end
      #
      it 'is invalid without a achievement_model_id' do
        expect(build(:idea_achievement , achievement_model_id: nil)).to_not be_valid
      end
      #
    end
    #
  end
end
