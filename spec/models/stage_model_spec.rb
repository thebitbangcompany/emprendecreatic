# == Schema Information
#
# Table name: stage_models
#
#  id               :integer          not null, primary key
#  name             :string           default(""), not null
#  description      :string           default(""), not null
#  process_model_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  image            :string
#  color            :string
#

require 'rails_helper'

RSpec.describe StageModel, type: :model do

  it 'has a valid factory' do
    expect(build(:stage_model)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a name' do
        expect(build(:stage_model, name: nil)).to_not be_valid
      end

    end

    context 'length' do
      it 'is invalid if the description is too short' do
        expect(build(:stage_model , description: "#{Faker::Lorem.characters(1)}")).to_not be_valid
      end


      it 'is invalid if the description is too long' do
        expect(build(:stage_model , description: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end
    end

  end


  describe 'relationship' do

    it {should belong_to(:process_model)}
    it {should  have_many(:idea_stages)}
    let (:process) {FactoryGirl.create(:process_model)}
    let (:process_b) {FactoryGirl.create(:process_model)}

    context 'uniqueness' do

      it 'does not allow duplicate stages per process' do
        FactoryGirl.create(:stage_model, name: 'stage' , process_model: process)
        expect(build(:stage_model , name: 'stage' , process_model: process)).to_not be_valid
      end

      it 'allows two processes to share a stage name' do
        FactoryGirl.create(:stage_model, name: 'stage' , process_model: process)
        expect(build(:stage_model , name: 'stage' , process_model: process_b)).to be_valid
      end


    end


  end


end
