# == Schema Information
#
# Table name: idea_processes
#
#  id               :integer          not null, primary key
#  process_model_id :integer
#  start_date       :datetime
#  due_date         :datetime
#  idea_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'rails_helper'

RSpec.describe IdeaProcess, type: :model do
  	
  it 'has a valid factory' do
    expect(build(:idea_process)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a process_model_id' do
        expect(build(:idea_process, process_model_id: nil)).to_not be_valid
      end

      it 'is invalid without a idea_id' do
        expect(build(:idea_process, idea_id: nil)).to_not be_valid
      end

      it 'is invalid without a start_date' do
        expect(build(:idea_process, start_date: nil)).to_not be_valid
      end

    end

  end

  describe 'relationship' do
    it {should  belong_to(:idea)}
    it {should  belong_to(:process_model)}
    it {should  have_many(:idea_stages)}
  end

end
