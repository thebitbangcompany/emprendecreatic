# == Schema Information
#
# Table name: idea_stages
#
#  id              :integer          not null, primary key
#  stage_model_id  :integer
#  idea_process_id :integer
#  status          :integer          default(0)
#  start_date      :datetime
#  due_date        :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  is_pivot        :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe IdeaStage, type: :model do
  it 'has a valid factory' do
    expect(build(:idea_stage)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a stage_model_id' do
        expect(build(:idea_stage, stage_model_id: nil)).to_not be_valid
      end

      it 'is invalid without a idea_process_id' do
        expect(build(:idea_stage, idea_process_id: nil)).to_not be_valid
      end

      it 'is invalid without a status' do
        expect(build(:idea_stage, status: nil)).to_not be_valid
      end

    end

  end

  context 'class methods' do
    context 'statuses' do
      it 'has the correct options' do
        expect(IdeaStage.statuses.keys).to eq(['pending','start','finish'])
      end
    end
  end

  context 'instance variables' do
    context 'status' do
      it 'is in pending by default' do
        expect(build(:idea_stage).status).to eq('pending')
      end
    end
  end


  describe 'relationship' do
    it {should  belong_to(:idea_process)}
    it {should  belong_to(:stage_model)}
    it {should  have_many(:idea_activities)}
  end

end
