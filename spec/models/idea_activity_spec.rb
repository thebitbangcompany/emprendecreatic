# == Schema Information
#
# Table name: idea_activities
#
#  id                :integer          not null, primary key
#  activity_model_id :integer
#  idea_stage_id     :integer
#  status            :integer          default(0)
#  start_date        :datetime
#  due_date          :datetime
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  deadline_date     :datetime
#

require 'rails_helper'

RSpec.describe IdeaActivity, type: :model do
  it 'has a valid factory' do
    expect(build(:idea_activity)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a activity_id' do
        expect(build(:idea_activity, activity_model_id: nil)).to_not be_valid
      end

      it 'is invalid without a idea_stage_id' do
        expect(build(:idea_activity, idea_stage_id: nil)).to_not be_valid
      end

      it 'is invalid without a status' do
        expect(build(:idea_activity, status: nil)).to_not be_valid
      end

    end

  end

  context 'class methods' do
    context 'statuses' do
      it 'has the correct options' do
        expect(IdeaActivity.statuses.keys).to eq(['pending','start','finish'])
      end
    end
  end

  context 'instance variables' do
    context 'status' do
      it 'is in pending by default' do
        expect(build(:idea_activity).status).to eq('pending')
      end
    end
  end


  describe 'relationships' do
    it {should  belong_to(:idea_stage)}
    it {should  belong_to(:activity_model)}
    it {should  have_many(:idea_artifacts)}
  end

end
