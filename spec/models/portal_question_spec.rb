# == Schema Information
#
# Table name: portal_questions
#
#  id         :integer          not null, primary key
#  issue      :string           default(""), not null
#  content    :string           default(""), not null
#  email      :string           default(""), not null
#  author     :string           default(""), not null
#  status     :integer          default(0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  reply      :string
#

require 'rails_helper'

RSpec.describe PortalQuestion, type: :model do

  it 'has a valid factory' do
    expect(build(:portal_question)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without an issue' do
        expect(build(:portal_question, issue: nil)).to_not be_valid
      end

      it 'is invalid without a content' do
        expect(build(:portal_question, content: nil)).to_not be_valid
      end

      it 'is invalid without an email' do
        expect(build(:portal_question, email: nil)).to_not be_valid
      end

      it 'is invalid without an author' do
        expect(build(:portal_question, author: nil)).to_not be_valid
      end

      it 'is invalid without a status' do
        expect(build(:portal_question, status: nil)).to_not be_valid
      end

    end

    context 'format' do
      it 'is invalid with a wrong email' do
        expect(build(:portal_question, email: 'esteban.hotmail.com')).to_not be_valid
      end

      it 'is valid with a good email' do
        expect(build(:portal_question, email: 'esteban.lp@hotmail.com')).to be_valid
      end
    end

    context 'length' do

      it 'is invalid when the author name is too short' do
        expect(build(:portal_question , author: 'a')).to_not be_valid
      end

      it 'is invalid when the issue is too short' do
        expect(build(:portal_question, issue: 'a')).to_not be_valid
      end

      it 'is invalid when the issue is too long' do
        expect(build(:portal_question , issue: Faker::Lorem.characters(141))).to_not be_valid
      end

      it 'is invalid when the content is too short' do
        expect(build(:portal_question, content: 'a')).to_not be_valid
      end

      it 'is invalid when the content is too long' do
        expect(build(:portal_question, content: Faker::Lorem.characters(281))).to_not be_valid
      end

    end

  end

end
