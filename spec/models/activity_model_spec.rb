# == Schema Information
#
# Table name: activity_models
#
#  id             :integer          not null, primary key
#  name           :string           default(""), not null
#  description    :string           default(""), not null
#  stage_model_id :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  has_deadline   :boolean          default(FALSE), not null
#

require 'rails_helper'

RSpec.describe ActivityModel, type: :model do

  it 'has a valid factory' do
    expect(build(:activity_model)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a name' do
        expect(build(:activity_model, name: nil)).to_not be_valid
      end

    end

    context 'length' do
      it 'is invalid if the description is too short' do
        expect(build(:activity_model , description: "#{Faker::Lorem.characters(1)}")).to_not be_valid
      end


      it 'is invalid if the description is too long' do
        expect(build(:activity_model , description: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end
    end

  end


  describe 'relationship' do

    let (:process_a) {FactoryGirl.create(:process_model)}
    let (:stage) {FactoryGirl.create(:stage_model , process_model: process_a)}
    let (:stage_b) {FactoryGirl.create(:stage_model , process_model: process_a)}


    it {should belong_to(:stage_model)}
    it {should  have_many(:idea_activities)}

    context 'uniqueness' do

      it 'does not allow duplicate activities per stage' do
        FactoryGirl.create(:activity_model, name: 'activity' , stage_model: stage)
        expect(build(:activity_model , name: 'activity' , stage_model: stage)).to_not be_valid
      end

      it 'allows two stages to share an activity' do
        FactoryGirl.create(:activity_model, name: 'activity' , stage_model: stage)
        expect(build(:activity_model , name: 'activity' , stage_model: stage_b)).to be_valid
      end
    end


  end



end
