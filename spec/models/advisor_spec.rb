# == Schema Information
#
# Table name: advisors
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  occupation             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  deleted_at             :datetime
#  avatar                 :string
#  is_expert              :boolean          default(FALSE), not null
#  authentication_token   :string
#

require 'rails_helper'

RSpec.describe Advisor, type: :model do

  it 'has a valid factory' do
    expect(build(:advisor)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a first name' do
        expect(build(:advisor , first_name: nil)).to_not be_valid
      end

      it 'is invalid without a last name' do
        expect(build(:advisor , last_name: nil)).to_not be_valid
      end

      it 'is invalid without an email' do
        expect(build(:advisor , email: nil)).to_not be_valid
      end

      it 'is invalid without a password' do
        expect(build(:advisor , password: nil)).to_not be_valid
      end

      it 'is invalid without a phone' do
        expect(build(:advisor , phone: nil )).to_not be_valid
      end

    end


    context 'length' do
      it 'is invalid if first name length is too small' do
        expect(build(:advisor , first_name:'n')).to_not be_valid
      end


      it 'is invalid if last name length is too small' do
        expect(build(:advisor , last_name:'n')).to_not be_valid
      end

      it 'is invalid if phone number is too small' do
        expect(build(:advisor , phone:'33333')).to_not be_valid
      end

      it 'is invalid if summary is too long' do
        expect(build(:advisor , summary: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end

      it 'is invalid if profession is too long' do
        expect(build(:advisor , profession: "#{Faker::Lorem.characters(141)}")).to_not be_valid
      end

      it 'is invalid if occupation is too long' do
        expect(build(:advisor, occupation: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end
    end

    context 'format' do
      it 'is invalid if you do not provide your facebook profile' do
        expect(build(:advisor , facebook: 'www.facebook.co')).to_not be_valid
      end


      it 'is invalid if you do not provide your linkedin profile' do
        expect(build(:advisor , linkedin: 'www.facebook.com')).to_not be_valid
      end


      it 'is invalid if you do not provide your twitter profile' do
        expect(build(:advisor , twitter:'https://facebook.com/estebance')).to_not be_valid
      end
    end

    context 'uniqueness' do
      it 'does not allow two advisors to share the email' do
        FactoryGirl.create(:advisor , email: 'esteban.lp@gmail.com')
        expect(build(:advisor , email: 'esteban.lp@gmail.com')).to_not be_valid
      end
    end

  end

  describe 'relationship' do
    it {should  have_many(:ideas)}
  end

end
