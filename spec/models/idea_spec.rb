# == Schema Information
#
# Table name: ideas
#
#  id                 :integer          not null, primary key
#  name               :string           default(""), not null
#  description        :text             default(""), not null
#  problem_definition :text             default(""), not null
#  startup_id         :integer
#  is_private         :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  advisor_id         :integer
#  is_active          :boolean          default(FALSE)
#  avatar             :string
#  website            :string
#  idea_type          :string
#  facebook           :string
#  twitter            :string
#  drive_folder       :string
#  drive_folder_link  :string
#  deleted_at         :datetime
#

require 'rails_helper'

RSpec.describe Idea, type: :model do

  it 'has a valid factory' do
    expect(build(:idea)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a name' do
        expect(build(:idea , name: nil)).to_not be_valid
      end
      
    end

    context 'length' do

      it 'is invalid when name is too short' do
        expect(build(:idea , name: 'n')).to_not be_valid
      end

      it 'is invalid when the name is too long' do
        expect(build(:idea , name: "#{Faker::Lorem.characters(281)}" )).to_not be_valid
      end

      it 'is invalid when description is too short' do
        expect(build(:idea, description: 'n')).to_not be_valid
      end

      it 'is invalid when problem definition is too short' do
        expect(build(:idea, problem_definition: 'n')).to_not be_valid
      end

    end


  end

  describe 'relationship' do
    it {should  belong_to(:advisor)}
    it {should have_one(:idea_process)}
    it {should  have_many(:idea_achievements)}
  end

  describe 'instance methods' do
    context 'activate' do 
      it "should return an idea process that belongs to the current idea" do
        idea = FactoryGirl.create(:idea)
        process = FactoryGirl.create(:process_model)
        advisor = FactoryGirl.create(:advisor)
        idea_process = idea.activate(process, 'driveid', 'filelink', advisor)
        expect(idea_process.idea).to eq(idea)
        expect(idea_process.process_model_id).to eq(process.id)
        expect(idea.reload().is_active).to be true
        expect(idea.reload().advisor_id).to be(advisor.id)
      end
    end
    context 'deactivate' do
      it 'should set idea is_active field to false' do
        idea = FactoryGirl.create(:idea, is_active: true)
        idea.deactivate()
        expect(idea.reload().is_active).to be false
      end
    end
  end
end
