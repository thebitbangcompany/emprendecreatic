# == Schema Information
#
# Table name: achievement_models
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  image       :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe AchievementModel, type: :model do

   it 'has a valid factory' do
     expect(build(:achievement_model)).to be_valid
   end
  #
   describe 'validations' do
  #
    context 'presence' do
  #
      it 'is invalid without a name' do
         expect(build(:achievement_model , name: nil)).to_not be_valid
       end
  #
      it 'is invalid without a description' do
         expect(build(:achievement_model , description: nil)).to_not be_valid
      end
  #
      it 'is invalid without an image' do
        expect(build(:achievement_model , image: nil)).to_not be_valid
      end
    end
  #
     context 'length' do

       it 'is invalid if name length is too small' do
         expect(build(:achievement_model , name:'n')).to_not be_valid
       end
  #
       it 'is invalid if description length is too small' do
         expect(build(:achievement_model , description:'n')).to_not be_valid
       end
  #
       it 'is invalid if name is too long' do
         expect(build(:achievement_model , name: "#{Faker::Lorem.characters(281)}")).to_not be_valid
       end
  #
       it 'is invalid if description is too long' do
         expect(build(:achievement_model , description: "#{Faker::Lorem.characters(281)}")).to_not be_valid
       end
     end
   end

   describe 'relationship' do
     it {should  have_many(:idea_achievements)}
   end
end
