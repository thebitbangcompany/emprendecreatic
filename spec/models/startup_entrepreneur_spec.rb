# == Schema Information
#
# Table name: startup_entrepreneurs
#
#  id              :integer          not null, primary key
#  startup_id      :integer
#  entrepreneur_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  role            :string           default("member")
#  position        :string
#

require 'rails_helper'

RSpec.describe StartupEntrepreneur, type: :model do

  it 'has a valid factory' do
    expect(build(:startup_entrepreneur)).to be_valid
  end

  describe 'relationship' do
    it {should  belong_to(:entrepreneur)}
    it {should  belong_to(:startup)}
  end

end
