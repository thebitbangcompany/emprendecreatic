# == Schema Information
#
# Table name: questions
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Question, type: :model do

  describe "Associations" do
    it { should have_many(:answers)}
  end

  it 'has a valid factory' do
    expect(build(:question)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

    	it 'is invalid without a name' do
        expect(build(:question, name: nil)).to_not be_valid
      end

      it 'is invalid without a description' do
        expect(build(:question, description: nil)).to_not be_valid
      end

    end

    context 'length' do

    	it 'is invalid if the name is too short' do
        expect(build(:question , name: "#{Faker::Lorem.characters(1)}")).to_not be_valid
      end

      it 'is invalid if the description is too short' do
        expect(build(:question , description: "#{Faker::Lorem.characters(1)}")).to_not be_valid
      end

      it 'is invalid if the name is too long' do
        expect(build(:question , name: "#{Faker::Lorem.characters(41)}")).to_not be_valid
      end

      it 'is invalid if the description is too long' do
        expect(build(:question , description: "#{Faker::Lorem.characters(121)}")).to_not be_valid
      end

    end
    
	end
end
