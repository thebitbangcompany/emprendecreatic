# == Schema Information
#
# Table name: idea_artifacts
#
#  id                :integer          not null, primary key
#  artifact_model_id :integer
#  idea_activity_id  :integer
#  url               :string
#  status            :integer          default(0)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  name              :string
#  support           :string
#  start_date        :datetime
#  due_date          :datetime
#

require 'rails_helper'

RSpec.describe IdeaArtifact, type: :model do
  it 'has a valid factory' do
    expect(build(:idea_artifact)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a name' do
        expect(build(:idea_artifact, name: nil)).to_not be_valid
      end

      it 'is invalid without an url' do
        expect(build(:idea_artifact, url: nil)).to_not be_valid
      end

      it 'is invalid without a status' do
        expect(build(:idea_artifact, status: nil)).to_not be_valid
      end

    end


    # context 'format' do

      # it 'is invalid if you do not  provide a valid drive file' do
        # expect(build(:idea_artifact, url: 'https://drive.google.com/open')).to_not be_valid
      # end

    # end

  end

  context 'class methods' do
    context 'statuses' do
      it 'has the correct options' do
        expect(IdeaArtifact.statuses.keys).to eq(['pending','start','corrections', 'corrected', 'finish'])
      end
    end
  end

  context 'instance variables' do
    context 'status' do
      it 'is in pending by default' do
        expect(build(:idea_artifact).status).to eq('pending')
      end
    end
  end


  describe 'relationship' do
    it {should  belong_to(:idea_activity)}
    it {should  belong_to(:artifact_model)}
  end

end
