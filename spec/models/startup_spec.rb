# == Schema Information
#
# Table name: startups
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  vision     :string
#  mission    :string
#  email      :string
#  phone      :string
#  website    :string
#  industry   :string
#  is_active  :boolean          default(TRUE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  avatar     :string
#  facebook   :string
#  linkedin   :string
#  twitter    :string
#  city       :string
#  country    :string
#  deleted_at :datetime
#

require 'rails_helper'

RSpec.describe Startup, type: :model do
  it 'has a valid factory' do
    expect(build(:startup)).to be_valid
  end

  describe 'validations' do
    context 'presence' do
      it 'is invalid without a name' do
        expect(build(:startup , name: nil)).to_not be_valid
      end

    end

    context 'length' do
      it 'is invalid if  name length is too short' do
        expect(build(:startup, name:'n')).to_not be_valid
      end

      it 'is invalid if summary is too long' do
        expect(build(:startup, name: Faker::Lorem.characters(281))).to_not be_valid
      end

      it 'is invalid if vision length is too short' do
        expect(build(:startup, vision: 'n')).to_not be_valid
      end

      it 'is invalid if vision length is too long' do
        expect(build(:startup, vision: Faker::Lorem.characters(281))).to_not be_valid
      end

      it 'is invalid if mission length is too short' do
        expect(build(:startup, mission: 'n')).to_not be_valid
      end

      it 'is invalid if mission length is too long' do
        expect(build(:startup, mission: Faker::Lorem.characters(281))).to_not be_valid
      end

      it 'is invalid if phone length is too short' do
        expect(build(:startup, phone: '333')).to_not be_valid
      end

      it 'is invalid if phone length is too long' do
        expect(build(:startup, phone: Faker::Lorem.characters(23))).to_not be_valid
      end

      it 'is invalid if industry length is too short' do
        expect(build(:startup, industry: 'n')).to_not be_valid
      end

      it 'is invalid if industry length is too long' do
        expect(build(:startup, industry: Faker::Lorem.characters(281))).to_not be_valid
      end
    end

    context 'format' do
      it 'is invalid if email format is wrong' do
        expect(build(:startup, email: 'esteban.lp.gmail.com')).to_not be_valid
      end

      it 'is invalid if website format is wrong' do
        expect(build(:startup, website: 'ww.cue')).to_not be_valid
      end

      it 'is valid if email format is ok' do
        expect(build(:startup, email: 'esteban.lp@gmail.com')).to be_valid
      end

      it 'is valid if website format is ok' do
        expect(build(:startup, website: 'http://www.google.com')).to be_valid
      end
    end

    context 'uniqueness' do
      it 'does not allow duplicate startups name' do
        FactoryGirl.create(:startup, name: 'startup')
        expect(build(:startup, name: 'startup')).to_not be_valid
      end

      it 'does not allow duplicate startups phone number' do
        FactoryGirl.create(:startup, phone: '7723456789')
        expect(build(:startup, phone: '7723456789')).to_not be_valid
      end

      it 'does not allow duplicate startups email' do
        FactoryGirl.create(:startup, email: 'info@thebitbang.company')
        expect(build(:startup, email: 'info@thebitbang.company')).to_not be_valid
      end
    end
  end

  describe 'relationship' do
    it { should have_many(:entrepreneurs) }
    it { should have_many(:ideas) }

    let(:startup) { FactoryGirl.create(:startup, name: 'TBBC') }
    let(:startup_b) { FactoryGirl.create(:startup, name: 'Other') }

    context 'uniqueness' do
      it 'does not allow duplicate ideas per startup' do
        FactoryGirl.create(:idea, name: 'EmprendeCreatic', startup_id: startup)
        expect(build(:idea, name: 'EmprendeCreatic', startup_id: startup)).to_not be_valid
      end

      it 'does not allow two startups to share an idea' do
        FactoryGirl.create(:idea, name: 'EmprendeCreatic', startup_id: startup)
        expect(build(:idea, name: 'EmprendeCreatic', startup_id: startup_b)).to_not be_valid
      end
    end
  end
end
