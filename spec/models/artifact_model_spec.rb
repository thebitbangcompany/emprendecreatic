# == Schema Information
#
# Table name: artifact_models
#
#  id                :integer          not null, primary key
#  name              :string           default(""), not null
#  description       :string           default(""), not null
#  activity_model_id :integer
#  artifact_type     :string           default("")
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  support           :string
#

require 'rails_helper'

RSpec.describe ArtifactModel, type: :model do

  it 'has a valid factory' do
    expect(create(:artifact_model)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a name' do
        expect(build(:artifact_model, name: nil)).to_not be_valid
      end

    end

    context 'length' do
      it 'is invalid if the description is too short' do
        expect(build(:artifact_model , description: "#{Faker::Lorem.characters(1)}")).to_not be_valid
      end


      it 'is invalid if the description is too long' do
        expect(build(:artifact_model , description: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end
    end

  end

  describe 'relationship' do

    let (:process_a) {FactoryGirl.create(:process_model)}
    let (:stage) {FactoryGirl.create(:stage_model , process_model: process_a)}
    let (:activity){FactoryGirl.create(:activity_model, stage_model: stage)}
    let (:activity_b){FactoryGirl.create(:activity_model, stage_model: stage)}

    it {should belong_to(:activity_model)}
    it {should  have_many(:idea_artifacts)}

    context 'uniqueness' do

      it 'does not allow duplicate artifacts per activity' do
        FactoryGirl.create(:artifact_model, name: 'artifact' , activity_model: activity)
        expect(build(:artifact_model , name: 'artifact' , activity_model: activity)).to_not be_valid
      end

      it 'allows two activities to share an artifact' do
        FactoryGirl.create(:artifact_model, name: 'artifact' , activity_model: activity)
        expect(build(:artifact_model , name: 'artifact' , activity_model: activity_b)).to be_valid
      end

    end


  end

end

