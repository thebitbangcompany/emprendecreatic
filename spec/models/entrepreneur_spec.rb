# == Schema Information
#
# Table name: entrepreneurs
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  linkedin               :string
#  twitter                :string
#  facebook               :string
#  summary                :string
#  profession             :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  avatar                 :string
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#  country                :string
#  city                   :string
#  deleted_at             :datetime
#

require 'rails_helper'

RSpec.describe Entrepreneur, type: :model do

  it 'has a valid factory' do
    expect(build(:entrepreneur)).to be_valid
  end

  describe 'validations' do

    context 'presence' do

      it 'is invalid without a first name' do
        expect(build(:entrepreneur , first_name: nil)).to_not be_valid
      end

      it 'is invalid without a last name' do
        expect(build(:entrepreneur , last_name: nil)).to_not be_valid
      end

      it 'is invalid without an email' do
        expect(build(:entrepreneur , email: nil)).to_not be_valid
      end

      it 'is invalid without a password' do
        expect(build(:entrepreneur , password: nil)).to_not be_valid
      end

      it 'is invalid without a phone' do
        expect(build(:entrepreneur , phone: nil )).to_not be_valid
      end

    end

    context 'length' do
      it 'is invalid if first name length is too small' do
        expect(build(:entrepreneur , first_name:'n')).to_not be_valid
      end


      it 'is invalid if last name length is too small' do
        expect(build(:entrepreneur , last_name:'n')).to_not be_valid
      end

      it 'is invalid if phone number is too small' do
        expect(build(:entrepreneur , phone:'33333')).to_not be_valid
      end

      it 'is invalid if summary is too long' do
        expect(build(:entrepreneur , summary: "#{Faker::Lorem.characters(281)}")).to_not be_valid
      end

      it 'is invalid if profession is too long' do
        expect(build(:entrepreneur , profession: "#{Faker::Lorem.characters(141)}")).to_not be_valid
      end

    end

    context 'format' do

      it 'is invalid if you do not provide your facebook profile' do
        expect(build(:entrepreneur , facebook: 'www.facebook.co')).to_not be_valid
      end


      it 'is invalid if you do not provide your linkedin profile' do
        expect(build(:entrepreneur , linkedin: 'www.facebook.com')).to_not be_valid
      end


      it 'is invalid if you do not provide your twitter profile' do
        expect(build(:entrepreneur , twitter:'https://facebook.com/estebance')).to_not be_valid
      end

    end


  end

  describe 'relationship' do
    it {should  have_many(:startups)}
  end


end
