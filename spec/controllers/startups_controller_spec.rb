# == Schema Information
#
# Table name: startups
#
#  id         :integer          not null, primary key
#  name       :string           default(""), not null
#  vision     :string
#  mission    :string
#  email      :string
#  phone      :string
#  website    :string
#  industry   :string
#  is_active  :boolean          default(TRUE), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  avatar     :string
#  facebook   :string
#  linkedin   :string
#  twitter    :string
#  city       :string
#  country    :string
#  deleted_at :datetime
#

require 'rails_helper'

RSpec.describe StartupsController, type: :controller do

end
