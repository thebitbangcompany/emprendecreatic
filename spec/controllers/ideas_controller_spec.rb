# == Schema Information
#
# Table name: ideas
#
#  id                 :integer          not null, primary key
#  name               :string           default(""), not null
#  description        :text             default(""), not null
#  problem_definition :text             default(""), not null
#  startup_id         :integer
#  is_private         :boolean          default(FALSE), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  advisor_id         :integer
#  is_active          :boolean          default(FALSE)
#  avatar             :string
#  website            :string
#  idea_type          :string
#  facebook           :string
#  twitter            :string
#  drive_folder       :string
#  drive_folder_link  :string
#  deleted_at         :datetime
#

require 'rails_helper'

RSpec.describe IdeasController, type: :controller do

end
