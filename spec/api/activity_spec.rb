require 'rails_helper'
require 'base64'

RSpec.describe 'Activity' do
  describe :v1 do
    context 'Activities' do

      before :context do
        @advisor = FactoryGirl.create(:advisor , password: 'super_password', password_confirmation: 'super_password')
        # proceso
        process = FactoryGirl.create(:process_model)

        2.times do
          stage  = FactoryGirl.create(:stage_model, process_model_id: process.id )
          2.times do
            FactoryGirl.create(:activity_model, stage_model_id: stage.id)
          end
        end

        3.times do
          idea = FactoryGirl.create(:idea)
          idea.activate(process, 'driveid', 'filelink', @advisor)
          # stages
          idea_stage = idea.idea_process.idea_stages.first
          idea_stage.status = 1
          idea_stage.save

          # activities
          idea_activity = idea_stage.idea_activities.first
          idea_activity.status = 1
          idea_activity.save

        end

      end

      context 'GET' do
        it 'retrieves ideas per activity' do
          get '/api/v1/activity/get_activity_ideas', {activity_id: 1} , { "Authorization" => "Basic #{Base64.encode64(@advisor.authentication_token)}" }
          expect(response.status).to eq 200
          expect(JSON.parse(response.body).count).to be 3
        end
      end

    end
  end
end