require 'rails_helper'

RSpec.describe 'Auth' do

  describe :v1 do
    context '#login' do

      before :context do
        @advisor = FactoryGirl.create(:advisor , password: 'super_password', password_confirmation: 'super_password')
      end

      context 'POST' do
        it 'log as advisor in the app' do
          post '/api/v1/auth/login', %Q{email=#{@advisor.email}&password=super_password}
          expect(response.status).to eq 201
          expect(JSON.parse(response.body)["authentication_token"]).not_to be_nil
        end
      end
    end
  end
end