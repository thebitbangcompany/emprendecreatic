# == Schema Information
#
# Table name: images
#
#  id          :integer          not null, primary key
#  image       :string
#  description :string
#  idea_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe "Images", type: :request do
  describe "GET /images" do
    it "works! (now write some real specs)" do
      get images_path
      expect(response).to have_http_status(200)
    end
  end
end
