source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.4'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'magnific-popup-rails', '~> 1.1.0'
gem 'unread', '~> 0.7.1'
# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-fileupload-rails', '~> 0.4.6'
gem 'jquery-turbolinks'
gem 'jquery-form-rails', '~> 1.0'
gem 'jquery-ui-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

#Where or from rails 5
gem 'where-or'

#create Pdf from html
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

#best in place
gem 'best_in_place', '~> 3.0.1'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# user auth
gem 'devise'
gem 'devise_invitable', '~> 1.5.2'
gem 'devise-token_authenticatable'

# administration and dependences
gem 'activeadmin', '1.0.0.pre1'
gem 'annotate'
#views
gem 'haml'
gem 'haml-rails', '~> 0.9'
#styles
gem 'bootstrap-sass'
gem 'will_paginate-bootstrap'
gem 'cancan'
gem 'draper'
gem 'pundit'

# simple form
gem 'simple_form'
#Client side validation gem
gem 'client_side_validations'
gem 'client_side_validations-simple_form'

#dynamic form
gem 'cocoon'

#API
# API
gem 'grape', '0.8.0'
gem 'grape-entity', '~> 0.4.0'
gem 'grape-swagger', '~> 0.7.2'
gem 'grape-swagger-rails', '~> 0.0.10'

# Capistrano
gem 'capistrano', '~> 3.4.0'
gem 'capistrano-bundler', '~> 1.1.2'
gem 'capistrano-rails', '~> 1.1.1'
# We are using rbenv
gem 'capistrano-rbenv', github: 'capistrano/rbenv'


# remove active record security layer
gem 'hashie-forbidden_attributes'

# Database
gem 'bcrypt'

# file uploader
gem 'carrierwave'
gem 'rmagick'

# tags abilities
gem 'acts-as-taggable-on'
# tags for frontend
gem 'tag-it-rails'

gem 'validates_email_format_of'

#active model
gem 'activemodel-associations'
gem 'font-awesome-rails'

#pusher for notifications
gem 'pusher'

#public_activity for activity feed
gem 'public_activity'

#provide communication between users
gem 'mailboxer'

#permission
gem 'cancancan'

# file validation
gem 'file_validators'


# follow functionality for models
gem 'acts_as_follower'

# redis
gem 'redis'

# gem to select countries on forms
gem 'country_select'

# provide comments
gem 'acts_as_commentable_with_threading'

# upload file using ajax
gem 'remotipart', '~> 1.2'

# pagination
gem 'will_paginate'
gem 'ajax_pagination'

# faker data
gem 'faker'
# elastic search
gem 'elasticsearch-model'
gem 'elasticsearch-rails'

# best in place
gem 'best_in_place', '~> 3.0.1'

# clone objects
gem 'deep_cloneable', '~> 2.2.0'

# time difference
gem 'time_diff'
gem 'time_difference'

# fake time gem
gem 'chronic'

# it doesn't delete all information from database
gem 'paranoia', '~> 2.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  # gem 'byebug'
  gem 'rspec-rails', '~> 3.0'
  gem 'rspec-collection_matchers'
  # added
  #gem 'factory_girl'
  gem 'factory_girl_rails'
  gem 'factory_girl'
  # ruby code
  gem 'rubocop'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  #debugging
  gem 'pry'
  gem 'pry-byebug'

  # change erb files to haml
  gem 'erb2haml'

end

group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'launchy'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers'
end
