# README #

### Emprende Creatic Project ###

Features:

 - Rails 4.2.4
 - Ruby 2.1.3
 - ActiveRecord
 - Devise
 - RSpec with basic config
 - Grape with basic config
 - Capistrano basic configuration

* Version

  1.0.0

### How do I get set up? ###

* bundle install
* cp config/database_example.yml config/database.yml
* rake db:migrate
* rails s
* please insert the following configuration in your host file to work with the drive api
   * nano /etc/hosts
   * insert the following line ( follow the previous configurations)
    * 127.0.0.1     local.example.com

##Setup elasticsearch (In rails console)

* Entrepreneur.import force: true
